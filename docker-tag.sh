#!/usr/bin/env bash

MODS="all"
DIR=$(dirname "$(readlink -f "$0")")

if [ "x$1" == "x" ]; then
    echo "source tag is required"
    exit 1
fi

if [ "x$2" == "x" ]; then
    echo "final tag is required"
    exit 1
fi
PREFIX=""
if [ "x$3" != "x" ]; then
    echo "using repository $3"
    PREFIX="$3/"
fi

docker tag myprysm/search-service:$1 ${PREFIX}myprysm/search-service:$2
docker tag myprysm/book-service:$1 ${PREFIX}myprysm/book-service:$2
docker tag myprysm/media-service:$1 ${PREFIX}myprysm/media-service:$2
docker tag myprysm/barrel:$1 ${PREFIX}myprysm/barrel:$2