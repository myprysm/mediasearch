package fr.myprysm.mediasearch.search.kotlin.dto

import fr.myprysm.mediasearch.search.dto.SearchCriteria
import fr.myprysm.mediasearch.search.dto.SearchCriteria.SortField
import fr.myprysm.mediasearch.search.dto.SearchCriteria.SortOrder
import fr.myprysm.mediasearch.search.dto.SearchCriteria.Source

fun SearchCriteria(
        country: String? = null,
        includeMeta: Boolean? = null,
        maxResultsPerSource: Int? = null,
        prettyPrint: Boolean? = null,
        sortField: SortField? = null,
        sortOrder: SortOrder? = null,
        source: Source? = null,
        term: String? = null): SearchCriteria = fr.myprysm.mediasearch.search.dto.SearchCriteria().apply {

    if (country != null) {
        this.setCountry(country)
    }
    if (includeMeta != null) {
        this.setIncludeMeta(includeMeta)
    }
    if (maxResultsPerSource != null) {
        this.setMaxResultsPerSource(maxResultsPerSource)
    }
    if (prettyPrint != null) {
        this.setPrettyPrint(prettyPrint)
    }
    if (sortField != null) {
        this.setSortField(sortField)
    }
    if (sortOrder != null) {
        this.setSortOrder(sortOrder)
    }
    if (source != null) {
        this.setSource(source)
    }
    if (term != null) {
        this.setTerm(term)
    }
}

