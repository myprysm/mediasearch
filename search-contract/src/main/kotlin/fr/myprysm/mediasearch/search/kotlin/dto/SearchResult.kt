package fr.myprysm.mediasearch.search.kotlin.dto

import fr.myprysm.mediasearch.search.dto.SearchResult

fun SearchResult(
        author: String? = null,
        meta: io.vertx.core.json.JsonObject? = null,
        term: String? = null,
        title: String? = null,
        type: String? = null): SearchResult = fr.myprysm.mediasearch.search.dto.SearchResult().apply {

    if (author != null) {
        this.setAuthor(author)
    }
    if (meta != null) {
        this.setMeta(meta)
    }
    if (term != null) {
        this.setTerm(term)
    }
    if (title != null) {
        this.setTitle(title)
    }
    if (type != null) {
        this.setType(type)
    }
}

