package fr.myprysm.mediasearch.search.service;

import fr.myprysm.mediasearch.search.dto.SearchCriteria;
import fr.myprysm.mediasearch.search.dto.SearchResult;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.GenIgnore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

import java.util.Comparator;
import java.util.List;

@VertxGen
@ProxyGen
public interface SearchService {

    /**
     * Generates the sample service bundled with the contract
     *
     * @param vertx   the vertx instance
     * @param handler the handler
     * @return the service, not yet initialized (use handler)
     */
    @GenIgnore
    static SearchService createSample(Vertx vertx, Handler<AsyncResult<SearchService>> handler) {
        return new SampleSearchService(vertx, handler);
    }


    /**
     * Get the comparator matching field and order
     *
     * @param sortField the sort field
     * @param sortOrder the order
     * @return the comparator
     */
    @GenIgnore
    default Comparator<JsonObject> getComparator(SearchCriteria.SortField sortField, SearchCriteria.SortOrder sortOrder) {
        Comparator<JsonObject> comparator = Comparator.comparing(o -> o.getString(sortField.name()), Comparator.nullsLast(String::compareTo));

        if (sortOrder == SearchCriteria.SortOrder.desc) {
            comparator = comparator.reversed();
        }
        return comparator;
    }

    /**
     * Search content by term
     *
     * @param criteria the criteria
     * @param handler  the result handler
     * @return this
     */
    @Fluent
    SearchService search(SearchCriteria criteria, Handler<AsyncResult<List<SearchResult>>> handler);
}
