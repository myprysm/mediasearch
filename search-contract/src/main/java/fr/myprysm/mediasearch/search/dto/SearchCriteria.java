package fr.myprysm.mediasearch.search.dto;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@DataObject(generateConverter = true)
public class SearchCriteria {

    private Integer maxResultsPerSource = 5;
    private Source source = Source.all;
    private String term;
    private String country;
    private SortOrder sortOrder = SortOrder.asc;
    private SortField sortField = SortField.author;
    private boolean includeMeta = false;
    private boolean prettyPrint = false;

    public SearchCriteria(SearchCriteria other) {
        setMaxResultsPerSource(other.maxResultsPerSource);
        source = other.source;
        term = other.term;
        country = other.country;
        sortOrder = other.sortOrder;
        sortField = other.sortField;
        includeMeta = other.includeMeta;
        prettyPrint = other.prettyPrint;
    }

    public SearchCriteria(JsonObject json) {
        SearchCriteriaConverter.fromJson(json, this);
    }

    public SearchCriteria setMaxResultsPerSource(Integer maxResultsPerSource) {
        if (maxResultsPerSource != null) {
            if (maxResultsPerSource < 1) {
                maxResultsPerSource = 1;
            }

            if (maxResultsPerSource > 50) {
                maxResultsPerSource = 50;
            }
        }
        this.maxResultsPerSource = maxResultsPerSource;
        return this;
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        SearchCriteriaConverter.toJson(this, json);
        return json;
    }

    public enum Source {books, media, all}

    public enum SortOrder {asc, desc}

    public enum SortField {author, title, type}
}
