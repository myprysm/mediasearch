package fr.myprysm.mediasearch.search.service;

import fr.myprysm.mediasearch.search.dto.SearchCriteria;
import fr.myprysm.mediasearch.search.dto.SearchResult;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class SampleSearchService implements SearchService {
    private static final Map<String, String> RESOURCES = new HashMap<>();
    private static final String SAMPLE_MEDIA_RESOURCE = "sample/search/media.json";
    private static final String SAMPLE_BOOKS_RESOURCE = "sample/search/books.json";
    private final Vertx vertx;
    private List<JsonObject> books;
    private List<JsonObject> media;

    public SampleSearchService(Vertx vertx, Handler<AsyncResult<SearchService>> handler) {
        this.vertx = vertx;
        this.vertx.executeBlocking(future -> {
            initialize();
            future.complete();
        }, result -> handler.handle(Future.succeededFuture(this)));
    }

    private void initialize() {
        if (books == null) {
            books = arrayFromFile(SAMPLE_BOOKS_RESOURCE).stream()
                    .map(JsonObject.class::cast)
                    .collect(Collectors.toList());
        }

        if (media == null) {
            media = arrayFromFile(SAMPLE_MEDIA_RESOURCE).stream()
                    .map(JsonObject.class::cast)
                    .collect(Collectors.toList());
        }
    }

    @Override
    public SearchService search(SearchCriteria criteria, Handler<AsyncResult<List<SearchResult>>> handler) {
        vertx.executeBlocking(future -> this.performSearch(criteria, future), handler);
        return this;
    }

    protected void performSearch(SearchCriteria criteria, Future<List<SearchResult>> future) {
        SearchCriteria.Source source = criteria.getSource();
        List<JsonObject> books = new ArrayList<>();
        List<JsonObject> media = new ArrayList<>();

        if (source == SearchCriteria.Source.all || source == SearchCriteria.Source.books) {
            books = findBooks(criteria);
        }

        if (source == SearchCriteria.Source.all || source == SearchCriteria.Source.media) {
            media = findMedia(criteria);
        }

        books.addAll(media);
        List<SearchResult> results = books.stream()
                .sorted(getComparator(criteria.getSortField(), criteria.getSortOrder()))
                .map(SearchResult::new)
                .collect(Collectors.toList());
        future.complete(results);
    }

    protected List<JsonObject> findMedia(SearchCriteria criteria) {
        return extractSample(criteria, media);
    }

    protected List<JsonObject> findBooks(SearchCriteria criteria) {
        return extractSample(criteria, books);
    }

    protected List<JsonObject> extractSample(SearchCriteria criteria, List<JsonObject> sample) {
        List<JsonObject> copy = new ArrayList<>(sample);
        Collections.shuffle(copy);
        return copy.stream()
                .limit(criteria.getMaxResultsPerSource())
                .map(JsonObject::copy)
                .peek(media -> {
                    media.put("term", criteria.getTerm());
                    if (!criteria.isIncludeMeta()) {
                        media.remove("meta");
                    }

                })
                .collect(Collectors.toList());
    }


    /**
     * Get the content of the resource at path as a {@link JsonArray}
     *
     * @param path the path of the resource
     * @return the resource as json array. an empty array if an error occured
     */
    JsonArray arrayFromFile(String path) {
        try {
            return new JsonArray(readFileFromClassPath(path));
        } catch (Exception e) {
            return new JsonArray();
        }
    }

    /**
     * Read the file from classpath resources and cache it for further calls to avoid reading resource everytime.
     * File is extracted manually to avoid vertx file cache to occur as feature can be explicitly disabled by user.
     *
     * @return the file as a string
     */
    String readFileFromClassPath(String path) throws IOException {
        if (!RESOURCES.containsKey(path)) {
            RESOURCES.put(path, readFromInputStream(ClassLoader.getSystemResourceAsStream(path)));
        }

        return RESOURCES.get(path);
    }

    /**
     * Extract text as String from the provided input stream.
     *
     * @param inputStream the stream to read text
     * @return the text extracted from the input stream
     * @throws IOException If any error occurs. Should never happen as only call
     */
    String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }
}

