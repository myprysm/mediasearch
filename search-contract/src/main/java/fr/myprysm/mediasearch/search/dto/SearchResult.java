package fr.myprysm.mediasearch.search.dto;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@DataObject(generateConverter = true)
public class SearchResult {

    private String type;
    private String author;
    private String title;
    private String term;
    private JsonObject meta;

    public SearchResult(SearchResult other) {
        type = other.type;
        author = other.author;
        title = other.title;
        term = other.term;
        meta = other.meta;
    }

    public SearchResult(JsonObject json) {
        SearchResultConverter.fromJson(json, this);
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        SearchResultConverter.toJson(this, json);
        return json;
    }
}
