import ceylon.json {
  JsonObject=Object,
  JsonArray=Array,
  parse
}
import io.vertx.lang.ceylon {
  BaseDataObject,
  Converter,
  ToJava
}
import ceylon.collection {
  HashMap
}
import fr.myprysm.mediasearch.search.dto {
  SearchCriteria_=SearchCriteria
}
import io.vertx.core.json {
  JsonObject_=JsonObject,
  JsonArray_=JsonArray
}
/* Generated from fr.myprysm.mediasearch.search.dto.SearchCriteria */
shared class SearchCriteria(
  shared String? country = null,
  shared Boolean? includeMeta = null,
  shared Integer? maxResultsPerSource = null,
  shared Boolean? prettyPrint = null,
  shared String? sortField = null,
  shared String? sortOrder = null,
  shared String? source = null,
  shared String? term = null) satisfies BaseDataObject {
  shared actual default JsonObject toJson() {
    value json = JsonObject();
    if (exists country) {
      json.put("country", country);
    }
    if (exists includeMeta) {
      json.put("includeMeta", includeMeta);
    }
    if (exists maxResultsPerSource) {
      json.put("maxResultsPerSource", maxResultsPerSource);
    }
    if (exists prettyPrint) {
      json.put("prettyPrint", prettyPrint);
    }
    if (exists sortField) {
      json.put("sortField", sortField);
    }
    if (exists sortOrder) {
      json.put("sortOrder", sortOrder);
    }
    if (exists source) {
      json.put("source", source);
    }
    if (exists term) {
      json.put("term", term);
    }
    return json;
  }
}

shared object searchCriteria {

  shared SearchCriteria fromJson(JsonObject json) {
    String? country = json.getStringOrNull("country");
    Boolean? includeMeta = json.getBooleanOrNull("includeMeta");
    Integer? maxResultsPerSource = json.getIntegerOrNull("maxResultsPerSource");
    Boolean? prettyPrint = json.getBooleanOrNull("prettyPrint");
    String? sortField = json.getStringOrNull("sortField");
    String? sortOrder = json.getStringOrNull("sortOrder");
    String? source = json.getStringOrNull("source");
    String? term = json.getStringOrNull("term");
    return SearchCriteria {
      country = country;
      includeMeta = includeMeta;
      maxResultsPerSource = maxResultsPerSource;
      prettyPrint = prettyPrint;
      sortField = sortField;
      sortOrder = sortOrder;
      source = source;
      term = term;
    };
  }

  shared object toCeylon extends Converter<SearchCriteria_, SearchCriteria>() {
    shared actual SearchCriteria convert(SearchCriteria_ src) {
      value json = parse(src.toJson().string);
      assert(is JsonObject json);
      return fromJson(json);
    }
  }

  shared object toJava extends Converter<SearchCriteria, SearchCriteria_>() {
    shared actual SearchCriteria_ convert(SearchCriteria src) {
      // Todo : make optimized version without json
      value json = JsonObject_(src.toJson().string);
      value ret = SearchCriteria_(json);
      return ret;
    }
  }
  shared JsonObject toJson(SearchCriteria obj) => obj.toJson();
}
