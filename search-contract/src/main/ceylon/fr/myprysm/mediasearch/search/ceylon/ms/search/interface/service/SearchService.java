package fr.myprysm.mediasearch.search.ceylon.ms.search.interface.service;

import com.redhat.ceylon.compiler.java.metadata.Ceylon;
import com.redhat.ceylon.compiler.java.metadata.TypeInfo;
import com.redhat.ceylon.compiler.java.metadata.TypeParameter;
import com.redhat.ceylon.compiler.java.metadata.TypeParameters;
import com.redhat.ceylon.compiler.java.metadata.Variance;
import com.redhat.ceylon.compiler.java.metadata.Ignore;
import com.redhat.ceylon.compiler.java.metadata.Name;
import com.redhat.ceylon.compiler.java.runtime.model.TypeDescriptor;
import com.redhat.ceylon.compiler.java.runtime.model.ReifiedType;
import ceylon.language.Callable;
import ceylon.language.DocAnnotation$annotation$;
import java.util.List;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

@Ceylon(major = 8)
public class SearchService implements ReifiedType {

  @Ignore
  public static final io.vertx.lang.ceylon.ConverterFactory<fr.myprysm.mediasearch.search.service.SearchService, SearchService> TO_CEYLON = new io.vertx.lang.ceylon.ConverterFactory<fr.myprysm.mediasearch.search.service.SearchService, SearchService>() {
    public io.vertx.lang.ceylon.Converter<fr.myprysm.mediasearch.search.service.SearchService, SearchService> converter(final TypeDescriptor... descriptors) {
      return new io.vertx.lang.ceylon.Converter<fr.myprysm.mediasearch.search.service.SearchService, SearchService>() {
        public SearchService convert(fr.myprysm.mediasearch.search.service.SearchService src) {
          return new SearchService(src);
        }
      };
    }
  };

  @Ignore
  public static final io.vertx.lang.ceylon.Converter<SearchService, fr.myprysm.mediasearch.search.service.SearchService> TO_JAVA = new io.vertx.lang.ceylon.Converter<SearchService, fr.myprysm.mediasearch.search.service.SearchService>() {
    public fr.myprysm.mediasearch.search.service.SearchService convert(SearchService src) {
      return src.delegate;
    }
  };

  @Ignore public static final TypeDescriptor $TypeDescriptor$ = new io.vertx.lang.ceylon.VertxTypeDescriptor(TypeDescriptor.klass(SearchService.class), fr.myprysm.mediasearch.search.service.SearchService.class, TO_JAVA, TO_CEYLON);
  @Ignore private final fr.myprysm.mediasearch.search.service.SearchService delegate;

  public SearchService(fr.myprysm.mediasearch.search.service.SearchService delegate) {
    this.delegate = delegate;
  }

  @Ignore 
  public TypeDescriptor $getType$() {
    return $TypeDescriptor$;
  }

  @Ignore
  public Object getDelegate() {
    return delegate;
  }

  @DocAnnotation$annotation$(description = " Search content by term\n")
  @TypeInfo("fr.myprysm.mediasearch.search.ceylon.ms.search.interface.service::SearchService")
  public SearchService search(
    final @TypeInfo("fr.myprysm.mediasearch.search.ceylon.ms.search.interface.dto::SearchCriteria") @Name("criteria")@DocAnnotation$annotation$(description = "the criteria\n") fr.myprysm.mediasearch.search.ceylon.ms.search.interface.dto.SearchCriteria criteria, 
    final @TypeInfo("ceylon.language::Anything(ceylon.language::Throwable|ceylon.language::List<fr.myprysm.mediasearch.search.ceylon.ms.search.interface.dto::SearchResult>)") @Name("handler")@DocAnnotation$annotation$(description = "the result handler\n") Callable<?> handler) {
    fr.myprysm.mediasearch.search.dto.SearchCriteria arg_0 = criteria == null ? null : new fr.myprysm.mediasearch.search.dto.SearchCriteria(io.vertx.lang.ceylon.ToJava.JsonObject.convert(criteria.toJson()));
    io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.search.dto.SearchResult>>> arg_1 = handler == null ? null : new io.vertx.lang.ceylon.CallableAsyncResultHandler<java.util.List<fr.myprysm.mediasearch.search.dto.SearchResult>>(handler) {
      public Object toCeylon(java.util.List<fr.myprysm.mediasearch.search.dto.SearchResult> event) {
        return io.vertx.lang.ceylon.ToCeylon.convertList(fr.myprysm.mediasearch.search.ceylon.ms.search.interface.dto.SearchResult.$TypeDescriptor$, event, fr.myprysm.mediasearch.search.ceylon.ms.search.interface.dto.searchResult_.get_().getToCeylon());
      }
    };
    SearchService ret = fr.myprysm.mediasearch.search.ceylon.ms.search.interface.service.SearchService.TO_CEYLON.converter().safeConvert(delegate.search(arg_0, arg_1));
    return this;
  }

}
