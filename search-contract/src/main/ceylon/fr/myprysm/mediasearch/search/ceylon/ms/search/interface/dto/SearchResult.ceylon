import ceylon.json {
  JsonObject=Object,
  JsonArray=Array,
  parse
}
import io.vertx.lang.ceylon {
  BaseDataObject,
  Converter,
  ToJava
}
import ceylon.collection {
  HashMap
}
import fr.myprysm.mediasearch.search.dto {
  SearchResult_=SearchResult
}
import io.vertx.core.json {
  JsonObject_=JsonObject,
  JsonArray_=JsonArray
}
/* Generated from fr.myprysm.mediasearch.search.dto.SearchResult */
shared class SearchResult(
  shared String? author = null,
  shared JsonObject? meta = null,
  shared String? term = null,
  shared String? title = null,
  shared String? type = null) satisfies BaseDataObject {
  shared actual default JsonObject toJson() {
    value json = JsonObject();
    if (exists author) {
      json.put("author", author);
    }
    if (exists meta) {
      json.put("meta", meta);
    }
    if (exists term) {
      json.put("term", term);
    }
    if (exists title) {
      json.put("title", title);
    }
    if (exists type) {
      json.put("type", type);
    }
    return json;
  }
}

shared object searchResult {

  shared SearchResult fromJson(JsonObject json) {
    String? author = json.getStringOrNull("author");
    JsonObject? meta = json.getObjectOrNull("meta");
    String? term = json.getStringOrNull("term");
    String? title = json.getStringOrNull("title");
    String? type = json.getStringOrNull("type");
    return SearchResult {
      author = author;
      meta = meta;
      term = term;
      title = title;
      type = type;
    };
  }

  shared object toCeylon extends Converter<SearchResult_, SearchResult>() {
    shared actual SearchResult convert(SearchResult_ src) {
      value json = parse(src.toJson().string);
      assert(is JsonObject json);
      return fromJson(json);
    }
  }

  shared object toJava extends Converter<SearchResult, SearchResult_>() {
    shared actual SearchResult_ convert(SearchResult src) {
      // Todo : make optimized version without json
      value json = JsonObject_(src.toJson().string);
      value ret = SearchResult_(json);
      return ret;
    }
  }
  shared JsonObject toJson(SearchResult obj) => obj.toJson();
}
