require 'vertx/util/utils.rb'
# Generated from fr.myprysm.mediasearch.search.service.SearchService
module MsSearchInterface
  class SearchService
    # @private
    # @param j_del [::MsSearchInterface::SearchService] the java delegate
    def initialize(j_del)
      @j_del = j_del
    end
    # @private
    # @return [::MsSearchInterface::SearchService] the underlying java delegate
    def j_del
      @j_del
    end
    @@j_api_type = Object.new
    def @@j_api_type.accept?(obj)
      obj.class == SearchService
    end
    def @@j_api_type.wrap(obj)
      SearchService.new(obj)
    end
    def @@j_api_type.unwrap(obj)
      obj.j_del
    end
    def self.j_api_type
      @@j_api_type
    end
    def self.j_class
      Java::FrMyprysmMediasearchSearchService::SearchService.java_class
    end
    #  Search content by term
    # @param [Hash] criteria the criteria
    # @yield the result handler
    # @return [self]
    def search(criteria=nil)
      if criteria.class == Hash && block_given?
        @j_del.java_method(:search, [Java::FrMyprysmMediasearchSearchDto::SearchCriteria.java_class,Java::IoVertxCore::Handler.java_class]).call(Java::FrMyprysmMediasearchSearchDto::SearchCriteria.new(::Vertx::Util::Utils.to_json_object(criteria)),(Proc.new { |ar| yield(ar.failed ? ar.cause : nil, ar.succeeded ? ar.result.to_a.map { |elt| elt != nil ? JSON.parse(elt.toJson.encode) : nil } : nil) }))
        return self
      end
      raise ArgumentError, "Invalid arguments when calling search(#{criteria})"
    end
  end
end
