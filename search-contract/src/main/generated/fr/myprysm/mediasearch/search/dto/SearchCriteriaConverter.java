/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package fr.myprysm.mediasearch.search.dto;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link fr.myprysm.mediasearch.search.dto.SearchCriteria}.
 *
 * NOTE: This class has been automatically generated from the {@link fr.myprysm.mediasearch.search.dto.SearchCriteria} original class using Vert.x codegen.
 */
public class SearchCriteriaConverter {

  public static void fromJson(JsonObject json, SearchCriteria obj) {
    if (json.getValue("country") instanceof String) {
      obj.setCountry((String)json.getValue("country"));
    }
    if (json.getValue("includeMeta") instanceof Boolean) {
      obj.setIncludeMeta((Boolean)json.getValue("includeMeta"));
    }
    if (json.getValue("maxResultsPerSource") instanceof Number) {
      obj.setMaxResultsPerSource(((Number)json.getValue("maxResultsPerSource")).intValue());
    }
    if (json.getValue("prettyPrint") instanceof Boolean) {
      obj.setPrettyPrint((Boolean)json.getValue("prettyPrint"));
    }
    if (json.getValue("sortField") instanceof String) {
      obj.setSortField(fr.myprysm.mediasearch.search.dto.SearchCriteria.SortField.valueOf((String)json.getValue("sortField")));
    }
    if (json.getValue("sortOrder") instanceof String) {
      obj.setSortOrder(fr.myprysm.mediasearch.search.dto.SearchCriteria.SortOrder.valueOf((String)json.getValue("sortOrder")));
    }
    if (json.getValue("source") instanceof String) {
      obj.setSource(fr.myprysm.mediasearch.search.dto.SearchCriteria.Source.valueOf((String)json.getValue("source")));
    }
    if (json.getValue("term") instanceof String) {
      obj.setTerm((String)json.getValue("term"));
    }
  }

  public static void toJson(SearchCriteria obj, JsonObject json) {
    if (obj.getCountry() != null) {
      json.put("country", obj.getCountry());
    }
    json.put("includeMeta", obj.isIncludeMeta());
    if (obj.getMaxResultsPerSource() != null) {
      json.put("maxResultsPerSource", obj.getMaxResultsPerSource());
    }
    json.put("prettyPrint", obj.isPrettyPrint());
    if (obj.getSortField() != null) {
      json.put("sortField", obj.getSortField().name());
    }
    if (obj.getSortOrder() != null) {
      json.put("sortOrder", obj.getSortOrder().name());
    }
    if (obj.getSource() != null) {
      json.put("source", obj.getSource().name());
    }
    if (obj.getTerm() != null) {
      json.put("term", obj.getTerm());
    }
  }
}