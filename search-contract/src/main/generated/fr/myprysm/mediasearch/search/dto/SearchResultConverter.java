/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package fr.myprysm.mediasearch.search.dto;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link fr.myprysm.mediasearch.search.dto.SearchResult}.
 *
 * NOTE: This class has been automatically generated from the {@link fr.myprysm.mediasearch.search.dto.SearchResult} original class using Vert.x codegen.
 */
public class SearchResultConverter {

  public static void fromJson(JsonObject json, SearchResult obj) {
    if (json.getValue("author") instanceof String) {
      obj.setAuthor((String)json.getValue("author"));
    }
    if (json.getValue("meta") instanceof JsonObject) {
      obj.setMeta(((JsonObject)json.getValue("meta")).copy());
    }
    if (json.getValue("term") instanceof String) {
      obj.setTerm((String)json.getValue("term"));
    }
    if (json.getValue("title") instanceof String) {
      obj.setTitle((String)json.getValue("title"));
    }
    if (json.getValue("type") instanceof String) {
      obj.setType((String)json.getValue("type"));
    }
  }

  public static void toJson(SearchResult obj, JsonObject json) {
    if (obj.getAuthor() != null) {
      json.put("author", obj.getAuthor());
    }
    if (obj.getMeta() != null) {
      json.put("meta", obj.getMeta());
    }
    if (obj.getTerm() != null) {
      json.put("term", obj.getTerm());
    }
    if (obj.getTitle() != null) {
      json.put("title", obj.getTitle());
    }
    if (obj.getType() != null) {
      json.put("type", obj.getType());
    }
  }
}