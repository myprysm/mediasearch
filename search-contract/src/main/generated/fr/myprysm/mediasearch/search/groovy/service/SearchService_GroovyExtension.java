package fr.myprysm.mediasearch.search.groovy.service;
public class SearchService_GroovyExtension {
  public static fr.myprysm.mediasearch.search.service.SearchService search(fr.myprysm.mediasearch.search.service.SearchService j_receiver, java.util.Map<String, Object> criteria, io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<java.util.Map<String, Object>>>> handler) {
    io.vertx.core.impl.ConversionHelper.fromObject(j_receiver.search(criteria != null ? new fr.myprysm.mediasearch.search.dto.SearchCriteria(io.vertx.core.impl.ConversionHelper.toJsonObject(criteria)) : null,
      handler != null ? new io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.search.dto.SearchResult>>>() {
      public void handle(io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.search.dto.SearchResult>> ar) {
        handler.handle(ar.map(event -> event != null ? event.stream().map(elt -> elt != null ? io.vertx.core.impl.ConversionHelper.fromJsonObject(elt.toJson()) : null).collect(java.util.stream.Collectors.toList()) : null));
      }
    } : null));
    return j_receiver;
  }
}
