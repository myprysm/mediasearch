/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package fr.myprysm.mediasearch.search.reactivex.service;

import java.util.Map;
import io.reactivex.Observable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import java.util.List;
import fr.myprysm.mediasearch.search.dto.SearchCriteria;
import io.vertx.core.AsyncResult;
import fr.myprysm.mediasearch.search.dto.SearchResult;
import io.vertx.core.Handler;


@io.vertx.lang.reactivex.RxGen(fr.myprysm.mediasearch.search.service.SearchService.class)
public class SearchService {

  @Override
  public String toString() {
    return delegate.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SearchService that = (SearchService) o;
    return delegate.equals(that.delegate);
  }
  
  @Override
  public int hashCode() {
    return delegate.hashCode();
  }

  public static final io.vertx.lang.reactivex.TypeArg<SearchService> __TYPE_ARG = new io.vertx.lang.reactivex.TypeArg<>(
    obj -> new SearchService((fr.myprysm.mediasearch.search.service.SearchService) obj),
    SearchService::getDelegate
  );

  private final fr.myprysm.mediasearch.search.service.SearchService delegate;
  
  public SearchService(fr.myprysm.mediasearch.search.service.SearchService delegate) {
    this.delegate = delegate;
  }

  public fr.myprysm.mediasearch.search.service.SearchService getDelegate() {
    return delegate;
  }

  /**
   * Search content by term
   * @param criteria the criteria
   * @param handler the result handler
   * @return this
   */
  public SearchService search(SearchCriteria criteria, Handler<AsyncResult<List<SearchResult>>> handler) { 
    delegate.search(criteria, handler);
    return this;
  }

  /**
   * Search content by term
   * @param criteria the criteria
   * @return 
   */
  public Single<List<SearchResult>> rxSearch(SearchCriteria criteria) { 
    return new io.vertx.reactivex.core.impl.AsyncResultSingle<List<SearchResult>>(handler -> {
      search(criteria, handler);
    });
  }


  public static  SearchService newInstance(fr.myprysm.mediasearch.search.service.SearchService arg) {
    return arg != null ? new SearchService(arg) : null;
  }
}
