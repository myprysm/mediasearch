package fr.myprysm.mediasearch.search.service;

import fr.myprysm.mediasearch.search.dto.SearchCriteria;
import fr.myprysm.vertx.test.VertxTest;
import io.vertx.core.Vertx;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SampleSearchServiceTest implements VertxTest {

    @Test
    @DisplayName("SampleSearchService should serve results")
    void itShouldServeResults(Vertx vertx, VertxTestContext ctx) {
        Checkpoint cp = ctx.checkpoint(2);
        SearchService.createSample(vertx, ctx.succeeding(service -> {
            service.search(new SearchCriteria().setTerm("Some term").setMaxResultsPerSource(10), ctx.succeeding(media -> {
                ctx.verify(() -> {
                    assertThat(media.size()).isEqualTo(20);
                    assertThat(media.get(0).getTerm()).isEqualTo("Some term");
                });
                cp.flag();
            }));

            service.search(new SearchCriteria().setTerm("Another term"), ctx.succeeding(media -> {
                ctx.verify(() -> {
                    assertThat(media.size()).isEqualTo(10);
                    assertThat(media.get(0).getTerm()).isEqualTo("Another term");
                });
                cp.flag();
            }));
        }));

    }
}