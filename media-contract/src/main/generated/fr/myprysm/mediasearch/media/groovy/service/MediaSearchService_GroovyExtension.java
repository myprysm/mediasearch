package fr.myprysm.mediasearch.media.groovy.service;
public class MediaSearchService_GroovyExtension {
  public static fr.myprysm.mediasearch.media.service.MediaSearchService findByTerm(fr.myprysm.mediasearch.media.service.MediaSearchService j_receiver, java.lang.String term, io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<java.util.Map<String, Object>>>> handler) {
    io.vertx.core.impl.ConversionHelper.fromObject(j_receiver.findByTerm(term,
      handler != null ? new io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.media.dto.Media>>>() {
      public void handle(io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.media.dto.Media>> ar) {
        handler.handle(ar.map(event -> event != null ? event.stream().map(elt -> elt != null ? io.vertx.core.impl.ConversionHelper.fromJsonObject(elt.toJson()) : null).collect(java.util.stream.Collectors.toList()) : null));
      }
    } : null));
    return j_receiver;
  }
  public static fr.myprysm.mediasearch.media.service.MediaSearchService search(fr.myprysm.mediasearch.media.service.MediaSearchService j_receiver, java.util.Map<String, Object> criteria, io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<java.util.Map<String, Object>>>> handler) {
    io.vertx.core.impl.ConversionHelper.fromObject(j_receiver.search(criteria != null ? new fr.myprysm.mediasearch.media.dto.MediaCriteria(io.vertx.core.impl.ConversionHelper.toJsonObject(criteria)) : null,
      handler != null ? new io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.media.dto.Media>>>() {
      public void handle(io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.media.dto.Media>> ar) {
        handler.handle(ar.map(event -> event != null ? event.stream().map(elt -> elt != null ? io.vertx.core.impl.ConversionHelper.fromJsonObject(elt.toJson()) : null).collect(java.util.stream.Collectors.toList()) : null));
      }
    } : null));
    return j_receiver;
  }
}
