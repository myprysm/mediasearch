/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package fr.myprysm.mediasearch.media.reactivex.service;

import java.util.Map;
import io.reactivex.Observable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import java.util.List;
import fr.myprysm.mediasearch.media.dto.Media;
import fr.myprysm.mediasearch.media.dto.MediaCriteria;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;


@io.vertx.lang.reactivex.RxGen(fr.myprysm.mediasearch.media.service.MediaSearchService.class)
public class MediaSearchService {

  @Override
  public String toString() {
    return delegate.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MediaSearchService that = (MediaSearchService) o;
    return delegate.equals(that.delegate);
  }
  
  @Override
  public int hashCode() {
    return delegate.hashCode();
  }

  public static final io.vertx.lang.reactivex.TypeArg<MediaSearchService> __TYPE_ARG = new io.vertx.lang.reactivex.TypeArg<>(
    obj -> new MediaSearchService((fr.myprysm.mediasearch.media.service.MediaSearchService) obj),
    MediaSearchService::getDelegate
  );

  private final fr.myprysm.mediasearch.media.service.MediaSearchService delegate;
  
  public MediaSearchService(fr.myprysm.mediasearch.media.service.MediaSearchService delegate) {
    this.delegate = delegate;
  }

  public fr.myprysm.mediasearch.media.service.MediaSearchService getDelegate() {
    return delegate;
  }

  /**
   * Search media by term (full text search)
   * <p>
   * This will use the default parameters configured in the service.
   * For a more fine grained search please check {@link fr.myprysm.mediasearch.media.reactivex.service.MediaSearchService#search}.
   * @param term the full text search
   * @param handler result handler
   * @return this
   */
  public MediaSearchService findByTerm(String term, Handler<AsyncResult<List<Media>>> handler) { 
    delegate.findByTerm(term, handler);
    return this;
  }

  /**
   * Search media by term (full text search)
   * <p>
   * This will use the default parameters configured in the service.
   * For a more fine grained search please check {@link fr.myprysm.mediasearch.media.reactivex.service.MediaSearchService#search}.
   * @param term the full text search
   * @return 
   */
  public Single<List<Media>> rxFindByTerm(String term) { 
    return new io.vertx.reactivex.core.impl.AsyncResultSingle<List<Media>>(handler -> {
      findByTerm(term, handler);
    });
  }

  /**
   * Search media by term
   * @param criteria 
   * @param handler 
   * @return 
   */
  public MediaSearchService search(MediaCriteria criteria, Handler<AsyncResult<List<Media>>> handler) { 
    delegate.search(criteria, handler);
    return this;
  }

  /**
   * Search media by term
   * @param criteria 
   * @return 
   */
  public Single<List<Media>> rxSearch(MediaCriteria criteria) { 
    return new io.vertx.reactivex.core.impl.AsyncResultSingle<List<Media>>(handler -> {
      search(criteria, handler);
    });
  }


  public static  MediaSearchService newInstance(fr.myprysm.mediasearch.media.service.MediaSearchService arg) {
    return arg != null ? new MediaSearchService(arg) : null;
  }
}
