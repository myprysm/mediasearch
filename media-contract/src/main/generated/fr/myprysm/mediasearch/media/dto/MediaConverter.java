/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package fr.myprysm.mediasearch.media.dto;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link fr.myprysm.mediasearch.media.dto.Media}.
 *
 * NOTE: This class has been automatically generated from the {@link fr.myprysm.mediasearch.media.dto.Media} original class using Vert.x codegen.
 */
public class MediaConverter {

  public static void fromJson(JsonObject json, Media obj) {
    if (json.getValue("artistId") instanceof Number) {
      obj.setArtistId(((Number)json.getValue("artistId")).intValue());
    }
    if (json.getValue("artistName") instanceof String) {
      obj.setArtistName((String)json.getValue("artistName"));
    }
    if (json.getValue("collectionId") instanceof Number) {
      obj.setCollectionId(((Number)json.getValue("collectionId")).intValue());
    }
    if (json.getValue("collectionName") instanceof String) {
      obj.setCollectionName((String)json.getValue("collectionName"));
    }
    if (json.getValue("id") instanceof String) {
      obj.setId((String)json.getValue("id"));
    }
    if (json.getValue("kind") instanceof String) {
      obj.setKind((String)json.getValue("kind"));
    }
    if (json.getValue("term") instanceof String) {
      obj.setTerm((String)json.getValue("term"));
    }
    if (json.getValue("trackId") instanceof Number) {
      obj.setTrackId(((Number)json.getValue("trackId")).intValue());
    }
    if (json.getValue("trackName") instanceof String) {
      obj.setTrackName((String)json.getValue("trackName"));
    }
    if (json.getValue("type") instanceof String) {
      obj.setType((String)json.getValue("type"));
    }
  }

  public static void toJson(Media obj, JsonObject json) {
    if (obj.getArtistId() != null) {
      json.put("artistId", obj.getArtistId());
    }
    if (obj.getArtistName() != null) {
      json.put("artistName", obj.getArtistName());
    }
    if (obj.getCollectionId() != null) {
      json.put("collectionId", obj.getCollectionId());
    }
    if (obj.getCollectionName() != null) {
      json.put("collectionName", obj.getCollectionName());
    }
    if (obj.getId() != null) {
      json.put("id", obj.getId());
    }
    if (obj.getKind() != null) {
      json.put("kind", obj.getKind());
    }
    if (obj.getTerm() != null) {
      json.put("term", obj.getTerm());
    }
    if (obj.getTrackId() != null) {
      json.put("trackId", obj.getTrackId());
    }
    if (obj.getTrackName() != null) {
      json.put("trackName", obj.getTrackName());
    }
    if (obj.getType() != null) {
      json.put("type", obj.getType());
    }
  }
}