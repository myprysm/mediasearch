/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/** @module ms-music-interface-js/media_search_service */
var utils = require('vertx-js/util/utils');

var io = Packages.io;
var JsonObject = io.vertx.core.json.JsonObject;
var JMediaSearchService = Java.type('fr.myprysm.mediasearch.media.service.MediaSearchService');
var Media = Java.type('fr.myprysm.mediasearch.media.dto.Media');
var MediaCriteria = Java.type('fr.myprysm.mediasearch.media.dto.MediaCriteria');

/**
 @class
*/
var MediaSearchService = function(j_val) {

  var j_mediaSearchService = j_val;
  var that = this;

  /**
   Search media by term (full text search)
   <p>
   This will use the default parameters configured in the service.
   For a more fine grained search please check {@link MediaSearchService#search}.

   @public
   @param term {string} the full text search 
   @param handler {function} result handler 
   @return {MediaSearchService} this
   */
  this.findByTerm = function(term, handler) {
    var __args = arguments;
    if (__args.length === 2 && typeof __args[0] === 'string' && typeof __args[1] === 'function') {
      j_mediaSearchService["findByTerm(java.lang.String,io.vertx.core.Handler)"](term, function(ar) {
      if (ar.succeeded()) {
        handler(utils.convReturnListSetDataObject(ar.result()), null);
      } else {
        handler(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**
   Search media by term

   @public
   @param criteria {Object} 
   @param handler {function} 
   @return {MediaSearchService} 
   */
  this.search = function(criteria, handler) {
    var __args = arguments;
    if (__args.length === 2 && (typeof __args[0] === 'object' && __args[0] != null) && typeof __args[1] === 'function') {
      j_mediaSearchService["search(fr.myprysm.mediasearch.media.dto.MediaCriteria,io.vertx.core.Handler)"](criteria != null ? new MediaCriteria(new JsonObject(Java.asJSONCompatible(criteria))) : null, function(ar) {
      if (ar.succeeded()) {
        handler(utils.convReturnListSetDataObject(ar.result()), null);
      } else {
        handler(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  // A reference to the underlying Java delegate
  // NOTE! This is an internal API and must not be used in user code.
  // If you rely on this property your code is likely to break if we change it / remove it without warning.
  this._jdel = j_mediaSearchService;
};

MediaSearchService._jclass = utils.getJavaClass("fr.myprysm.mediasearch.media.service.MediaSearchService");
MediaSearchService._jtype = {
  accept: function(obj) {
    return MediaSearchService._jclass.isInstance(obj._jdel);
  },
  wrap: function(jdel) {
    var obj = Object.create(MediaSearchService.prototype, {});
    MediaSearchService.apply(obj, arguments);
    return obj;
  },
  unwrap: function(obj) {
    return obj._jdel;
  }
};
MediaSearchService._create = function(jdel) {
  var obj = Object.create(MediaSearchService.prototype, {});
  MediaSearchService.apply(obj, arguments);
  return obj;
}
module.exports = MediaSearchService;