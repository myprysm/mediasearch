package fr.myprysm.mediasearch.media.kotlin.dto

import fr.myprysm.mediasearch.media.dto.Media

fun Media(
        artistId: Int? = null,
        artistName: String? = null,
        collectionId: Int? = null,
        collectionName: String? = null,
        id: String? = null,
        kind: String? = null,
        term: String? = null,
        trackId: Int? = null,
        trackName: String? = null,
        type: String? = null): Media = fr.myprysm.mediasearch.media.dto.Media().apply {

    if (artistId != null) {
        this.setArtistId(artistId)
    }
    if (artistName != null) {
        this.setArtistName(artistName)
    }
    if (collectionId != null) {
        this.setCollectionId(collectionId)
    }
    if (collectionName != null) {
        this.setCollectionName(collectionName)
    }
    if (id != null) {
        this.setId(id)
    }
    if (kind != null) {
        this.setKind(kind)
    }
    if (term != null) {
        this.setTerm(term)
    }
    if (trackId != null) {
        this.setTrackId(trackId)
    }
    if (trackName != null) {
        this.setTrackName(trackName)
    }
    if (type != null) {
        this.setType(type)
    }
}

