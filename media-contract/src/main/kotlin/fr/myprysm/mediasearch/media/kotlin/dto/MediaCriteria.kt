package fr.myprysm.mediasearch.media.kotlin.dto

import fr.myprysm.mediasearch.media.dto.MediaCriteria
import fr.myprysm.mediasearch.media.dto.MediaCriteria.Entity
import fr.myprysm.mediasearch.media.dto.MediaCriteria.Media

fun MediaCriteria(
        country: String? = null,
        entity: Entity? = null,
        maxResults: Int? = null,
        media: Media? = null,
        prettyPrint: Boolean? = null,
        term: String? = null): MediaCriteria = fr.myprysm.mediasearch.media.dto.MediaCriteria().apply {

    if (country != null) {
        this.setCountry(country)
    }
    if (entity != null) {
        this.setEntity(entity)
    }
    if (maxResults != null) {
        this.setMaxResults(maxResults)
    }
    if (media != null) {
        this.setMedia(media)
    }
    if (prettyPrint != null) {
        this.setPrettyPrint(prettyPrint)
    }
    if (term != null) {
        this.setTerm(term)
    }
}

