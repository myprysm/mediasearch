/*
 * Copyright 2018 the original author or the original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.myprysm.mediasearch.media.service;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;

public class MediaSearchServiceException extends ServiceException {
    public static final int EMPTY_TERM = 1;
    public static final int REMOTE_ERROR = 9;

    public MediaSearchServiceException(int failureCode, String message) {
        super(failureCode, message);
    }

    public MediaSearchServiceException(ServiceException exc) {
        this(exc.failureCode(), exc.getMessage());
    }

    public MediaSearchServiceException(int failureCode, String message, JsonObject debugInfo) {
        super(failureCode, message, debugInfo);
    }

    /**
     * Create a failed Future containing a ServiceException.
     *
     * @param failureCode The failure code.
     * @param message     The failure message.
     * @param <T>         The type of the AsyncResult.
     * @return A failed Future containing the ServiceException.
     */
    public static <T> AsyncResult<T> fail(int failureCode, String message) {
        return Future.failedFuture(new MediaSearchServiceException(failureCode, message));
    }
}
