package fr.myprysm.mediasearch.media.service;

import fr.myprysm.mediasearch.media.dto.Media;
import fr.myprysm.mediasearch.media.dto.MediaCriteria;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class SampleMediaSearchService implements MediaSearchService {
    private static final Map<String, String> RESOURCES = new HashMap<>();
    private static final String SAMPLE_RESOURCE = "sample/media.json";

    private static final MediaCriteria defaultCriteria = new MediaCriteria()
            .setMaxResults(5);
    private static List<Media> media = new ArrayList<>();
    private final Vertx vertx;

    public SampleMediaSearchService(Vertx vertx, Handler<AsyncResult<MediaSearchService>> handler) {
        this.vertx = vertx;
        this.vertx.executeBlocking(future -> {
            initialize();
            future.complete();
        }, result -> handler.handle(Future.succeededFuture(this)));

    }

    private void initialize() {
        if (media.size() == 0) {
            media = arrayFromFile(SAMPLE_RESOURCE).stream()
                    .map(JsonObject.class::cast)
                    .map(Media::new)
                    .collect(Collectors.toList());
        }
    }

    @Override
    public MediaSearchService findByTerm(String term, Handler<AsyncResult<List<Media>>> handler) {
        return search(new MediaCriteria(defaultCriteria).setTerm(term), handler);
    }

    @Override
    public MediaSearchService search(MediaCriteria criteria, Handler<AsyncResult<List<Media>>> handler) {
        vertx.executeBlocking(future -> {
            List<Media> copy = new ArrayList<>(media);
            Collections.shuffle(copy);
            future.complete(copy.stream().limit(criteria.getMaxResults()).map(Media::new).map(media -> media.setTerm(criteria.getTerm())).collect(Collectors.toList()));
        }, handler);
        return this;
    }

    /**
     * Get the content of the resource at path as a {@link JsonArray}
     *
     * @param path the path of the resource
     * @return the resource as json array. an empty array if an error occured
     */
    JsonArray arrayFromFile(String path) {
        try {
            return new JsonArray(readFileFromClassPath(path));
        } catch (Exception e) {
            return new JsonArray();
        }
    }

    /**
     * Read the file from classpath resources and cache it for further calls to avoid reading resource everytime.
     * File is extracted manually to avoid vertx file cache to occur as feature can be explicitly disabled by user.
     *
     * @return the file as a string
     */
    String readFileFromClassPath(String path) throws IOException {
        if (!RESOURCES.containsKey(path)) {
            RESOURCES.put(path, readFromInputStream(ClassLoader.getSystemResourceAsStream(path)));
        }

        return RESOURCES.get(path);
    }

    /**
     * Extract text as String from the provided input stream.
     *
     * @param inputStream the stream to read text
     * @return the text extracted from the input stream
     * @throws IOException If any error occurs. Should never happen as only call
     */
    String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

}
