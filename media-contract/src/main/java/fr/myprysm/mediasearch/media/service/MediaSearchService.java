package fr.myprysm.mediasearch.media.service;

import fr.myprysm.mediasearch.media.dto.Media;
import fr.myprysm.mediasearch.media.dto.MediaCriteria;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.GenIgnore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

import java.util.List;

@VertxGen
@ProxyGen
public interface MediaSearchService {

    /**
     * Generates the sample service bundled with the contract
     *
     * @param vertx   the vertx instance
     * @param handler the handler
     * @return the service, not yet initialized (use handler)
     */
    @GenIgnore
    static MediaSearchService createSample(Vertx vertx, Handler<AsyncResult<MediaSearchService>> handler) {
        return new SampleMediaSearchService(vertx, handler);
    }

    /**
     * Search media by term (full text search)
     * <p>
     * This will use the default parameters configured in the service.
     * For a more fine grained search please check {@link #search(MediaCriteria, Handler)}.
     *
     * @param term    the full text search
     * @param handler result handler
     * @return this
     */
    @Fluent
    MediaSearchService findByTerm(String term, Handler<AsyncResult<List<Media>>> handler);


    /**
     * Search media by term
     *
     * @param criteria
     * @param handler
     * @return
     */
    @Fluent
    MediaSearchService search(MediaCriteria criteria, Handler<AsyncResult<List<Media>>> handler);
}
