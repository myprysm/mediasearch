package fr.myprysm.mediasearch.media.dto;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@DataObject(generateConverter = true)
public class MediaCriteria {

    private static final String DEFAULT_COUNTRY = "US";
    private Integer maxResults = 10;
    private Media media = Media.music;
    private Entity entity = Entity.album;
    private String country = DEFAULT_COUNTRY;
    private String term;
    private Boolean prettyPrint = false;

    public MediaCriteria(MediaCriteria other) {
        setMaxResults(other.maxResults);
        setCountry(other.country);
        setMedia(other.media);
        setEntity(other.entity);
        setTerm(other.term);
        setPrettyPrint(other.prettyPrint);
    }

    public MediaCriteria(JsonObject json) {
        MediaCriteriaConverter.fromJson(json, this);
    }

    public MediaCriteria setMaxResults(Integer maxResults) {
        if (maxResults != null) {
            if (maxResults < 1) {
                maxResults = 1;
            }

            if (maxResults > 200) {
                maxResults = 200;
            }
        }
        this.maxResults = maxResults;
        return this;
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        MediaCriteriaConverter.toJson(this, json);
        return json;
    }

    public enum Media {
        // Music(-related)
        music, musicVideo,
        // Other
//        movie, podcast, audiobook, shortFilm, tvShow, software, ebook, all
    }

    public enum Entity {
        // Music
        music, musicArtist, musicTrack, album, musicVideo, mix, song,

        // Other
//        movieArtist, podcastAuth,
//        audiobookAuthor, audiobook,
//        shortFilmArtist, shortFilm,
//        tvEpisode, tvSeason,
//        software, iPadSoftware, macSoftware,
//        ebook, movie, allArtist, podcast, allTrack
    }

}
