package fr.myprysm.mediasearch.media.dto;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@DataObject(generateConverter = true)
public class Media {

    private String id;
    private String type;
    private String kind;
    private String term;

    private Integer artistId;
    private String artistName;
    private Integer collectionId;
    private String collectionName;
    private Integer trackId;
    private String trackName;

    public Media(Media other) {
        id = other.id;
        type = other.type;
        kind = other.kind;
        term = other.term;

        artistId = other.artistId;
        collectionId = other.collectionId;
        trackId = other.trackId;

        artistName = other.artistName;
        collectionName = other.collectionName;
        trackName = other.trackName;
    }

    public Media(JsonObject json) {
        MediaConverter.fromJson(json, this);
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        MediaConverter.toJson(this, json);
        return json;
    }
}
