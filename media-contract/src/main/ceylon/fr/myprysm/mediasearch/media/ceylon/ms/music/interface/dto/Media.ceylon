import fr.myprysm.mediasearch.media.dto {
  Media_=Media
}
import ceylon.json {
  JsonObject=Object,
  JsonArray=Array,
  parse
}
import io.vertx.lang.ceylon {
  BaseDataObject,
  Converter,
  ToJava
}
import ceylon.collection {
  HashMap
}
import io.vertx.core.json {
  JsonObject_=JsonObject,
  JsonArray_=JsonArray
}
/* Generated from fr.myprysm.mediasearch.media.dto.Media */
shared class Media(
  shared Integer? artistId = null,
  shared String? artistName = null,
  shared Integer? collectionId = null,
  shared String? collectionName = null,
  shared String? id = null,
  shared String? kind = null,
  shared String? term = null,
  shared Integer? trackId = null,
  shared String? trackName = null,
  shared String? type = null) satisfies BaseDataObject {
  shared actual default JsonObject toJson() {
    value json = JsonObject();
    if (exists artistId) {
      json.put("artistId", artistId);
    }
    if (exists artistName) {
      json.put("artistName", artistName);
    }
    if (exists collectionId) {
      json.put("collectionId", collectionId);
    }
    if (exists collectionName) {
      json.put("collectionName", collectionName);
    }
    if (exists id) {
      json.put("id", id);
    }
    if (exists kind) {
      json.put("kind", kind);
    }
    if (exists term) {
      json.put("term", term);
    }
    if (exists trackId) {
      json.put("trackId", trackId);
    }
    if (exists trackName) {
      json.put("trackName", trackName);
    }
    if (exists type) {
      json.put("type", type);
    }
    return json;
  }
}

shared object media {

  shared Media fromJson(JsonObject json) {
    Integer? artistId = json.getIntegerOrNull("artistId");
    String? artistName = json.getStringOrNull("artistName");
    Integer? collectionId = json.getIntegerOrNull("collectionId");
    String? collectionName = json.getStringOrNull("collectionName");
    String? id = json.getStringOrNull("id");
    String? kind = json.getStringOrNull("kind");
    String? term = json.getStringOrNull("term");
    Integer? trackId = json.getIntegerOrNull("trackId");
    String? trackName = json.getStringOrNull("trackName");
    String? type = json.getStringOrNull("type");
    return Media {
      artistId = artistId;
      artistName = artistName;
      collectionId = collectionId;
      collectionName = collectionName;
      id = id;
      kind = kind;
      term = term;
      trackId = trackId;
      trackName = trackName;
      type = type;
    };
  }

  shared object toCeylon extends Converter<Media_, Media>() {
    shared actual Media convert(Media_ src) {
      value json = parse(src.toJson().string);
      assert(is JsonObject json);
      return fromJson(json);
    }
  }

  shared object toJava extends Converter<Media, Media_>() {
    shared actual Media_ convert(Media src) {
      // Todo : make optimized version without json
      value json = JsonObject_(src.toJson().string);
      value ret = Media_(json);
      return ret;
    }
  }
  shared JsonObject toJson(Media obj) => obj.toJson();
}
