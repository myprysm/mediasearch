import fr.myprysm.mediasearch.media.dto {
  MediaCriteria_=MediaCriteria
}
import ceylon.json {
  JsonObject=Object,
  JsonArray=Array,
  parse
}
import io.vertx.lang.ceylon {
  BaseDataObject,
  Converter,
  ToJava
}
import ceylon.collection {
  HashMap
}
import io.vertx.core.json {
  JsonObject_=JsonObject,
  JsonArray_=JsonArray
}
/* Generated from fr.myprysm.mediasearch.media.dto.MediaCriteria */
shared class MediaCriteria(
  shared String? country = null,
  shared String? entity = null,
  shared Integer? maxResults = null,
  shared String? media = null,
  shared Boolean? prettyPrint = null,
  shared String? term = null) satisfies BaseDataObject {
  shared actual default JsonObject toJson() {
    value json = JsonObject();
    if (exists country) {
      json.put("country", country);
    }
    if (exists entity) {
      json.put("entity", entity);
    }
    if (exists maxResults) {
      json.put("maxResults", maxResults);
    }
    if (exists media) {
      json.put("media", media);
    }
    if (exists prettyPrint) {
      json.put("prettyPrint", prettyPrint);
    }
    if (exists term) {
      json.put("term", term);
    }
    return json;
  }
}

shared object mediaCriteria {

  shared MediaCriteria fromJson(JsonObject json) {
    String? country = json.getStringOrNull("country");
    String? entity = json.getStringOrNull("entity");
    Integer? maxResults = json.getIntegerOrNull("maxResults");
    String? media = json.getStringOrNull("media");
    Boolean? prettyPrint = json.getBooleanOrNull("prettyPrint");
    String? term = json.getStringOrNull("term");
    return MediaCriteria {
      country = country;
      entity = entity;
      maxResults = maxResults;
      media = media;
      prettyPrint = prettyPrint;
      term = term;
    };
  }

  shared object toCeylon extends Converter<MediaCriteria_, MediaCriteria>() {
    shared actual MediaCriteria convert(MediaCriteria_ src) {
      value json = parse(src.toJson().string);
      assert(is JsonObject json);
      return fromJson(json);
    }
  }

  shared object toJava extends Converter<MediaCriteria, MediaCriteria_>() {
    shared actual MediaCriteria_ convert(MediaCriteria src) {
      // Todo : make optimized version without json
      value json = JsonObject_(src.toJson().string);
      value ret = MediaCriteria_(json);
      return ret;
    }
  }
  shared JsonObject toJson(MediaCriteria obj) => obj.toJson();
}
