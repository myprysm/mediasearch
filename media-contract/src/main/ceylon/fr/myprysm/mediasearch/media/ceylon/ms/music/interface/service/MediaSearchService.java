package fr.myprysm.mediasearch.media.ceylon.ms.music.interface.service;

import com.redhat.ceylon.compiler.java.metadata.Ceylon;
import com.redhat.ceylon.compiler.java.metadata.TypeInfo;
import com.redhat.ceylon.compiler.java.metadata.TypeParameter;
import com.redhat.ceylon.compiler.java.metadata.TypeParameters;
import com.redhat.ceylon.compiler.java.metadata.Variance;
import com.redhat.ceylon.compiler.java.metadata.Ignore;
import com.redhat.ceylon.compiler.java.metadata.Name;
import com.redhat.ceylon.compiler.java.runtime.model.TypeDescriptor;
import com.redhat.ceylon.compiler.java.runtime.model.ReifiedType;
import ceylon.language.Callable;
import ceylon.language.DocAnnotation$annotation$;
import java.util.List;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

@Ceylon(major = 8)
public class MediaSearchService implements ReifiedType {

  @Ignore
  public static final io.vertx.lang.ceylon.ConverterFactory<fr.myprysm.mediasearch.media.service.MediaSearchService, MediaSearchService> TO_CEYLON = new io.vertx.lang.ceylon.ConverterFactory<fr.myprysm.mediasearch.media.service.MediaSearchService, MediaSearchService>() {
    public io.vertx.lang.ceylon.Converter<fr.myprysm.mediasearch.media.service.MediaSearchService, MediaSearchService> converter(final TypeDescriptor... descriptors) {
      return new io.vertx.lang.ceylon.Converter<fr.myprysm.mediasearch.media.service.MediaSearchService, MediaSearchService>() {
        public MediaSearchService convert(fr.myprysm.mediasearch.media.service.MediaSearchService src) {
          return new MediaSearchService(src);
        }
      };
    }
  };

  @Ignore
  public static final io.vertx.lang.ceylon.Converter<MediaSearchService, fr.myprysm.mediasearch.media.service.MediaSearchService> TO_JAVA = new io.vertx.lang.ceylon.Converter<MediaSearchService, fr.myprysm.mediasearch.media.service.MediaSearchService>() {
    public fr.myprysm.mediasearch.media.service.MediaSearchService convert(MediaSearchService src) {
      return src.delegate;
    }
  };

  @Ignore public static final TypeDescriptor $TypeDescriptor$ = new io.vertx.lang.ceylon.VertxTypeDescriptor(TypeDescriptor.klass(MediaSearchService.class), fr.myprysm.mediasearch.media.service.MediaSearchService.class, TO_JAVA, TO_CEYLON);
  @Ignore private final fr.myprysm.mediasearch.media.service.MediaSearchService delegate;

  public MediaSearchService(fr.myprysm.mediasearch.media.service.MediaSearchService delegate) {
    this.delegate = delegate;
  }

  @Ignore 
  public TypeDescriptor $getType$() {
    return $TypeDescriptor$;
  }

  @Ignore
  public Object getDelegate() {
    return delegate;
  }

  @DocAnnotation$annotation$(description = " Search media by term (full text search)\n <p>\n This will use the default parameters configured in the service.\n For a more fine grained search please check [search](../service/MediaSearchService.type.html#search).\n")
  @TypeInfo("fr.myprysm.mediasearch.media.ceylon.ms.music.interface.service::MediaSearchService")
  public MediaSearchService findByTerm(
    final @TypeInfo("ceylon.language::String") @Name("term")@DocAnnotation$annotation$(description = "the full text search\n") ceylon.language.String term, 
    final @TypeInfo("ceylon.language::Anything(ceylon.language::Throwable|ceylon.language::List<fr.myprysm.mediasearch.media.ceylon.ms.music.interface.dto::Media>)") @Name("handler")@DocAnnotation$annotation$(description = "result handler\n") Callable<?> handler) {
    java.lang.String arg_0 = io.vertx.lang.ceylon.ToJava.String.safeConvert(term);
    io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.media.dto.Media>>> arg_1 = handler == null ? null : new io.vertx.lang.ceylon.CallableAsyncResultHandler<java.util.List<fr.myprysm.mediasearch.media.dto.Media>>(handler) {
      public Object toCeylon(java.util.List<fr.myprysm.mediasearch.media.dto.Media> event) {
        return io.vertx.lang.ceylon.ToCeylon.convertList(fr.myprysm.mediasearch.media.ceylon.ms.music.interface.dto.Media.$TypeDescriptor$, event, fr.myprysm.mediasearch.media.ceylon.ms.music.interface.dto.media_.get_().getToCeylon());
      }
    };
    MediaSearchService ret = fr.myprysm.mediasearch.media.ceylon.ms.music.interface.service.MediaSearchService.TO_CEYLON.converter().safeConvert(delegate.findByTerm(arg_0, arg_1));
    return this;
  }

  @DocAnnotation$annotation$(description = " Search media by term\n")
  @TypeInfo("fr.myprysm.mediasearch.media.ceylon.ms.music.interface.service::MediaSearchService")
  public MediaSearchService search(
    final @TypeInfo("fr.myprysm.mediasearch.media.ceylon.ms.music.interface.dto::MediaCriteria") @Name("criteria") fr.myprysm.mediasearch.media.ceylon.ms.music.interface.dto.MediaCriteria criteria, 
    final @TypeInfo("ceylon.language::Anything(ceylon.language::Throwable|ceylon.language::List<fr.myprysm.mediasearch.media.ceylon.ms.music.interface.dto::Media>)") @Name("handler") Callable<?> handler) {
    fr.myprysm.mediasearch.media.dto.MediaCriteria arg_0 = criteria == null ? null : new fr.myprysm.mediasearch.media.dto.MediaCriteria(io.vertx.lang.ceylon.ToJava.JsonObject.convert(criteria.toJson()));
    io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.media.dto.Media>>> arg_1 = handler == null ? null : new io.vertx.lang.ceylon.CallableAsyncResultHandler<java.util.List<fr.myprysm.mediasearch.media.dto.Media>>(handler) {
      public Object toCeylon(java.util.List<fr.myprysm.mediasearch.media.dto.Media> event) {
        return io.vertx.lang.ceylon.ToCeylon.convertList(fr.myprysm.mediasearch.media.ceylon.ms.music.interface.dto.Media.$TypeDescriptor$, event, fr.myprysm.mediasearch.media.ceylon.ms.music.interface.dto.media_.get_().getToCeylon());
      }
    };
    MediaSearchService ret = fr.myprysm.mediasearch.media.ceylon.ms.music.interface.service.MediaSearchService.TO_CEYLON.converter().safeConvert(delegate.search(arg_0, arg_1));
    return this;
  }

}
