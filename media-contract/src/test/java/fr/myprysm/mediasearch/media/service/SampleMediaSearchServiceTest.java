package fr.myprysm.mediasearch.media.service;

import fr.myprysm.mediasearch.media.dto.MediaCriteria;
import fr.myprysm.vertx.test.VertxTest;
import io.vertx.core.Vertx;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SampleMediaSearchServiceTest implements VertxTest {

    @Test
    @DisplayName("SampleMediaSearchService should serve results")
    void itShouldServeResults(Vertx vertx, VertxTestContext ctx) {
        Checkpoint cp = ctx.checkpoint(2);
        new SampleMediaSearchService(vertx, ctx.succeeding(service -> {
            service.search(new MediaCriteria().setTerm("Some term").setMaxResults(10), ctx.succeeding(media -> {
                ctx.verify(() -> {
                    assertThat(media.size()).isEqualTo(10);
                    assertThat(media.get(0).getTerm()).isEqualTo("Some term");
                });
                cp.flag();
            }));

            service.findByTerm("Some other term", ctx.succeeding(media -> {
                ctx.verify(() -> {
                    assertThat(media.size()).isEqualTo(5);
                    assertThat(media.get(0).getTerm()).isEqualTo("Some other term");
                });
                cp.flag();
            }));
        }));

    }
}