package fr.myprysm.mediasearch.media.service.retrofit;

import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

import java.util.Map;

public interface ITunesSearchService {
    /**
     * Search books on Google Play Books API
     *
     * @param params the search params
     * @return the response
     */
    @GET("search")
    Single<Response<JsonObject>> search(@QueryMap Map<String, String> params);
}
