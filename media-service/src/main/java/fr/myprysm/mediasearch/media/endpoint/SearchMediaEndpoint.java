package fr.myprysm.mediasearch.media.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.myprysm.mediasearch.media.dto.Media;
import fr.myprysm.mediasearch.media.dto.MediaCriteria;
import fr.myprysm.mediasearch.media.service.MediaSearchService;
import fr.myprysm.mediasearch.media.service.MediaSearchServiceException;
import fr.myprysm.mediasearch.media.service.SampleMediaSearchService;
import fr.myprysm.vertx.core.reactivex.config.ConfigService;
import fr.myprysm.vertx.core.utils.Utils;
import fr.myprysm.vertx.json.Json;
import fr.myprysm.vertx.web.AbstractOpenAPIEndpoint;
import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.api.RequestParameter;
import io.vertx.ext.web.api.RequestParameters;
import io.vertx.reactivex.circuitbreaker.CircuitBreaker;
import io.vertx.reactivex.core.Future;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.servicediscovery.ServiceDiscovery;
import io.vertx.reactivex.servicediscovery.types.EventBusService;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static fr.myprysm.pipeline.util.JsonHelpers.obj;

@Slf4j
public class SearchMediaEndpoint extends AbstractOpenAPIEndpoint {
    private static final JsonObject SERVICE_FILTER = new JsonObject().put("service.interface", MediaSearchService.class.getName());
    private static final String CIRCUIT_BREAKER_CONFIG = "search.media.endpoint.circuitBreaker";
    private static final String DEFAULT_CRITERIA_CONFIG = "search.media.service.default";

    private static final String BREAKER_GET_SERVICE = "media-service-endpoint:get-service:" + Utils.instanceId;
    private static final String BREAKER_HANDLE_SEARCH = "media-service-endpoint:handle-search:" + Utils.instanceId;
    private static final String SERVICE_DISCOVERY_NAME = "search-media-endpoint:" + Utils.instanceId;

    private ServiceDiscovery discovery;
    private CircuitBreaker breakerHandleSearch;
    private CircuitBreaker breakerGetService;
    private ConfigService configProxy;
    private MediaCriteria defaults;

    private MediaSearchService fallbackService;

    @Override
    public String operationId() {
        return "searchMedia";
    }

    @Override
    public void handle(RoutingContext event) {
        RequestParameters params = event.get("parsedParameters");
        setContentJson(event);
        MediaCriteria criteria = handleParams(params);
        breakerGetService
                .rxExecuteCommand(this::getService)
                .flatMap(this.search(criteria))
                .onErrorResumeNext(this.fallback(criteria))
                .map(this.processFields(params))
                .subscribe(this.writeResponse(event, criteria), err -> event.response().end("[]"));
    }

    private Single<List<Media>> fallback(MediaCriteria criteria) {
        return Single.create(emitter -> fallbackService.search(criteria, ar -> {
            if (ar.succeeded()) {
                emitter.onSuccess(ar.result());
            } else {
                emitter.onError(ar.cause());
            }
        }));
    }


    private void getService(Future<MediaSearchService> getService) {
        EventBusService.getServiceProxyWithJsonFilter(discovery, SERVICE_FILTER, MediaSearchService.class, ar -> {
            if (ar.succeeded()) {
                MediaSearchService service = ar.result();
                getService.complete(service);
            } else {
                log.error("Unable to discover service [media-search-service]", ar.cause());
                if (ar.cause() instanceof MediaSearchServiceException && ((MediaSearchServiceException) ar.cause()).getDebugInfo() != null) {
                    log.error("Debug info: ", ((MediaSearchServiceException) ar.cause()).getDebugInfo());
                }
                getService.fail(ar.cause());
            }
        });
    }

    @SuppressWarnings("Duplicates")
    private Function<MediaSearchService, Single<List<Media>>> search(MediaCriteria criteria) {
        return service -> breakerHandleSearch.rxExecuteCommand(media -> {
            //noinspection unchecked
            service.search(criteria, ar -> {
                if (ar.succeeded()) {
                    media.complete(ar.result());
                } else {
                    log.error("Unable to get medias", ar.cause());
                    media.fail(ar.cause());
                }
                ServiceDiscovery.releaseServiceObject(discovery, service);
            });
        });
    }


    //region Map fields

    /**
     * Process the requested fields for each book.
     * <p>
     * Default is minimal.
     * Whenever the "fields" field is present with at least one value, then the mapper is discovered.
     *
     * @param params the request parameters
     * @return the mapper
     */
    private Function<List<Media>, List<JsonObject>> processFields(RequestParameters params) {
        java.util.function.Function<Media, Media> mapper;
        if (params.queryParameter("fields") != null && params.queryParameter("fields").getArray().size() > 0) {
            Set<String> fields = params.queryParameter("fields").getArray().stream().map(RequestParameter::getString).collect(Collectors.toSet());
            mapper = this.getMapper(fields);
        } else {
            mapper = this::minimalMapper;
        }

        return books -> books.stream()
                .map(mapper)
                .map(Media::toJson)
                .collect(Collectors.toList());
    }

    /**
     * Get the appropriate mapper to fit the requested fields.
     * When "full" is requested, identity is returned.
     * When "minimal" is requested, minimal mapper is returned.
     * When the fields set size is equal to 6, then identity is returned (everything is requested).
     * Otherwise a custom mapper will copy field by field the book to match expected response.
     *
     * @param fields the fields to map
     * @return the mapper
     */
    private java.util.function.Function<Media, Media> getMapper(Set<String> fields) {
        if (fields.contains("full")) {
            return java.util.function.Function.identity();
        } else if (fields.contains("minimal")) {
            return this::minimalMapper;
        } else if (fields.size() == 7) {
            return java.util.function.Function.identity();
        } else {
            return this.customMapper(fields);
        }
    }

    /**
     * Returns a custom mapper that will build a new book with the minimal information + requested fields.
     *
     * @param fields the fields to add to the new book
     * @return the mapper
     */
    private java.util.function.Function<Media, Media> customMapper(Set<String> fields) {
        return media -> {
            Media mapped = this.minimalMapper(media);
            if (fields.contains("trackName")) {
                mapped.setTrackName(media.getTrackName());
            }

            if (fields.contains("term")) {
                mapped.setTerm(media.getTerm());
            }

            if (fields.contains("type")) {
                mapped.setType(media.getType());
            }

            if (fields.contains("kind")) {
                mapped.setKind(media.getKind());
            }

            if (fields.contains("artistId")) {
                mapped.setArtistId(media.getArtistId());
            }

            if (fields.contains("collectionId")) {
                mapped.setCollectionId(media.getCollectionId());
            }

            if (fields.contains("trackId")) {
                mapped.setTrackId(media.getTrackId());
            }

            return mapped;
        };
    }

    /**
     * Minimal mapper adding only artistName and collectionName.
     *
     * @param media the media to map
     * @return the mapped media
     */
    private Media minimalMapper(Media media) {
        return new Media().setArtistName(media.getArtistName()).setCollectionName(media.getCollectionName());
    }

    //endregion

    private Consumer<List<JsonObject>> writeResponse(RoutingContext event, MediaCriteria criteria) {
        return medias -> {
            ObjectMapper mapper = criteria.getPrettyPrint() ? Json.prettyMapper : Json.mapper;
            event.response().end(mapper.writeValueAsString(medias));
        };
    }

    private MediaCriteria handleParams(RequestParameters params) {
        MediaCriteria criteria = new MediaCriteria(defaults);
        criteria.setTerm(params.queryParameter("term").getString());

        if (params.queryParameter("maxResults") != null) {
            criteria.setMaxResults(params.queryParameter("maxResults").getInteger());
        }

        if (params.queryParameter("country") != null) {
            criteria.setCountry(params.queryParameter("country").getString());
        }

        if (params.queryParameter("entity") != null) {
            criteria.setEntity(fromEnum(params.queryParameter("entity"), MediaCriteria.Entity.class));
        }

        if (params.queryParameter("media") != null) {
            criteria.setMedia(fromEnum(params.queryParameter("media"), MediaCriteria.Media.class));
        }

        if (params.queryParameter("prettyPrint") != null) {
            criteria.setPrettyPrint(params.queryParameter("prettyPrint").getBoolean());
        }
        return criteria;
    }

    private <T extends Enum<T>> T fromEnum(RequestParameter param, Class<T> mediaClass) {
        return Enum.valueOf(mediaClass, param.getString());
    }

    //region Configuration
    @Override
    public Completable configure() {
        configProxy = Utils.configServiceProxy(vertx());
        return Completable.concatArray(
                Completable.create(this::prepareFallbackService),
                configureDiscovery(),
                configureCircuitBreaker(),
                configureDefaultCriteria()
        );
    }

    private void prepareFallbackService(CompletableEmitter emitter) {
        fallbackService = new SampleMediaSearchService(vertx().getDelegate(), service -> emitter.onComplete());
    }

    private Completable configureDefaultCriteria() {
        return configProxy
                .rxGetObject(DEFAULT_CRITERIA_CONFIG)
                .onErrorResumeNext(Single.just(obj()))
                .map(MediaCriteria::new)
                .doOnSuccess(this::setDefaults)
                .toCompletable();
    }

    private void setDefaults(MediaCriteria criteria) {
        defaults = criteria;
    }

    private Completable configureCircuitBreaker() {
        return configProxy
                .rxGetObject(CIRCUIT_BREAKER_CONFIG)
                .onErrorResumeNext(Single.just(obj()))
                .map(CircuitBreakerOptions::new)
                .doOnSuccess(this::prepareBreakers)
                .toCompletable();
    }

    private void prepareBreakers(CircuitBreakerOptions options) {
        breakerHandleSearch = CircuitBreaker.create(BREAKER_HANDLE_SEARCH, vertx(), options);
        breakerGetService = CircuitBreaker.create(BREAKER_GET_SERVICE, vertx(), options);
    }

    private Completable configureDiscovery() {
        return Single.<ServiceDiscovery>create(emitter -> ServiceDiscovery.create(vertx(), new ServiceDiscoveryOptions().setName(SERVICE_DISCOVERY_NAME), emitter::onSuccess))
                .doOnSuccess(this::setDiscovery)
                .toCompletable();
    }

    private void setDiscovery(ServiceDiscovery discovery) {
        this.discovery = discovery;
    }

    //endregion
}
