package fr.myprysm.mediasearch.media.service.impl;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.chainr.ChainrBuilder;
import com.google.common.collect.ImmutableList;
import fr.myprysm.mediasearch.media.dto.Media;
import fr.myprysm.mediasearch.media.dto.MediaCriteria;
import fr.myprysm.mediasearch.media.service.MediaSearchService;
import fr.myprysm.mediasearch.media.service.MediaSearchServiceException;
import fr.myprysm.mediasearch.media.service.MediaSearchServiceExceptionMessageCodec;
import fr.myprysm.mediasearch.media.service.retrofit.ITunesSearchService;
import fr.myprysm.pipeline.util.RoundRobin;
import fr.myprysm.pipeline.validation.ValidationResult;
import fr.myprysm.vertx.core.reactivex.config.ConfigService;
import fr.myprysm.vertx.core.utils.Utils;
import fr.myprysm.vertx.json.Json;
import fr.myprysm.vertx.retrofit.ServiceFactory;
import fr.myprysm.vertx.service.AbstractService;
import fr.myprysm.vertx.service.HealthCheck;
import io.reactivex.Completable;
import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.Status;
import io.vertx.reactivex.SingleHelper;
import io.vertx.reactivex.core.Future;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static fr.myprysm.mediasearch.media.service.MediaSearchServiceException.EMPTY_TERM;
import static fr.myprysm.mediasearch.media.service.MediaSearchServiceException.REMOTE_ERROR;
import static fr.myprysm.pipeline.util.JsonHelpers.arr;
import static fr.myprysm.pipeline.validation.JsonValidation.*;
import static io.reactivex.Completable.fromAction;

@Slf4j
public class ITunesMediaSearchServiceImpl extends AbstractService implements MediaSearchService, HealthCheck {
    public static final String SERVICE_PARAMS = "search.media.service";
    private static final Iterator<String> beats = RoundRobin.of(ImmutableList.of(
            "Rammstein", "Red Hot Chili Peppers", "God is an Astronaut", "Carbon Based Lifeforms", "Aes Dana"
    )).iterator();
    private String cacheAddress;
    private Chainr chainr;
    private MediaCriteria defaults;
    private ITunesSearchService searchService;
    private boolean shouldTransform;

    @Override
    public Completable configure() {
        tryRegisterCodec(MediaSearchServiceException.class, new MediaSearchServiceExceptionMessageCodec());
        ConfigService configProxy = Utils.configServiceProxy(vertx());
        return configProxy
                .rxGetObject(SERVICE_PARAMS)
                .flatMapCompletable(this::configureInternal);
    }

    private ValidationResult validate(JsonObject config) {
        return matches("baseUrl",
                "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]/",
                "this is not a valid baseUrl (must end with a \"/\")"
        )
                .and(isNull("default").or(isObject("default")))
                .and(isString("cacheAddress"))
                .and(isNull("mapping").or(arrayOf("mapping", JsonObject.class)))
                .apply(config);
    }

    private Completable configureInternal(JsonObject config) {
        ValidationResult result = validate(config);
        if (!result.isValid()) {
            return Completable.error(result.toException());
        }

        cacheAddress = config.getString("cacheAddress");

        return Completable.concatArray(
                fromAction(() -> prepareClient(config)),
                fromAction(() -> prepareDefault(config.getJsonObject("default"))),
                fromAction(() -> prepareMapper(config.getJsonArray("mapping", arr())))
        );
    }

    /**
     * Configures JOLT chainr to convert an iTunes Media object to a more reasonable object.
     *
     * @param mapping the list of JOLT transformations
     * @see com.bazaarvoice.jolt.Shiftr
     * @see Chainr
     */
    private void prepareMapper(JsonArray mapping) {
        if (mapping.size() > 0) {
            List<Map<String, Object>> rawSpecs = Json.convert(mapping);
            chainr = new ChainrBuilder(rawSpecs).build();
            shouldTransform = true;
        }
    }

    /**
     * Prepares the default criteria when using simple term search
     *
     * @param defaultParams the default params for search
     */
    private void prepareDefault(JsonObject defaultParams) {
        this.defaults = new MediaCriteria(defaultParams);
    }

    /**
     * Parses the URL and prepare the default request that will be reused by the service.
     *
     * @param config the service configuration
     */
    private void prepareClient(JsonObject config) {
        ServiceFactory factory = ServiceFactory.forBase(config.getString("baseUrl")).build();
        searchService = factory.create(ITunesSearchService.class);
    }

    @Override
    public MediaSearchService findByTerm(String term, Handler<AsyncResult<List<Media>>> handler) {
        return search(new MediaCriteria(defaults).setTerm(term), handler);
    }

    @Override
    public MediaSearchService search(MediaCriteria criteria, Handler<AsyncResult<List<Media>>> handler) {
        if (StringUtils.isNotBlank(criteria.getTerm())) {
            MediaCriteriaParams params = new MediaCriteriaParams(criteria);
            searchService.search(params.toMap())
                    .flatMap(this::handleStatus)
                    .map(this.handleResponse(params.getTerm()))
                    .subscribe(SingleHelper.toObserver(handler));
        } else {
            handler.handle(MediaSearchServiceException.fail(EMPTY_TERM, "Term is empty"));
        }
        return this;
    }

    private Function<JsonObject, List<Media>> handleResponse(String term) {
        return body -> Observable.fromIterable(body.getJsonArray("results", arr()))
                .map(JsonObject.class::cast)
                .map(addTerm(term))
                .map(this::prepareId)
                .flatMapSingle(this::performTransform)
                .doOnEach(this::cache)
                .map(Media::new)
                .collectInto(new ArrayList<Media>(), ArrayList::add).blockingGet();
    }

    /**
     * Prepares an identifier for indexing to avoid duplicates.
     * <p>
     * Uses all id information when available
     *
     * @param media the current media
     * @return the transformed media
     */
    private JsonObject prepareId(JsonObject media) {
        StringBuilder id = new StringBuilder();
        JsonObject copy = media.copy();
        boolean prev = false;
        if (copy.getInteger("artistId") != null) {
            id.append(copy.getInteger("artistId"));
            prev = true;
        }

        if (copy.getInteger("collectionId") != null) {
            if (prev) {
                id.append("-");
            }

            id.append(copy.getInteger("collectionId"));
            prev = true;
        }

        if (copy.getInteger("trackId") != null) {
            if (prev) {
                id.append("-");
            }

            id.append(copy.getInteger("trackId"));
        }

        copy.put("id", id);
        return copy;
    }

    /**
     * Handler that rejects all responses but OK.
     * When OK, then forwards the JSON body
     *
     * @param response the response
     * @return the body if OK
     */
    @SuppressWarnings("ConstantConditions")
    private Single<JsonObject> handleStatus(Response<JsonObject> response) {
        if (response.code() != 200) {
            log.error("Error occured during request.");
            if (log.isDebugEnabled()) {
                try {
                    log.debug("Response [{}], Body: [{}]", response, response.errorBody().string());
                } catch (IOException e) {
                    //
                }
            }
            return Single.error(new MediaSearchServiceException(REMOTE_ERROR, "Error from remote", response.body()));
        }

        return Single.just(response.body());
    }

    private Function<JsonObject, JsonObject> addTerm(String term) {
        return json -> json.put("term", term);
    }

    /**
     * Perform the iTunes media transformation to get a simpler version (see configuration.search.media.mapping)
     *
     * @param item the media
     * @return the asynchronously transformed item
     */
    @SuppressWarnings("unchecked")
    private Single<JsonObject> performTransform(JsonObject item) {
        return shouldTransform
                ? vertx().rxExecuteBlocking(handler -> handler.complete(transform(item)), true)
                : Single.just(item);
    }

    /**
     * Transform the iTunes Media to a simpler version (see configuration.search.media.mapping)
     *
     * @param item the media
     * @return the asynchronously transformed item
     */
    @SuppressWarnings("unchecked")
    private JsonObject transform(JsonObject item) {
        JsonObject mapped = new JsonObject((Map<String, Object>) chainr.transform(Json.convert(item)));

        // Small trick, album kind is returned as "Album" so put it in lowercase
        if (mapped.getValue("kind") instanceof String) {
            mapped.put("kind", mapped.getString("kind").toLowerCase());
        }

        return mapped;
    }

    /**
     * Emits an item to the cache pipeline
     *
     * @param notif the stream notification
     */
    private void cache(Notification<JsonObject> notif) {
        if (notif.isOnNext()) {
            vertx().eventBus().send(cacheAddress, notif.getValue());
        }
    }

    @Override
    public String name() {
        return "media-service";
    }

    @Override
    public Handler<Future<Status>> beat() {
        return status -> {
            MediaCriteria criteria = new MediaCriteria(defaults)
                    .setTerm(beats.next())
                    .setMaxResults(1);
            searchService.search(new MediaCriteriaParams(criteria).toMap())
                    .subscribe((res, err) -> {
                        if (!status.isComplete()) {
                            if (res != null) {
                                status.complete(res.code() == 200 ? Status.OK() : Status.KO());
                            } else {
                                status.complete(Status.KO());
                            }
                        }
                    });
        };
    }
}
