package fr.myprysm.mediasearch.media.service.impl;

import fr.myprysm.mediasearch.media.dto.MediaCriteria;

import java.util.HashMap;
import java.util.Map;

public class MediaCriteriaParams extends MediaCriteria {

    public MediaCriteriaParams(MediaCriteria other) {
        super(other);
    }

    public String getTerm() {
        return super.getTerm().trim();
    }

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>(5);
        map.put("term", getTerm());
        map.put("country", getCountry());
        map.put("media", getMedia().name());
        map.put("entity", getEntity().name());
        map.put("limit", String.valueOf(getMaxResults()));
        map.put("prettyPrint", String.valueOf(getPrettyPrint()));
        return map;
    }

}
