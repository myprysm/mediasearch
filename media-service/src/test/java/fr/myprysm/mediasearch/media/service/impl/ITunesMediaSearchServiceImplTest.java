package fr.myprysm.mediasearch.media.service.impl;

import com.github.tomakehurst.wiremock.WireMockServer;
import fr.myprysm.mediasearch.media.service.MediaSearchService;
import fr.myprysm.pipeline.validation.ValidationException;
import fr.myprysm.vertx.core.test.ConfigTestHelper;
import fr.myprysm.vertx.test.VertxTest;
import io.reactivex.Single;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.vertx.core.Vertx;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("iTunes Media Search Service tests")
class ITunesMediaSearchServiceImplTest implements VertxTest {

    // See resources/services/media/valid/test-config.yml
    private static final int PORT = 8765;
    private static final String BASE_PATH = "services/media/";

    private static final Action NOOP_ACTION = () -> {
    };

    private static final Consumer<MediaSearchService> NOOP_SUB = obj -> {

    };
    private static WireMockServer wiremockServer;

    @BeforeAll
    static void startWireMock() {
        wiremockServer = new WireMockServer(options()
                .port(PORT)
                .usingFilesUnderClasspath("services/media/wiremock"));
        wiremockServer.start();
    }

    @AfterAll
    static void stopWireMock() {
        wiremockServer.stop();
    }

    ITunesMediaSearchServiceImpl getService(Vertx vertx) {
        ITunesMediaSearchServiceImpl searchService = new ITunesMediaSearchServiceImpl();
        searchService.init(new io.vertx.reactivex.core.Vertx(vertx));
        return searchService;
    }

    Single<MediaSearchService> getServiceInitialized(Vertx vertx) {
        ITunesMediaSearchServiceImpl service = getService(vertx);
        return service.configure().andThen(Single.just(service));
    }

    @Test
    @DisplayName("Service should respond data")
    void itShouldRespondData(Vertx vertx, VertxTestContext ctx) {
        Checkpoint cp = ctx.checkpoint(1);
        ConfigTestHelper.load(vertx, ctx, path("valid/test-config.yml"), (_service, _consumer) -> {
            getServiceInitialized(vertx)
                    .doOnSuccess(service -> {
                        service.findByTerm("Rammstein", ctx.succeeding(result -> ctx.verify(() -> {
                            assertThat(result).hasSize(10);
                            cp.flag();
                        })));

                    })
                    .subscribe(NOOP_SUB, ctx::failNow);
        });

    }

    private String path(String resource) {
        return BASE_PATH + resource;
    }


    @Nested
    @SuppressWarnings("Duplicates")
    @DisplayName("Service does not start with invalid options")
    class InvalidOptionsTest {

        @Test
        @DisplayName("it should not start without url")
        void itShouldNotStartWithInvalidUrl(Vertx vertx, VertxTestContext ctx) {
            ConfigTestHelper.load(vertx, ctx, path("invalid/bad-url.yaml"), (service, consumer) -> {
                getService(vertx)
                        .configure()
                        .subscribe(NOOP_ACTION, err -> ctx.verify(() -> {
                            assertThat(err).isInstanceOf(ValidationException.class);
                            consumer.unregister();
                            ctx.completeNow();
                        }));
            });
        }

        @Test
        @DisplayName("it should not start without a cache address")
        void itShouldNotStartWithoutCacheAddress(Vertx vertx, VertxTestContext ctx) {
            ConfigTestHelper.load(vertx, ctx, path("invalid/no-cache-address.yaml"), (service, consumer) -> {
                getService(vertx)
                        .configure()
                        .subscribe(NOOP_ACTION, err -> ctx.verify(() -> {
                            assertThat(err).isInstanceOf(ValidationException.class);
                            consumer.unregister();
                            ctx.completeNow();
                        }));
            });
        }

        @Test
        @DisplayName("it should not start with a list of strings as mapping")
        void itShouldNotStartWithAListOfStringsAsMapping(Vertx vertx, VertxTestContext ctx) {
            ConfigTestHelper.load(vertx, ctx, path("invalid/bad-mapping-type.yaml"), (service, consumer) -> {
                getService(vertx)
                        .configure()
                        .subscribe(NOOP_ACTION, err -> ctx.verify(() -> {
                            assertThat(err).isInstanceOf(ValidationException.class);
                            consumer.unregister();
                            ctx.completeNow();
                        }));
            });
        }

        @Test
        @DisplayName("it should not start with an invalid mapping")
        void itShouldNotStartWithInvalidMapping(Vertx vertx, VertxTestContext ctx) {
            ConfigTestHelper.load(vertx, ctx, path("invalid/bad-mapping-specs-invalid.yaml"), (service, consumer) -> {
                getService(vertx)
                        .configure()
                        .subscribe(NOOP_ACTION, err -> ctx.verify(() -> {
                            assertThat(err).isInstanceOf(ValidationException.class);
                            consumer.unregister();
                            ctx.completeNow();
                        }));

            });
        }
    }
}