package fr.myprysm.mediasearch.media;

import com.github.tomakehurst.wiremock.WireMockServer;
import fr.myprysm.vertx.test.VertxTest;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static fr.myprysm.pipeline.util.JsonHelpers.obj;
import static org.assertj.core.api.Assertions.assertThat;

public class IntegrationTest implements VertxTest {

    private static final Integer PORT = 4001;
    private static final int WIREMOCK_PORT = 8765;
    private static WireMockServer wiremockServer;

    @BeforeAll
    static void startWireMock() {
        wiremockServer = new WireMockServer(options()
                .port(WIREMOCK_PORT)
                .usingFilesUnderClasspath("services/media/wiremock"));
        wiremockServer.start();
    }

    @AfterAll
    static void stopWireMock() {
        wiremockServer.stop();
    }

    void startApplication(String config, Vertx vertx, VertxTestContext ctx, Handler<String> handler) {
        DeploymentOptions options = new DeploymentOptions()
                .setConfig(obj().put("path", config));
        vertx.deployVerticle("fr.myprysm.vertx.core.StarterVerticle", options, ctx.succeeding(handler));
    }

    @Nested
    @DisplayName("It should serve requests from remote")
    class ServeRequestFromRemoteTest {
        private String deployment;

        @BeforeEach
        void start(Vertx vertx, VertxTestContext ctx) {
            startApplication("it-config.yml", vertx, ctx, id -> {
                deployment = id;
                ctx.completeNow();
            });
        }

        @AfterEach
        void stop(Vertx vertx, VertxTestContext ctx) {
            if (vertx.deploymentIDs().contains(deployment)) {
                vertx.undeploy(deployment, ctx.succeeding(zoid -> ctx.completeNow()));
            } else ctx.completeNow();
        }


        @Test
        @DisplayName("it should serve requests")
        void itShouldServeRequests(Vertx vertx, VertxTestContext ctx) {
            Checkpoint cp = ctx.checkpoint(5);

            HttpClient client = getClient(vertx);
            client.getNow("/media?term=Rammstein", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));

            client.getNow("/media?term=Rammstein&fields=artistId&maxResults=10&country=us&entity=album&media=music&prettyPrint=true", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));

            client.getNow("/media?term=Rammstein&fields=full", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));

            client.getNow("/media?term=Rammstein&fields=artistId", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));

            client.getNow("/media?term=Rammstein&fields=trackName,term,type,kind,collectionId,trackId", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));

        }

        private HttpClient getClient(Vertx vertx) {
            return vertx.createHttpClient(new HttpClientOptions().setDefaultPort(PORT));
        }
    }

    @Nested
    @DisplayName("It should serve requests from fallback when service faces an error")
    class ServeRequestFromFallbackTest {
        private String deployment;

        @BeforeEach
        void start(Vertx vertx, VertxTestContext ctx) {
            startApplication("fallback-config.yml", vertx, ctx, id -> {
                deployment = id;
                ctx.completeNow();
            });
        }

        @AfterEach
        void stop(Vertx vertx, VertxTestContext ctx) {
            if (vertx.deploymentIDs().contains(deployment)) {
                vertx.undeploy(deployment, ctx.succeeding(zoid -> ctx.completeNow()));
            } else ctx.completeNow();
        }


        @Test
        @DisplayName("it should serve requests")
        void itShouldServeRequests(Vertx vertx, VertxTestContext ctx) {
            Checkpoint cp = ctx.checkpoint(1);

            HttpClient client = getClient(vertx);
            client.getNow("/media?term=Rammstein", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));


            // Health Check is ko
            client.getNow("/__/hc", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isBetween(500, 503);
                cp.flag();
            }));

        }

        private HttpClient getClient(Vertx vertx) {
            return vertx.createHttpClient(new HttpClientOptions().setDefaultPort(PORT));
        }
    }

    @Nested
    @DisplayName("It should service request from fallback with no sevice defined")
    class ServeRequestNoServiceTest {
        private String deployment;

        @BeforeEach
        void start(Vertx vertx, VertxTestContext ctx) {
            startApplication("no-service-config.yml", vertx, ctx, id -> {
                deployment = id;
                ctx.completeNow();
            });
        }

        @AfterEach
        void stop(Vertx vertx, VertxTestContext ctx) {
            if (vertx.deploymentIDs().contains(deployment)) {
                vertx.undeploy(deployment, ctx.succeeding(zoid -> ctx.completeNow()));
            } else ctx.completeNow();
        }


        @Test
        @DisplayName("it should serve requests")
        void itShouldServeRequests(Vertx vertx, VertxTestContext ctx) {
            Checkpoint cp = ctx.checkpoint(1);

            HttpClient client = getClient(vertx);
            client.getNow("/media?term=Rammstein", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));

        }

        private HttpClient getClient(Vertx vertx) {
            return vertx.createHttpClient(new HttpClientOptions().setDefaultPort(PORT));
        }
    }
}


