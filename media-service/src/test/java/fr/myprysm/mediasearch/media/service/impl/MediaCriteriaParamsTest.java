package fr.myprysm.mediasearch.media.service.impl;

import com.google.common.collect.ImmutableMap;
import fr.myprysm.mediasearch.media.dto.MediaCriteria;
import fr.myprysm.vertx.test.VertxTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class MediaCriteriaParamsTest implements VertxTest {


    @Test
    @DisplayName("it should transform a criteria to a map")
    void itShouldTransformCriteriaToMap() {
        MediaCriteria criteria = new MediaCriteria()
                .setCountry("fr")
                .setMaxResults(10)
                .setEntity(MediaCriteria.Entity.album)
                .setMedia(MediaCriteria.Media.musicVideo)
                .setPrettyPrint(true)
                .setTerm("some term");
        MediaCriteriaParams params = new MediaCriteriaParams(criteria);


        Map<String, String> result = ImmutableMap.<String, String>builder()
                .put("country", "fr")
                .put("limit", "10")
                .put("media", "musicVideo")
                .put("entity", "album")
                .put("term", "some term")
                .put("prettyPrint", "true")
                .build();

        assertThat(params.toMap()).isEqualTo(result);
    }

}