#!/usr/bin/env bash

MODS="all"
DIR=$(dirname "$(readlink -f "$0")")

PREFIX=""
if [ "x$1" != "x" ]; then
    echo "using repository $1"
    PREFIX="$1/"
fi

SUFFIX=""
if [ "x$2" != "x" ]; then
    echo "pushing only tag $2"
    SUFFIX="${SUFFIX}"
fi



docker push ${PREFIX}myprysm/search-service${SUFFIX}
docker push ${PREFIX}myprysm/book-service${SUFFIX}
docker push ${PREFIX}myprysm/media-service${SUFFIX}
docker push ${PREFIX}myprysm/barrel${SUFFIX}