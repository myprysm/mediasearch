package fr.myprysm.mediasearch.book.service;

import fr.myprysm.mediasearch.book.dto.Book;
import fr.myprysm.mediasearch.book.dto.BookCriteria;
import fr.myprysm.vertx.service.AbstractService;
import io.reactivex.Completable;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

import java.util.List;

public class SampleBookSearchServiceWrapper extends AbstractService implements BookSearchService {

    private SampleBookSearchService delegate;

    @Override
    public Completable configure() {
        return Completable.create(emitter -> {
            delegate = new SampleBookSearchService(vertx().getDelegate(), ar -> {
                if (ar.succeeded()) {
                    emitter.onComplete();
                } else {
                    emitter.onError(ar.cause());
                }
            });
        });

    }

    @Override
    public BookSearchService findByTerm(String term, Handler<AsyncResult<List<Book>>> handler) {
        return delegate.findByTerm(term, handler);
    }

    @Override
    public BookSearchService search(BookCriteria criteria, Handler<AsyncResult<List<Book>>> handler) {
        return delegate.search(criteria, handler);
    }
}
