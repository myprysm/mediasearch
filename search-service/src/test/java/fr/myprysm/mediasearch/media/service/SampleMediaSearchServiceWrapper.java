package fr.myprysm.mediasearch.media.service;

import fr.myprysm.mediasearch.media.dto.Media;
import fr.myprysm.mediasearch.media.dto.MediaCriteria;
import fr.myprysm.vertx.service.AbstractService;
import io.reactivex.Completable;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

import java.util.List;

public class SampleMediaSearchServiceWrapper extends AbstractService implements MediaSearchService {

    SampleMediaSearchService delegate;

    @Override
    public Completable configure() {
        return Completable.create(emitter -> {
            delegate = new SampleMediaSearchService(vertx().getDelegate(), ar -> {
                if (ar.succeeded()) {
                    emitter.onComplete();
                } else {
                    emitter.onError(ar.cause());
                }
            });
        });

    }

    @Override
    public MediaSearchService findByTerm(String term, Handler<AsyncResult<List<Media>>> handler) {
        return delegate.findByTerm(term, handler);
    }

    @Override
    public MediaSearchService search(MediaCriteria criteria, Handler<AsyncResult<List<Media>>> handler) {
        return delegate.search(criteria, handler);
    }
}
