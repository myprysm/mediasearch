package fr.myprysm.mediasearch.search.service;

import fr.myprysm.mediasearch.search.dto.SearchCriteria;
import fr.myprysm.mediasearch.search.dto.SearchResult;
import fr.myprysm.vertx.service.AbstractService;
import io.reactivex.Completable;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

import java.util.List;

public class SampleSearchServiceWrapper extends AbstractService implements SearchService {

    private SampleSearchService delegate;

    @Override
    public Completable configure() {
        return Completable.create(emitter -> {
            delegate = new SampleSearchService(vertx().getDelegate(), ar -> {
                if (ar.succeeded()) {
                    emitter.onComplete();
                } else {
                    emitter.onError(ar.cause());
                }
            });
        });

    }

    @Override
    public SearchService search(SearchCriteria criteria, Handler<AsyncResult<List<SearchResult>>> handler) {
        return delegate.search(criteria, handler);
    }
}
