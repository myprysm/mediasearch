package fr.myprysm.mediasearch.search;

import fr.myprysm.vertx.test.VertxTest;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static fr.myprysm.vertx.json.JsonHelpers.obj;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("It should serve requests in any scenario")
public class IntegrationTest implements VertxTest {

    private static final Integer PORT = 4001;
    private String deployment;

    void startApplication(String config, Vertx vertx, VertxTestContext ctx, Handler<String> handler) {
        DeploymentOptions options = new DeploymentOptions().setConfig(obj().put("path", config));
        vertx.deployVerticle("fr.myprysm.vertx.core.StarterVerticle", options, ctx.succeeding(handler));
    }

    @AfterEach
    void stop(Vertx vertx, VertxTestContext ctx) {
        if (vertx.deploymentIDs().contains(deployment)) {
            vertx.undeploy(deployment, ctx.succeeding(zoid -> ctx.completeNow()));
        } else ctx.completeNow();
    }


    @ValueSource(strings = {
            "it-config-all-services.yml",
            "it-config-cache-only.yml",
            "it-config-no-book-service.yml",
            "it-config-no-media-service.yml",
            "it-config-no-cache-with-remote.yml",
            "it-config-no-service.yml"
    })
    @ParameterizedTest
    @DisplayName("it should serve requests")
    void itShouldServeRequests(String config, Vertx vertx, VertxTestContext ctx) {
        Checkpoint cp = ctx.checkpoint(5);
        startApplication(config, vertx, ctx, id -> {
            deployment = id;

            HttpClient client = getClient(vertx);
            client.getNow("/search?term=Rammstein", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));

            client.getNow("/search?term=Rammstein&maxResultsPerSource=20&includeMeta=true&prettyPrint=true", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));

            client.getNow("/search?term=Rammstein&source=media", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));

            client.getNow("/search?term=Rammstein&source=books", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));

            client.getNow("/search?term=Rammstein&country=US&sortOrder=desc&sortField=type", response -> ctx.verify(() -> {
                assertThat(response.statusCode()).isEqualTo(200);
                cp.flag();
            }));
        });
    }

    private HttpClient getClient(Vertx vertx) {
        return vertx.createHttpClient(new HttpClientOptions().setDefaultPort(PORT));
    }


}


