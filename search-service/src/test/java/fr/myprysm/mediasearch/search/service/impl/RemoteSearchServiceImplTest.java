package fr.myprysm.mediasearch.search.service.impl;

import fr.myprysm.mediasearch.book.service.BookSearchService;
import fr.myprysm.mediasearch.media.service.MediaSearchService;
import fr.myprysm.mediasearch.search.dto.SearchCriteria;
import fr.myprysm.mediasearch.search.dto.SearchResult;
import fr.myprysm.mediasearch.search.service.SampleSearchService;
import fr.myprysm.mediasearch.search.service.SearchService;
import fr.myprysm.vertx.core.test.ConfigTestHelper;
import fr.myprysm.vertx.service.HealthCheck;
import fr.myprysm.vertx.test.VertxTest;
import io.reactivex.Single;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.Status;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxTestContext;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.EventBusService;
import io.vertx.serviceproxy.ServiceBinder;
import org.junit.jupiter.api.*;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("Duplicates")
@DisplayName("Remote Search Service tests")
class RemoteSearchServiceImplTest implements VertxTest {

    private static MediaSearchService mediaService;
    private static BookSearchService bookService;

    @BeforeAll
    static void startSampleServices(Vertx vertx, VertxTestContext ctx) {
        Future<BookSearchService> bookServiceFuture = Future.future();
        Future<MediaSearchService> mediaServiceFuture = Future.future();
        bookService = BookSearchService.createSample(vertx, bookServiceFuture);
        mediaService = MediaSearchService.createSample(vertx, mediaServiceFuture);

        CompositeFuture.all(bookServiceFuture, mediaServiceFuture).setHandler(ctx.succeeding(r -> ctx.completeNow()));
    }

    private RemoteSearchServiceImpl getService(Vertx vertx) {
        RemoteSearchServiceImpl searchService = new RemoteSearchServiceImpl();
        searchService.init(new io.vertx.reactivex.core.Vertx(vertx));
        return searchService;
    }

    private Single<SearchService> getServiceInitialized(Vertx vertx) {
        RemoteSearchServiceImpl service = getService(vertx);
        return service.configure().andThen(Single.just(service));
    }

    @Nested
    @DisplayName("It should serve with remote services")
    class RemoteSearchWithRemoteTest {

        ServiceDiscovery discovery;
        private MessageConsumer<JsonObject> consumerMedia;
        private Record mediaRecord;

        private MessageConsumer<JsonObject> consumerBook;
        private Record bookRecord;

        @Test
        @DisplayName("It should serve results with remote services")
        void itShouldServeResults(Vertx vertx, VertxTestContext ctx) {
            Checkpoint cp = ctx.checkpoint(6);
            ConfigTestHelper.load(vertx, ctx, "services/search/config.yml", (_service, _consumer) -> {
                getServiceInitialized(vertx)
                        .subscribe(searchService -> {
                            searchService.search(new SearchCriteria().setTerm("test"), ctx.succeeding(results -> ctx.verify(() -> {
                                assertThat(results).hasSize(10);

                                for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                                cp.flag();
                            })));

                            // 10 per source
                            searchService.search(new SearchCriteria().setTerm("other term").setMaxResultsPerSource(10), ctx.succeeding(results -> ctx.verify(() -> {
                                assertThat(results).hasSize(20);
                                cp.flag();
                            })));

                            // Include meta
                            searchService.search(new SearchCriteria().setTerm("other term").setIncludeMeta(true), ctx.succeeding(results -> ctx.verify(() -> {
                                assertThat(results).hasSize(10);

                                for (SearchResult result : results) assertThat(result.getMeta()).isNotNull();

                                cp.flag();
                            })));

                            // Media only
                            searchService.search(new SearchCriteria().setTerm("other term").setSource(SearchCriteria.Source.media), ctx.succeeding(results -> ctx.verify(() -> {
                                assertThat(results).hasSize(5);

                                for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                                cp.flag();
                            })));

                            // Books only
                            searchService.search(new SearchCriteria().setTerm("other term").setSource(SearchCriteria.Source.books), ctx.succeeding(results -> ctx.verify(() -> {
                                assertThat(results).hasSize(5);

                                for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                                cp.flag();
                            })));

                            Future<Status> future = Future.future();
                            ((HealthCheck) searchService).beat().handle(io.vertx.reactivex.core.Future.newInstance(future));
                            future.setHandler(ctx.succeeding(status -> ctx.verify(() -> {
                                assertThat(status.isOk()).isTrue();
                                cp.flag();
                            })));

                        }, ctx::failNow);
            });
        }


        @BeforeEach
        void bindService(Vertx vertx, VertxTestContext ctx) {
            Checkpoint cp = ctx.checkpoint(2);
            ServiceDiscovery.create(vertx, discovery -> {
                this.discovery = discovery;

                consumerMedia = new ServiceBinder(vertx).setAddress("media-service").register(MediaSearchService.class, mediaService);
                Record record = EventBusService.createRecord("media-service", "media-service", MediaSearchService.class);
                discovery.publish(record, ctx.succeeding(r -> {
                    mediaRecord = r;
                    cp.flag();
                }));

                consumerBook = new ServiceBinder(vertx).setAddress("book-service").register(BookSearchService.class, bookService);
                record = EventBusService.createRecord("book-service", "book-service", BookSearchService.class);
                discovery.publish(record, ctx.succeeding(r -> {
                    bookRecord = r;
                    cp.flag();
                }));
            });

        }

        @AfterEach
        void unbindService(Vertx vertx, VertxTestContext ctx) {
            Checkpoint cp = ctx.checkpoint(2);
            discovery.unpublish(mediaRecord.getRegistration(), ctx.succeeding(zoid -> {
                consumerMedia.unregister(ctx.succeeding(zoid2 -> cp.flag()));
            }));

            discovery.unpublish(bookRecord.getRegistration(), ctx.succeeding(zoid -> {
                consumerBook.unregister(ctx.succeeding(zoid2 -> cp.flag()));
            }));

            vertx.setTimer(100, timer -> discovery.close());
        }


    }


    @Nested
    @DisplayName("It should serve with only one remote service")
    class RemoteSearchWithOneLegTest {

        ServiceDiscovery discovery;
        private MessageConsumer<JsonObject> consumerMedia;
        private Record mediaRecord;

        private MessageConsumer<JsonObject> consumerBook;
        private Record bookRecord;

        private MessageConsumer<JsonObject> consumerSearch;
        private Record searchRecord;
        private SampleSearchService sampleSearchService;

        @Test
        @DisplayName("It should serve results with book service only")
        void itShouldServeResultsWithBookServiceOnly(Vertx vertx, VertxTestContext ctx) {
            Checkpoint cp = ctx.checkpoint(6);
            consumerBook = new ServiceBinder(vertx).setAddress("book-service").register(BookSearchService.class, bookService);
            bookRecord = EventBusService.createRecord("book-service", "book-service", BookSearchService.class);

            discovery.publish(bookRecord, ctx.succeeding(r -> {


                ConfigTestHelper.load(vertx, ctx, "services/search/config.yml", (_service, _consumer) -> {
                    getServiceInitialized(vertx)
                            .subscribe(searchService -> {
                                searchService.search(new SearchCriteria().setTerm("test"), ctx.succeeding(results -> ctx.verify(() -> {
                                    assertThat(results).hasSize(10);

                                    for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                                    cp.flag();
                                })));

                                // 10 per source
                                searchService.search(new SearchCriteria().setTerm("other term").setMaxResultsPerSource(10), ctx.succeeding(results -> ctx.verify(() -> {
                                    assertThat(results).hasSize(20);
                                    cp.flag();
                                })));

                                // Include meta
                                searchService.search(new SearchCriteria().setTerm("other term").setIncludeMeta(true), ctx.succeeding(results -> ctx.verify(() -> {
                                    assertThat(results).hasSize(10);

                                    for (SearchResult result : results) assertThat(result.getMeta()).isNotNull();

                                    cp.flag();
                                })));

                                // Media only
                                searchService.search(new SearchCriteria().setTerm("other term").setSource(SearchCriteria.Source.media), ctx.succeeding(results -> ctx.verify(() -> {
                                    assertThat(results).hasSize(5);

                                    for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                                    cp.flag();
                                })));

                                // Books only
                                searchService.search(new SearchCriteria().setTerm("other term").setSource(SearchCriteria.Source.books), ctx.succeeding(results -> ctx.verify(() -> {
                                    assertThat(results).hasSize(5);

                                    for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                                    cp.flag();
                                })));

                                Future<Status> future = Future.future();
                                ((HealthCheck) searchService).beat().handle(io.vertx.reactivex.core.Future.newInstance(future));
                                future.setHandler(ctx.succeeding(status -> ctx.verify(() -> {
                                    assertThat(status.isOk()).isTrue();
                                    cp.flag();
                                })));

                            }, ctx::failNow);
                });

            }));


        }

        @Test
        @DisplayName("It should serve results with media service only")
        void itShouldServeResultsWithMediaServiceOnly(Vertx vertx, VertxTestContext ctx) {
            Checkpoint cp = ctx.checkpoint(6);
            consumerMedia = new ServiceBinder(vertx).setAddress("media-service").register(MediaSearchService.class, mediaService);
            mediaRecord = EventBusService.createRecord("media-service", "media-service", MediaSearchService.class);

            discovery.publish(mediaRecord, ctx.succeeding(r -> {


                ConfigTestHelper.load(vertx, ctx, "services/search/config.yml", (_service, _consumer) -> {
                    getServiceInitialized(vertx)
                            .subscribe(searchService -> {
                                searchService.search(new SearchCriteria().setTerm("test"), ctx.succeeding(results -> ctx.verify(() -> {
                                    assertThat(results).hasSize(10);

                                    for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                                    cp.flag();
                                })));

                                // 10 per source
                                searchService.search(new SearchCriteria().setTerm("other term").setMaxResultsPerSource(10), ctx.succeeding(results -> ctx.verify(() -> {
                                    assertThat(results).hasSize(20);
                                    cp.flag();
                                })));

                                // Include meta
                                searchService.search(new SearchCriteria().setTerm("other term").setIncludeMeta(true), ctx.succeeding(results -> ctx.verify(() -> {
                                    assertThat(results).hasSize(10);

                                    for (SearchResult result : results) assertThat(result.getMeta()).isNotNull();

                                    cp.flag();
                                })));

                                // Media only
                                searchService.search(new SearchCriteria().setTerm("other term").setSource(SearchCriteria.Source.media), ctx.succeeding(results -> ctx.verify(() -> {
                                    assertThat(results).hasSize(5);

                                    for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                                    cp.flag();
                                })));

                                // Books only
                                searchService.search(new SearchCriteria().setTerm("other term").setSource(SearchCriteria.Source.books), ctx.succeeding(results -> ctx.verify(() -> {
                                    assertThat(results).hasSize(5);

                                    for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                                    cp.flag();
                                })));

                                Future<Status> future = Future.future();
                                ((HealthCheck) searchService).beat().handle(io.vertx.reactivex.core.Future.newInstance(future));
                                future.setHandler(ctx.succeeding(status -> ctx.verify(() -> {
                                    assertThat(status.isOk()).isTrue();
                                    cp.flag();
                                })));

                            }, ctx::failNow);
                });

            }));


        }


        @BeforeEach
        void bindService(Vertx vertx, VertxTestContext ctx) {
            ServiceDiscovery.create(vertx, discovery -> {
                this.discovery = discovery;
                sampleSearchService = new SampleSearchService(vertx, ctx.succeeding(service -> {
                    consumerSearch = new ServiceBinder(vertx).setAddress("search-cache-service").register(SearchService.class, service);
                    searchRecord = EventBusService.createRecord("search-cache-service", "search-cache-service", SearchService.class);

                    discovery.publish(searchRecord, r -> ctx.completeNow());
                }));
            });

        }

        @AfterEach
        void unbindService(Vertx vertx, VertxTestContext ctx) {
            int services = 1;
            if (mediaRecord != null) {
                services++;
            }

            if (bookRecord != null) {
                services++;
            }

            Checkpoint cp = ctx.checkpoint(services);
            if (mediaRecord != null) {
                discovery.unpublish(mediaRecord.getRegistration(), ctx.succeeding(zoid -> {
                    consumerMedia.unregister(ctx.succeeding(zoid2 -> cp.flag()));
                }));
            }

            if (bookRecord != null) {
                discovery.unpublish(bookRecord.getRegistration(), ctx.succeeding(zoid -> {
                    consumerBook.unregister(ctx.succeeding(zoid2 -> cp.flag()));
                }));
            }

            discovery.unpublish(searchRecord.getRegistration(), ctx.succeeding(zoid -> {
                consumerSearch.unregister(ctx.succeeding(zoid2 -> cp.flag()));
            }));

            vertx.setTimer(100, timer -> discovery.close());
        }


    }


}