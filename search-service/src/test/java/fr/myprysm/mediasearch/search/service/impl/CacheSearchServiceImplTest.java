package fr.myprysm.mediasearch.search.service.impl;

import com.github.tomakehurst.wiremock.WireMockServer;
import fr.myprysm.mediasearch.search.dto.SearchCriteria;
import fr.myprysm.mediasearch.search.dto.SearchResult;
import fr.myprysm.mediasearch.search.service.SearchService;
import fr.myprysm.vertx.core.test.ConfigTestHelper;
import fr.myprysm.vertx.service.HealthCheck;
import fr.myprysm.vertx.test.VertxTest;
import io.reactivex.Single;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.ext.healthchecks.Status;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Cache Search Service tests")
class CacheSearchServiceImplTest implements VertxTest {
    // See resources/services/search/valid/config.yml
    private static final int PORT = 8765;

    private static WireMockServer wiremockServer;

    @BeforeAll
    static void startWireMock() {
        wiremockServer = new WireMockServer(options()
                .port(PORT)
                .usingFilesUnderClasspath("services/search/wiremock"));
        wiremockServer.start();
    }

    @AfterAll
    static void stopWireMock() {
        wiremockServer.stop();
    }

    private CacheSearchServiceImpl getService(Vertx vertx) {
        CacheSearchServiceImpl searchService = new CacheSearchServiceImpl();
        searchService.init(new io.vertx.reactivex.core.Vertx(vertx));
        return searchService;
    }

    private Single<SearchService> getServiceInitialized(Vertx vertx) {
        CacheSearchServiceImpl service = getService(vertx);
        return service.configure().andThen(Single.just(service));
    }

    @Test
    @DisplayName("It should serve results from cache")
    void itShouldServeResults(Vertx vertx, VertxTestContext ctx) {
        Checkpoint cp = ctx.checkpoint(6);
        ConfigTestHelper.load(vertx, ctx, "services/search/config.yml", (_service, _consumer) -> {
            getServiceInitialized(vertx)
                    .subscribe(searchService -> {

                        // 7 results only, cache not fulfilled yet
                        searchService.search(new SearchCriteria().setTerm("Philip K Dick"), ctx.succeeding(results -> ctx.verify(() -> {
                            assertThat(results).hasSize(7);

                            for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                            cp.flag();
                        })));

                        // 10 per source, not enough results == 12
                        searchService.search(new SearchCriteria().setTerm("Bernard Werber").setMaxResultsPerSource(10), ctx.succeeding(results -> ctx.verify(() -> {
                            assertThat(results).hasSize(12);
                            cp.flag();
                        })));

                        // Include meta
                        searchService.search(new SearchCriteria().setTerm("Philip K Dick").setIncludeMeta(true), ctx.succeeding(results -> ctx.verify(() -> {
                            assertThat(results).hasSize(7);

                            for (SearchResult result : results) assertThat(result.getMeta()).isNotNull();

                            cp.flag();
                        })));

                        // Media only, 2 results
                        searchService.search(new SearchCriteria().setTerm("Philip K Dick").setSource(SearchCriteria.Source.media), ctx.succeeding(results -> ctx.verify(() -> {
                            assertThat(results).hasSize(2);

                            for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                            cp.flag();
                        })));

                        // Books only
                        searchService.search(new SearchCriteria().setTerm("Philip K Dick").setSource(SearchCriteria.Source.books), ctx.succeeding(results -> ctx.verify(() -> {
                            assertThat(results).hasSize(5);

                            for (SearchResult result : results) assertThat(result.getMeta()).isNull();
                            cp.flag();
                        })));

                        Future<Status> future = Future.future();
                        ((HealthCheck) searchService).beat().handle(io.vertx.reactivex.core.Future.newInstance(future));
                        future.setHandler(ctx.succeeding(status -> ctx.verify(() -> {
                            assertThat(status.isOk()).isTrue();
                            cp.flag();
                        })));

                    }, ctx::failNow);
        });
    }


}