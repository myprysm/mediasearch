package fr.myprysm.mediasearch.search.service.impl;

import fr.myprysm.mediasearch.search.dto.SearchCriteria;
import fr.myprysm.mediasearch.search.dto.SearchResult;
import fr.myprysm.mediasearch.search.service.SearchCriteriaParams;
import fr.myprysm.mediasearch.search.service.SearchService;
import fr.myprysm.vertx.core.reactivex.config.ConfigService;
import fr.myprysm.vertx.core.utils.Utils;
import fr.myprysm.vertx.service.AbstractService;
import fr.myprysm.vertx.service.HealthCheck;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.BiFunction;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.SingleHelper;
import io.vertx.reactivex.core.Future;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static fr.myprysm.vertx.json.JsonHelpers.obj;

/**
 * Base Search service that implements the search logic across the services.
 * Plain old declarative at the moment
 * <p>
 * Expects to work with a chain in future releases.
 */
@Slf4j
public abstract class BaseSearchService extends AbstractService implements SearchService, HealthCheck {

    public static final String DEFAULT_CRITERIA_CONFIG = "search.service.default";

    private SearchCriteria defaults;
    private ConfigService configProxy;

    @Override
    public Completable configure() {
        configProxy = Utils.configServiceProxy(vertx());
        return configureDefaultCriteria();
    }

    @Override
    public SearchService search(SearchCriteria criteria, Handler<AsyncResult<List<SearchResult>>> handler) {
        SearchCriteria.Source source = criteria.getSource();
        SearchCriteriaParams params = new SearchCriteriaParams(criteria);

        Future<List<JsonObject>> books = Future.succeededFuture(new ArrayList<>());
        Future<List<JsonObject>> media = Future.succeededFuture(new ArrayList<>());

        if (source == SearchCriteria.Source.all || source == SearchCriteria.Source.books) {
            log.debug("including books");
            books = findBooks(params);
        }

        if (source == SearchCriteria.Source.all || source == SearchCriteria.Source.media) {
            log.debug("including media");
            media = findMedia(params);
        }

        Single.zip(books.rxSetHandler(), media.rxSetHandler(), this.mergeResults(criteria))
                .subscribe(SingleHelper.toObserver(handler));


        return this;
    }


    /**
     * Returns a handler that will zip both results and sort them according to the criteria
     *
     * @param criteria the criteria
     * @return search results
     */
    private BiFunction<List<JsonObject>, List<JsonObject>, List<SearchResult>> mergeResults(SearchCriteria criteria) {
        return (books, media) -> {
            books.addAll(media);

            return books.stream()
                    .sorted(getComparator(criteria.getSortField(), criteria.getSortOrder()))
                    .map(SearchResult::new)
                    .map(result -> {
                        if (criteria.isIncludeMeta()) {
                            result.setTerm(criteria.getTerm());
                        } else {
                            result.setMeta(null);
                        }

                        return result;
                    })
                    .collect(Collectors.toList());
        };
    }

    /**
     * Finds the books according to the provided criteria
     *
     * @param criteria the criteria
     * @return a future containing the books
     */
    protected abstract Future<List<JsonObject>> findBooks(SearchCriteriaParams criteria);

    /**
     * Finds the media according to the provide criteria
     *
     * @param criteria the criteria
     * @return a future containing the criteria
     */
    protected abstract Future<List<JsonObject>> findMedia(SearchCriteriaParams criteria);

    SearchCriteria defaults() {
        return defaults;
    }

    ConfigService configProxy() {
        return configProxy;
    }

    private Completable configureDefaultCriteria() {
        return configProxy
                .rxGetObject(DEFAULT_CRITERIA_CONFIG)
                .onErrorResumeNext(Single.just(obj()))
                .map(SearchCriteria::new)
                .doOnSuccess(this::setDefaults)
                .toCompletable();
    }

    private void setDefaults(SearchCriteria criteria) {
        defaults = criteria;
    }


}
