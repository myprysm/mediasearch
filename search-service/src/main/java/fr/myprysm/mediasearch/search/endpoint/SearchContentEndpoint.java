package fr.myprysm.mediasearch.search.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.myprysm.mediasearch.search.dto.SearchCriteria;
import fr.myprysm.mediasearch.search.dto.SearchResult;
import fr.myprysm.mediasearch.search.service.SampleSearchService;
import fr.myprysm.mediasearch.search.service.SearchService;
import fr.myprysm.mediasearch.search.service.SearchServiceException;
import fr.myprysm.vertx.core.reactivex.config.ConfigService;
import fr.myprysm.vertx.core.utils.Utils;
import fr.myprysm.vertx.json.Json;
import fr.myprysm.vertx.web.AbstractOpenAPIEndpoint;
import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.api.RequestParameter;
import io.vertx.ext.web.api.RequestParameters;
import io.vertx.reactivex.circuitbreaker.CircuitBreaker;
import io.vertx.reactivex.core.Future;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.servicediscovery.ServiceDiscovery;
import io.vertx.reactivex.servicediscovery.types.EventBusService;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static fr.myprysm.vertx.json.JsonHelpers.obj;

@Slf4j
public class SearchContentEndpoint extends AbstractOpenAPIEndpoint {
    private static final JsonObject SERVICE_REMOTE_FILTER = new JsonObject()
            .put("name", "search-remote-service")
            .put("service.interface", SearchService.class.getName());

    private static final JsonObject SERVICE_CACHE_FILTER = new JsonObject()
            .put("name", "search-cache-service")
            .put("service.interface", SearchService.class.getName());

    private static final String SERVICE_DISCOVERY_NAME = "search-endpoint:" + Utils.instanceId;

    private static final String CIRCUIT_BREAKER_CONFIG = "search.endpoint.circuitBreaker";
    private static final String BREAKER_GET_REMOTE_SERVICE = "search-service-endpoint:get-remote-service:" + Utils.instanceId;
    private static final String BREAKER_GET_CACHE_SERVICE = "search-service-endpoint:get-cache-service:" + Utils.instanceId;
    private static final String BREAKER_HANDLE_SEARCH = "search-service-endpoint:handle-search:" + Utils.instanceId;


    private static final String DEFAULT_CRITERIA_CONFIG = "search.service.default";

    private ServiceDiscovery discovery;
    private CircuitBreaker breakerHandleSearch;
    private CircuitBreaker breakerGetRemoteService;
    private CircuitBreaker breakerGetCacheService;

    private ConfigService configProxy;
    private SearchCriteria defaults;
    private SearchService fallbackService;


    @Override
    public String operationId() {
        return "searchContent";
    }


    @Override
    public void handle(RoutingContext event) {
        RequestParameters params = event.get("parsedParameters");
        setContentJson(event);
        SearchCriteria criteria = handleParams(params);
        breakerGetRemoteService
                .rxExecuteCommand(this::getRemoteService)
                .onErrorResumeNext(Single.defer(this::getCacheService))
                .flatMap(this.search(criteria))
                .onErrorResumeNext(this.fallback(criteria))
                .subscribe(this.writeResponse(event, criteria), err -> event.response().end("[]"));
    }

    private Single<List<SearchResult>> fallback(SearchCriteria criteria) {
        return Single.create(emitter -> fallbackService.search(criteria, ar -> {
            log.info("Hit fallback");
            if (ar.succeeded()) {
                emitter.onSuccess(ar.result());
            } else {
                emitter.onError(ar.cause());
            }
        }));
    }

    /**
     * Returns a cache service from discovery when available
     * otherwise fallbacks to the {@link SampleSearchService}
     *
     * @return a single containing the search service
     */
    private Single<SearchService> getCacheService() {
        return breakerGetCacheService.rxExecuteCommandWithFallback(cacheService -> {
            EventBusService.getServiceProxyWithJsonFilter(discovery, SERVICE_CACHE_FILTER, SearchService.class, ar -> {
                if (ar.succeeded()) {
                    cacheService.complete(ar.result());
                } else {
                    cacheService.fail(ar.cause());
                }
            });
        }, err -> fallbackService);
    }

    /**
     * Handle the incoming request parameters and transform them into a {@link SearchCriteria}
     *
     * @param params the params
     * @return the criteria
     */
    private SearchCriteria handleParams(RequestParameters params) {
        SearchCriteria criteria = new SearchCriteria(defaults);
        criteria.setTerm(params.queryParameter("term").getString());

        if (params.queryParameter("maxResultsPerSource") != null) {
            criteria.setMaxResultsPerSource(params.queryParameter("maxResultsPerSource").getInteger());
        }

        if (params.queryParameter("source") != null) {
            criteria.setSource(fromEnum(params.queryParameter("source"), SearchCriteria.Source.class));
        }

        if (params.queryParameter("sortOrder") != null) {
            criteria.setSortOrder(fromEnum(params.queryParameter("sortOrder"), SearchCriteria.SortOrder.class));
        }

        if (params.queryParameter("sortField") != null) {
            criteria.setSortField(fromEnum(params.queryParameter("sortField"), SearchCriteria.SortField.class));
        }

        if (params.queryParameter("includeMeta") != null) {
            criteria.setIncludeMeta(params.queryParameter("includeMeta").getBoolean());
        }

        if (params.queryParameter("prettyPrint") != null) {
            criteria.setPrettyPrint(params.queryParameter("prettyPrint").getBoolean());
        }

        return criteria;
    }

    /**
     * Get an enum value from a parameter
     *
     * @param param     the request parameter
     * @param enumClass the enum class
     * @param <T>       the enum
     * @return the value
     */
    private <T extends Enum<T>> T fromEnum(RequestParameter param, Class<T> enumClass) {
        return Enum.valueOf(enumClass, param.getString());
    }

    /**
     * Get a handler that executes the search with the provided service through the circuit breaker.
     * <p>
     * It releases the service from the discovery once finished
     *
     * @param criteria the search criteria
     * @return the handler
     */
    @SuppressWarnings("Duplicates")
    private Function<SearchService, Single<List<SearchResult>>> search(SearchCriteria criteria) {
        return service -> breakerHandleSearch.rxExecuteCommand(search -> {
            //noinspection unchecked
            service.search(criteria, ar -> {
                if (ar.succeeded()) {
                    search.complete(ar.result());
                } else {
                    log.error("Unable to get search results", ar.cause());
                    searchCache(criteria, search);
                }
                ServiceDiscovery.releaseServiceObject(discovery, service);
            });
        });
    }

    /**
     * Search from cache with the provide criteria.
     * <p>
     * Fulfills the provided future
     *
     * @param criteria the criteria
     * @param search   the future
     */
    private void searchCache(SearchCriteria criteria, Future<List<SearchResult>> search) {
        EventBusService.getServiceProxyWithJsonFilter(discovery, SERVICE_CACHE_FILTER, SearchService.class, getCacheService -> {
            if (getCacheService.succeeded()) {
                //noinspection unchecked
                log.info("Falling back on cache");
                getCacheService.result().search(criteria, search.getDelegate());
            } else {
                search.fail(getCacheService.cause());
            }
        });
    }

    /**
     * Get the Search service plugged to remotes and fulfills the provided future
     *
     * @param getService the future that accepts the remote service
     */
    private void getRemoteService(Future<SearchService> getService) {
        EventBusService.getServiceProxyWithJsonFilter(discovery, SERVICE_REMOTE_FILTER, SearchService.class, ar -> {
            if (ar.succeeded()) {
                SearchService service = ar.result();
                getService.complete(service);
            } else {
                log.error("Unable to discover service [search-remote-service]", ar.cause());
                if (ar.cause() instanceof SearchServiceException && ((SearchServiceException) ar.cause()).getDebugInfo() != null) {
                    log.error("Debug info: ", ((SearchServiceException) ar.cause()).getDebugInfo());
                }
                getService.fail(ar.cause());
            }
        });
    }

    /**
     * returns a consumer that writes the results as json in the response according to the requested format
     *
     * @param event    the request
     * @param criteria the criteria
     * @return the
     */
    private Consumer<List<SearchResult>> writeResponse(RoutingContext event, SearchCriteria criteria) {
        return results -> {
            ObjectMapper mapper = criteria.isPrettyPrint() ? Json.prettyMapper : Json.mapper;
            event.response().end(mapper.writeValueAsString(results));
        };
    }

    //region Configuration
    @Override
    public Completable configure() {
        configProxy = Utils.configServiceProxy(vertx());
        return Completable.concatArray(
                Completable.create(this::prepareFallbackService),
                configureDiscovery(),
                configureCircuitBreaker(),
                configureDefaultCriteria()
        );
    }

    private Completable configureDefaultCriteria() {
        return configProxy
                .rxGetObject(DEFAULT_CRITERIA_CONFIG)
                .onErrorResumeNext(Single.just(obj()))
                .map(SearchCriteria::new)
                .doOnSuccess(this::setDefaults)
                .toCompletable();
    }

    private void setDefaults(SearchCriteria criteria) {
        defaults = criteria;
    }

    private Completable configureCircuitBreaker() {
        return configProxy
                .rxGetObject(CIRCUIT_BREAKER_CONFIG)
                .onErrorResumeNext(Single.just(obj()))
                .map(CircuitBreakerOptions::new)
                .doOnSuccess(this::prepareBreakers)
                .toCompletable();
    }

    private void prepareBreakers(CircuitBreakerOptions options) {
        breakerHandleSearch = CircuitBreaker.create(BREAKER_HANDLE_SEARCH, vertx(), options);
        breakerGetRemoteService = CircuitBreaker.create(BREAKER_GET_REMOTE_SERVICE, vertx(), options);
        breakerGetCacheService = CircuitBreaker.create(BREAKER_GET_CACHE_SERVICE, vertx(), options);
    }

    private void prepareFallbackService(CompletableEmitter emitter) {
        fallbackService = new SampleSearchService(vertx().getDelegate(), service -> emitter.onComplete());
    }

    private Completable configureDiscovery() {
        return Single.<ServiceDiscovery>create(emitter -> ServiceDiscovery.create(vertx(), new ServiceDiscoveryOptions().setName(SERVICE_DISCOVERY_NAME), emitter::onSuccess))
                .doOnSuccess(this::setDiscovery)
                .toCompletable();
    }

    private void setDiscovery(ServiceDiscovery discovery) {
        this.discovery = discovery;
    }

    //endregion
}
