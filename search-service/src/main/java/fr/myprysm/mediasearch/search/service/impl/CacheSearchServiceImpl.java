package fr.myprysm.mediasearch.search.service.impl;

import fr.myprysm.mediasearch.book.dto.Book;
import fr.myprysm.mediasearch.media.dto.Media;
import fr.myprysm.mediasearch.search.dto.SearchResult;
import fr.myprysm.mediasearch.search.service.SearchCriteriaParams;
import fr.myprysm.mediasearch.search.service.SearchResultWrapper;
import fr.myprysm.mediasearch.search.service.VertxActionListener;
import io.reactivex.Completable;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.Status;
import io.vertx.reactivex.core.Future;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static fr.myprysm.vertx.json.JsonHelpers.extractString;
import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Searches books and media from an elastisearch cache
 */
public class CacheSearchServiceImpl extends BaseSearchService {
    private static final String ES_CONFIG = "search.service.es";


    RestHighLevelClient es;
    private String bookIndex;
    private String mediaIndex;

    @Override
    public Completable configure() {
        return super.configure().andThen(configureElasticSearchClient());
    }

    @Override
    protected Future<List<JsonObject>> findBooks(SearchCriteriaParams criteria) {
        BoolQueryBuilder boolQuery = boolQuery().minimumShouldMatch(1);

        boolQuery.should(matchQuery("term", criteria.getTerm()).boost(1.4F))
                .should(matchQuery("title", criteria.getTerm()).boost(1.2F))
                .should(matchQuery("author", criteria.getTerm()).boost(1.2F))
                .should(matchQuery("publisher", criteria.getTerm()).boost(0.8F))
                .should(matchQuery("description", criteria.getTerm()))
                .should(matchQuery("category", criteria.getTerm()))
        ;

        if (criteria.getCountry() != null) {
            boolQuery.must(termQuery("language", criteria.getCountry()));
        }

        SearchSourceBuilder source = new SearchSourceBuilder()
                .size(criteria.getMaxResultsPerSource())
                .from(0)
                .query(boolQuery);

        SearchRequest request = new SearchRequest(bookIndex).source(source);

        Future<SearchResponse> searchResponseFuture = Future.future();
        es.searchAsync(request, new VertxActionListener<>(searchResponseFuture));

        return searchResponseFuture.compose(searchResponse -> Future.succeededFuture(Arrays.stream(searchResponse.getHits().getHits())
                .map(SearchHit::getSourceAsMap)
                .map(JsonObject::new)
                .map(Book::new)
                .map(SearchResultWrapper::new)
                .map(SearchResult::toJson)
                .collect(Collectors.toList())));
    }

    @Override
    protected Future<List<JsonObject>> findMedia(SearchCriteriaParams criteria) {
        BoolQueryBuilder boolQuery = boolQuery().minimumShouldMatch(1);

        boolQuery.should(matchQuery("term", criteria.getTerm()))
                .should(matchQuery("artistName", criteria.getTerm()))
                .should(matchQuery("collectionName", criteria.getTerm()))
                .should(matchQuery("trackName", criteria.getTerm()))
        ;

        if (criteria.getCountry() != null) {
            boolQuery.must(termQuery("language", criteria.getCountry()));
        }

        SearchSourceBuilder source = new SearchSourceBuilder()
                .size(criteria.getMaxResultsPerSource())
                .from(0)
                .query(boolQuery);

        SearchRequest request = new SearchRequest(mediaIndex).source(source);

        Future<SearchResponse> searchResponseFuture = Future.future();
        es.searchAsync(request, new VertxActionListener<>(searchResponseFuture));

        return searchResponseFuture.compose(searchResponse -> Future.succeededFuture(Arrays.stream(searchResponse.getHits().getHits())
                .map(SearchHit::getSourceAsMap)
                .map(JsonObject::new)
                .map(Media::new)
                .map(SearchResultWrapper::new)
                .map(SearchResult::toJson)
                .collect(Collectors.toList())));
    }

    private Completable configureElasticSearchClient() {
        return configProxy()
                .rxGetObject(ES_CONFIG)
                .doOnSuccess(this::extractIndexes)
                .map(this::mapHost)
                .doOnSuccess(this::initClient)
                .toCompletable();
    }

    private void extractIndexes(JsonObject config) {
        bookIndex = extractString(config, "books.index").orElse("book");
        mediaIndex = extractString(config, "media.index").orElse("media");
    }

    private void initClient(HttpHost httpHost) {
        es = new RestHighLevelClient(RestClient.builder(httpHost));
    }


    @SuppressWarnings("Duplicates")
    private HttpHost mapHost(JsonObject host) {
        HttpHost httpHost;
        if (host.getValue("port") instanceof Number) {
            if (host.getValue("protocol") instanceof String) {
                httpHost = new HttpHost(host.getString("hostname"), host.getInteger("port"), host.getString("protocol"));
            } else {
                httpHost = new HttpHost(host.getString("hostname"), host.getInteger("port"));
            }
        } else {
            httpHost = HttpHost.create(host.getString("hostname"));
        }

        return httpHost;
    }

    @Override
    public String name() {
        return "search-cache-service";
    }

    @Override
    public Handler<Future<Status>> beat() {
        return status -> vertx().<Status>executeBlocking(future -> {
            try {
                future.complete(es.ping() ? Status.OK() : Status.KO());
            } catch (IOException e) {
                future.complete(Status.KO());
            }
        }, result -> {
            if (!status.isComplete()) {
                if (result.succeeded()) {
                    status.complete(result.result());
                } else {
                    status.complete(Status.KO());
                }
            }
        });
    }
}

