package fr.myprysm.mediasearch.search.service;

import fr.myprysm.mediasearch.book.dto.BookCriteria;
import fr.myprysm.mediasearch.media.dto.MediaCriteria;
import fr.myprysm.mediasearch.search.dto.SearchCriteria;

public class SearchCriteriaParams extends SearchCriteria {

    public SearchCriteriaParams(SearchCriteria other) {
        super(other);
    }

    public SearchCriteriaParams(SearchCriteriaParams other) {
        super(other);
    }

    public BookCriteria toBookCriteria() {
        BookCriteria criteria = new BookCriteria().setTerm(getTerm())
                .setPrettyPrint(isPrettyPrint())
                .setMedia(BookCriteria.PrintType.books)
                .setMaxResults(getMaxResultsPerSource())
                .setStartIndex(0);
        if (getCountry() != null) {
            criteria.setLangRestrict(getCountry());
        }

        return criteria;
    }

    public MediaCriteria toMediaCriteria() {
        MediaCriteria criteria = new MediaCriteria()
                .setMedia(MediaCriteria.Media.music)
                .setEntity(MediaCriteria.Entity.album)
                .setMaxResults(getMaxResultsPerSource())
                .setTerm(getTerm());

        if (getCountry() != null) {
            criteria.setCountry(getCountry());
        }

        return criteria;
    }
}
