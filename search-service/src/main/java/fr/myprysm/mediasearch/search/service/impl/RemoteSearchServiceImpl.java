package fr.myprysm.mediasearch.search.service.impl;

import com.google.common.collect.ImmutableList;
import fr.myprysm.mediasearch.book.service.BookSearchService;
import fr.myprysm.mediasearch.book.service.BookSearchServiceException;
import fr.myprysm.mediasearch.book.service.BookSearchServiceExceptionMessageCodec;
import fr.myprysm.mediasearch.media.service.MediaSearchService;
import fr.myprysm.mediasearch.media.service.MediaSearchServiceException;
import fr.myprysm.mediasearch.media.service.MediaSearchServiceExceptionMessageCodec;
import fr.myprysm.mediasearch.search.dto.SearchCriteria;
import fr.myprysm.mediasearch.search.dto.SearchResult;
import fr.myprysm.mediasearch.search.service.SearchCriteriaParams;
import fr.myprysm.mediasearch.search.service.SearchResultWrapper;
import fr.myprysm.mediasearch.search.service.SearchService;
import fr.myprysm.mediasearch.search.service.SearchServiceException;
import fr.myprysm.mediasearch.search.service.SearchServiceExceptionMessageCodec;
import fr.myprysm.vertx.core.utils.RoundRobin;
import fr.myprysm.vertx.core.utils.Utils;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.Status;
import io.vertx.reactivex.core.CompositeFuture;
import io.vertx.reactivex.core.Future;
import io.vertx.reactivex.servicediscovery.ServiceDiscovery;
import io.vertx.reactivex.servicediscovery.types.EventBusService;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import lombok.extern.slf4j.Slf4j;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Search books and media across book-service and media-service
 */
@Slf4j
public class RemoteSearchServiceImpl extends BaseSearchService {


    private static final JsonObject SERVICE_BOOKS_FILTER = new JsonObject().put("service.interface", BookSearchService.class.getName());
    private static final JsonObject SERVICE_MEDIA_FILTER = new JsonObject().put("service.interface", MediaSearchService.class.getName());
    private static final JsonObject SERVICE_CACHE_FILTER = new JsonObject()
            .put("name", "search-cache-service")
            .put("service.interface", SearchService.class.getName());
    private static final String SERVICE_DISCOVERY_NAME = "remote-search-service:" + Utils.instanceId;

    /**
     * Used for healtchecks to test remote services
     */
    private static final Iterator<String> beats = RoundRobin.of(ImmutableList.of(
            "Bernard Werber", "Philip K Dick", "Isaac Asimov", "Stephen King", "Jean de la Fontaine",
            "Red Hot Chili Peppers", "Rammstein", "Carpenter Brut", "God is an Astronaut", "Carbon Based Lifeforms"
    )).iterator();

    private ServiceDiscovery discovery;


    @Override
    protected Future<List<JsonObject>> findBooks(SearchCriteriaParams criteria) {
        Future<List<JsonObject>> future = Future.future();
        EventBusService.getServiceProxyWithJsonFilter(discovery, SERVICE_BOOKS_FILTER, BookSearchService.class, getBookService -> {
            if (getBookService.succeeded()) {
                getBookService.result().search(criteria.toBookCriteria(), arBooks -> {
                    if (arBooks.succeeded()) {
                        List<JsonObject> books = arBooks.result().stream()
                                .map(SearchResultWrapper::new)
                                .map(SearchResult::toJson)
                                .collect(Collectors.toList());
                        future.complete(books);
                    } else {
                        log.info("Error while gettings books, fallback on cache");
                        fromCache(criteria, SearchCriteria.Source.books, future);
                    }

                    ServiceDiscovery.releaseServiceObject(discovery, getBookService.result());
                });
            } else {
                fromCache(criteria, SearchCriteria.Source.books, future);
            }
        });
        return future;
    }

    @Override
    protected Future<List<JsonObject>> findMedia(SearchCriteriaParams criteria) {
        Future<List<JsonObject>> future = Future.future();
        EventBusService.getServiceProxyWithJsonFilter(discovery, SERVICE_MEDIA_FILTER, MediaSearchService.class, getMediaService -> {
            if (getMediaService.succeeded()) {
                getMediaService.result().search(criteria.toMediaCriteria(), arMedia -> {
                    if (arMedia.succeeded()) {
                        List<JsonObject> media = arMedia.result().stream()
                                .map(SearchResultWrapper::new)
                                .map(SearchResult::toJson)
                                .collect(Collectors.toList());
                        future.complete(media);
                    } else {
                        log.info("Error while gettings media, fallback on cache");
                        fromCache(criteria, SearchCriteria.Source.media, future);
                    }
                    ServiceDiscovery.releaseServiceObject(discovery, getMediaService.result());
                });
            } else {
                fromCache(criteria, SearchCriteria.Source.media, future);
            }
        });
        return future;
    }

    /**
     * Returns the search results from cache for the provided source and completes the future.
     *
     * @param criteria the current criteria
     * @param source   the source to search
     * @param future   the future to complete
     */
    private void fromCache(SearchCriteriaParams criteria, SearchCriteria.Source source, Future<List<JsonObject>> future) {
        EventBusService.getServiceProxyWithJsonFilter(discovery, SERVICE_CACHE_FILTER, SearchService.class, getCacheService -> {
            if (getCacheService.succeeded()) {
                getCacheService.result().search(new SearchCriteriaParams(criteria).setSource(source), ar -> {
                    if (ar.succeeded()) {
                        List<JsonObject> results = ar.result().stream()
                                .map(SearchResult::toJson)
                                .collect(Collectors.toList());
                        future.complete(results);
                    } else {
                        log.error("Unable to get from cache...", ar.cause());
                        future.fail(ar.cause());
                    }

                    ServiceDiscovery.releaseServiceObject(discovery, getCacheService.result());
                });
            } else {
                log.error("No cache service in the cluster...");
                future.fail(getCacheService.cause());
            }
        });
    }

    //region Configuration
    @Override
    public Completable configure() {
        tryRegisterCodec(MediaSearchServiceException.class, new MediaSearchServiceExceptionMessageCodec());
        tryRegisterCodec(BookSearchServiceException.class, new BookSearchServiceExceptionMessageCodec());
        tryRegisterCodec(SearchServiceException.class, new SearchServiceExceptionMessageCodec());
        return super.configure().andThen(configureDiscovery());
    }

    private Completable configureDiscovery() {
        return Single.<ServiceDiscovery>create(emitter -> ServiceDiscovery.create(vertx(), new ServiceDiscoveryOptions().setName(SERVICE_DISCOVERY_NAME), emitter::onSuccess))
                .doOnSuccess(this::setDiscovery)
                .toCompletable();
    }

    private void setDiscovery(ServiceDiscovery discovery) {
        this.discovery = discovery;
    }


    @Override
    public String name() {
        return "search-remote-service";
    }

    @Override
    public Handler<Future<Status>> beat() {
        return status -> {
            SearchCriteriaParams params = new SearchCriteriaParams(defaults());
            params.setTerm(beats.next());
            CompositeFuture.all(findBooks(params), findMedia(params))
                    .setHandler(ar -> {
                        if (!status.isComplete()) {
                            if (ar.succeeded()) {
                                status.complete(Status.OK());
                            } else {
                                status.complete(Status.KO());
                            }
                        }
                    });
        };
    }
    //endregion
}

