package fr.myprysm.mediasearch.search.service;

import io.vertx.core.Future;
import org.elasticsearch.action.ActionListener;

/**
 * Wraps an ElasticSearch {@link ActionListener} with a future that
 * completes when the listener is triggered.
 *
 * @param <T>
 */
public class VertxActionListener<T> implements ActionListener<T> {

    private final Future<T> future;

    /**
     * Builds an action listener with the provided future
     *
     * @param future the future
     */
    public VertxActionListener(Future<T> future) {
        this.future = future;
    }

    /**
     * Builds an action listener with the provided rx future
     *
     * @param future the rx future
     */
    @SuppressWarnings("unchecked")
    public VertxActionListener(io.vertx.reactivex.core.Future<T> future) {
        this(future.getDelegate());
    }

    @Override
    public void onResponse(T t) {
        future.complete(t);
    }

    @Override
    public void onFailure(Exception e) {
        future.fail(e);
    }
}
