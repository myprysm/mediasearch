package fr.myprysm.mediasearch.search.service;

import fr.myprysm.mediasearch.book.dto.Book;
import fr.myprysm.mediasearch.media.dto.Media;
import fr.myprysm.mediasearch.search.dto.SearchResult;

/**
 * Wraps the content as a search result for consistency
 */
public class SearchResultWrapper extends SearchResult {

    /**
     * Wraps a book as a search result
     *
     * @param book the book
     */
    public SearchResultWrapper(Book book) {
        setAuthor(book.getAuthor());
        setTitle(book.getTitle());
        setType("book");
        setMeta(book.toJson());
    }

    /**
     * Wraps a media as a search result
     *
     * @param media the media
     */
    public SearchResultWrapper(Media media) {
        setType(media.getKind());
        setAuthor(media.getArtistName());

        if (media.getCollectionName() != null) {
            setTitle(media.getCollectionName());
        } else if (media.getTrackName() != null) {
            setTitle(media.getTrackName());
        } else {
            setTitle(media.getArtistName());
        }

        setMeta(media.toJson());
    }
}
