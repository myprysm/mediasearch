<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>fr.myprysm.mediasearch</groupId>
    <artifactId>mediasearch</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>pom</packaging>

    <parent>
        <groupId>fr.myprysm.vertx</groupId>
        <artifactId>vertx-parent</artifactId>
        <version>0.1.1</version>
    </parent>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <myprysm-stack.version>0.1.3</myprysm-stack.version>
        <java.version>1.8</java.version>
        <vertx.version>3.5.3</vertx.version>
        <hazelcast-kubernetes.version>1.1.0</hazelcast-kubernetes.version>
        <pipeline.version>1.0.0-SNAPSHOT</pipeline.version>
        <jackson.version>2.9.5</jackson.version>
        <logback.version>1.2.3</logback.version>
        <guava.version>25.0-jre</guava.version>
        <lombok.version>1.16.20</lombok.version>
        <wiremock.version>2.18.0</wiremock.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.hazelcast</groupId>
            <artifactId>hazelcast-kubernetes</artifactId>
            <version>${hazelcast-kubernetes.version}</version>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-service-discovery-bridge-kubernetes</artifactId>
        </dependency>

        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-codegen</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>fr.myprysm.vertx</groupId>
            <artifactId>vertx-test</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-junit5</artifactId>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-engine</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-launcher</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>fr.myprysm</groupId>
                <artifactId>vertx-pipeline-core</artifactId>
                <version>${pipeline.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.myprysm</groupId>
                <artifactId>vertx-pipeline-metrics</artifactId>
                <version>${pipeline.version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>

            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>ch.qos.logback</groupId>
                <artifactId>logback-classic</artifactId>
                <version>${logback.version}</version>
            </dependency>

            <dependency>
                <groupId>fr.myprysm.vertx</groupId>
                <artifactId>vertx-dependencies</artifactId>
                <version>${myprysm-stack.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>io.vertx</groupId>
                <artifactId>vertx-dependencies</artifactId>
                <version>${vertx.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <modules>
        <module>book-contract</module>
        <module>book-service</module>

        <module>media-contract</module>
        <module>media-service</module>

        <module>search-contract</module>
        <module>search-service</module>

        <module>barrel</module>
    </modules>

    <build>
        <plugins>
            <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>docker-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>docker</id>
                        <phase>none</phase>
                    </execution>
                </executions>
            </plugin>
        </plugins>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${compiler-plugin.version}</version>
                    <configuration>
                        <source>${java.version}</source>
                        <target>${java.version}</target>
                        <useIncrementalCompilation>false</useIncrementalCompilation>

                        <!-- Codegen -->
                        <annotationProcessors>
                            <annotationProcessor>lombok.launch.AnnotationProcessorHider$AnnotationProcessor
                            </annotationProcessor>
                            <annotationProcessor>io.vertx.codegen.CodeGenProcessor</annotationProcessor>
                        </annotationProcessors>
                        <generatedSourcesDirectory>${project.basedir}/src/main/generated</generatedSourcesDirectory>
                        <compilerArgs>
                            <arg>-Acodegen.output=${project.basedir}/src/main</arg>
                        </compilerArgs>

                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-shade-plugin</artifactId>
                    <version>${shade-plugin.version}</version>
                    <executions>
                        <execution>
                            <phase>package</phase>
                            <goals>
                                <goal>shade</goal>
                            </goals>
                            <configuration>
                                <filters>
                                    <filter>
                                        <artifact>*:*</artifact>
                                        <excludes>
                                            <exclude>META-INF/*.SF</exclude>
                                            <exclude>META-INF/*.DSA</exclude>
                                            <exclude>META-INF/*.RSA</exclude>
                                        </excludes>
                                    </filter>
                                </filters>
                                <transformers>
                                    <transformer
                                            implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                        <manifestEntries>
                                            <Main-Class>fr.myprysm.vertx.core.Launcher</Main-Class>
                                            <Main-Verticle>fr.myprysm.vertx.core.StarterVerticle</Main-Verticle>
                                        </manifestEntries>
                                    </transformer>
                                    <transformer
                                            implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
                                        <resource>META-INF/services/io.vertx.core.spi.VerticleFactory</resource>
                                    </transformer>
                                </transformers>
                                <artifactSet>
                                </artifactSet>
                                <outputFile>${project.build.directory}/${project.artifactId}-fat.jar
                                </outputFile>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${jacoco-plugin.version}</version>
                    <configuration>
                        <excludes>
                            <exclude>**/*VertxEBProxy.*</exclude>
                            <exclude>**/*VertxProxyHandler.*</exclude>
                            <exclude>**/*Converter.*</exclude>
                        </excludes>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <version>${docker-plugin.version}</version>
                    <executions>
                        <execution>
                            <id>docker</id>
                            <phase>none</phase>
                            <goals>
                                <goal>build</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <!-- Configure the image name -->
                        <imageName>myprysm/${project.artifactId}:latest</imageName>
                        <baseImage>openjdk:8-jre-alpine</baseImage>
                        <env>
                            <JAR_NAME>${project.artifactId}-fat.jar</JAR_NAME>
                            <VERTICLE_HOME>/usr/verticles</VERTICLE_HOME>
                            <pipeline_es_host>elasticsearch</pipeline_es_host>
                            <pipeline_es_port>9200</pipeline_es_port>
                        </env>
                        <workdir>$VERTICLE_HOME</workdir>
                        <resources>
                            <resource>
                                <targetPath>/usr/verticles</targetPath>
                                <directory>${project.build.directory}</directory>
                                <includes>
                                    <include>${project.artifactId}-fat.jar</include>
                                </includes>
                            </resource>
                            <!-- don't forget to also add all the dependencies required by your application -->
                        </resources>
                        <entryPoint>["sh", "-c"]</entryPoint>
                        <!-- Allows to provide memory settings through env with docker/kubernetes -->
                        <cmd><![CDATA[
                        ["exec java -XX:+PrintFlagsFinal $JAVA_OPTIONS -Dvertx.metrics.options.enabled=true -Dvertx.metrics.options.jmxEnabled=true -jar $JAR_NAME -cluster -cp ."]
                        ]]></cmd>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

</project>