package fr.myprysm.mediasearch.book.service.impl;

import com.google.common.collect.ImmutableMap;
import fr.myprysm.mediasearch.book.dto.BookCriteria;
import fr.myprysm.vertx.test.VertxTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class BookCriteriaParamsTest implements VertxTest {


    @Test
    @DisplayName("it should transform a criteria to a map")
    void itShouldTransformCriteriaToMap() {
        BookCriteria criteria = new BookCriteria()
                .setLangRestrict("fr")
                .setMaxResults(10)
                .setMedia(BookCriteria.PrintType.magazines)
                .setPrettyPrint(true)
                .setOrderBy(BookCriteria.OrderBy.relevance)
                .setTerm("some term");
        BookCriteriaParams params = new BookCriteriaParams(criteria);


        Map<String, String> result = ImmutableMap.<String, String>builder()
                .put("langRestrict", "fr")
                .put("maxResults", "10")
                .put("startIndex", "0")
                .put("printType", "magazines")
                .put("prettyPrint", "true")
                .put("sortOrder", "relevance")
                .put("q", "some term")
                .build();

        assertThat(params.toMap()).isEqualTo(result);
    }

}