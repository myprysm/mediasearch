package fr.myprysm.mediasearch.book.service.impl;

import fr.myprysm.mediasearch.book.dto.BookCriteria;

import java.util.HashMap;
import java.util.Map;

public class BookCriteriaParams extends BookCriteria {

    public BookCriteriaParams(BookCriteria other) {
        super(other);
    }

    public String getTerm() {
        return super.getTerm().trim();
    }

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>(7);
        map.put("q", getTerm());
        map.put("maxResults", String.valueOf(getMaxResults()));
        map.put("startIndex", String.valueOf(getStartIndex()));
        map.put("prettyPrint", String.valueOf(getPrettyPrint()));

        if (getLangRestrict() != null) {
            map.put("langRestrict", getLangRestrict().trim());
        }

        if (getMedia() != null) {
            map.put("printType", getMedia().name());
        }

        if (getOrderBy() != null) {
            map.put("sortOrder", getOrderBy().name());
        }

        return map;
    }

}
