package fr.myprysm.mediasearch.book.service.impl;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.chainr.ChainrBuilder;
import com.google.common.collect.ImmutableList;
import fr.myprysm.mediasearch.book.dto.Book;
import fr.myprysm.mediasearch.book.dto.BookCriteria;
import fr.myprysm.mediasearch.book.service.BookSearchService;
import fr.myprysm.mediasearch.book.service.BookSearchServiceException;
import fr.myprysm.mediasearch.book.service.BookSearchServiceExceptionMessageCodec;
import fr.myprysm.mediasearch.book.service.retrofit.GoogleBooksSearchService;
import fr.myprysm.pipeline.util.RoundRobin;
import fr.myprysm.pipeline.validation.ValidationResult;
import fr.myprysm.vertx.core.reactivex.config.ConfigService;
import fr.myprysm.vertx.core.utils.Utils;
import fr.myprysm.vertx.json.Json;
import fr.myprysm.vertx.retrofit.ServiceFactory;
import fr.myprysm.vertx.service.AbstractService;
import fr.myprysm.vertx.service.HealthCheck;
import io.reactivex.Completable;
import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.Status;
import io.vertx.reactivex.SingleHelper;
import io.vertx.reactivex.core.Future;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static fr.myprysm.mediasearch.book.service.BookSearchServiceException.EMPTY_TERM;
import static fr.myprysm.mediasearch.book.service.BookSearchServiceException.REMOTE_ERROR;
import static fr.myprysm.pipeline.util.JsonHelpers.arr;
import static fr.myprysm.pipeline.validation.JsonValidation.*;
import static io.reactivex.Completable.fromAction;

@Slf4j
public class GoogleBookSearchServiceImpl extends AbstractService implements BookSearchService, HealthCheck {

    public static final String SERVICE_PARAMS = "search.book.service";
    private static final Iterator<String> beats = RoundRobin.of(ImmutableList.of(
            "Bernard Werber", "Philip K Dick", "Isaac Asimov", "Stephen King", "Jean de la Fontaine"
    )).iterator();
    private BookCriteria defaults;
    private Chainr chainr;
    private String cacheAddress;
    private GoogleBooksSearchService searchService;
    private boolean shouldTransform;

    @Override
    public Completable configure() {
        tryRegisterCodec(BookSearchServiceException.class, new BookSearchServiceExceptionMessageCodec());
        ConfigService configProxy = Utils.configServiceProxy(vertx());
        return configProxy
                .rxGetObject(SERVICE_PARAMS)
                .flatMapCompletable(this::configureInternal);
    }

    private ValidationResult validate(JsonObject config) {
        return matches("baseUrl",
                "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]/",
                "this is not a valid baseUrl (must end with a \"/\")"
        )
                .and(isString("apiKey"))
                .and(isNull("default").or(isObject("default")))
                .and(isString("cacheAddress"))
                .and(isNull("mapping").or(arrayOf("mapping", JsonObject.class)))
                .apply(config);
    }

    private Completable configureInternal(JsonObject config) {
        ValidationResult result = validate(config);
        if (!result.isValid()) {
            return Completable.error(result.toException());
        }

        cacheAddress = config.getString("cacheAddress");

        return Completable.concatArray(
                fromAction(() -> prepareClient(config)),
                fromAction(() -> prepareDefault(config.getJsonObject("default"))),
                fromAction(() -> prepareMapper(config.getJsonArray("mapping", arr())))
        );
    }

    /**
     * Configures JOLT chainr to convert a Google book object to a more reasonable object.
     *
     * @param mapping the list of JOLT transformations
     * @see com.bazaarvoice.jolt.Shiftr
     * @see Chainr
     */
    private void prepareMapper(JsonArray mapping) {
        if (mapping.size() > 0) {
            List<Map<String, Object>> rawSpecs = Json.convert(mapping);
            chainr = new ChainrBuilder(rawSpecs).build();
            shouldTransform = true;
        }
    }

    /**
     * Prepares the default criteria when using simple term search
     *
     * @param defaultParams the default params for search
     */
    private void prepareDefault(JsonObject defaultParams) {
        this.defaults = new BookCriteria(defaultParams);
    }

    /**
     * Parses the URL and prepare the default request that will be reused by the service.
     *
     * @param config the service configuration
     */
    private void prepareClient(JsonObject config) {
        ServiceFactory factory = ServiceFactory.forBase(config.getString("baseUrl"))
                .apiKey(config.getString("apiKey"))
                .build();

        searchService = factory.create(GoogleBooksSearchService.class);
    }

    @Override
    public BookSearchService findByTerm(String term, Handler<AsyncResult<List<Book>>> handler) {
        return search(new BookCriteria(defaults).setTerm(term), handler);
    }

    @Override
    public BookSearchService search(BookCriteria criteria, Handler<AsyncResult<List<Book>>> handler) {
        if (StringUtils.isNotBlank(criteria.getTerm())) {
            BookCriteriaParams params = new BookCriteriaParams(criteria);
            searchService.search(params.toMap())
                    .flatMap(this::handleStatus)
                    .map(this.handleResponse(params.getTerm()))
                    .subscribe(SingleHelper.toObserver(handler));
        } else {
            handler.handle(BookSearchServiceException.fail(EMPTY_TERM, "Term is empty"));
        }
        return this;
    }

    /**
     * Handler that rejects all responses but OK.
     * When OK, then forwards the JSON body
     *
     * @param response the response
     * @return the body if OK
     */
    @SuppressWarnings("ConstantConditions")
    private Single<JsonObject> handleStatus(Response<JsonObject> response) {
        if (response.code() != 200) {
            log.error("Error occured during request.");
            BookSearchServiceException error = new BookSearchServiceException(REMOTE_ERROR, "Error from remote");

            try {
                if (log.isDebugEnabled()) {
                    log.debug("Response [{}], Body: [{}]", response, response.errorBody().string());
                }
                error = new BookSearchServiceException(REMOTE_ERROR, "Error from remote", new JsonObject(response.errorBody().string()));
            } catch (IOException e) {
//
            }

            return Single.error(error);
        }

        return Single.just(response.body());
    }

    private Function<JsonObject, List<Book>> handleResponse(String term) {
        return body -> Observable.fromIterable(body.getJsonArray("items", arr()))
                .map(JsonObject.class::cast)
                .map(addTerm(term))
                .flatMapSingle(this::performTransform)
                .doOnEach(this::cache)
                .map(Book::new)
                .collectInto(new ArrayList<Book>(), ArrayList::add).blockingGet();
    }

    private Function<JsonObject, JsonObject> addTerm(String term) {
        return json -> json.put("term", term);
    }

    /**
     * Transform the Google book to a simpler version (see configuration.search.book.mapping)
     *
     * @param item the book
     * @return the asynchronously transformed item
     */
    @SuppressWarnings("unchecked")
    private Single<JsonObject> performTransform(JsonObject item) {
        return shouldTransform ?
                vertx().rxExecuteBlocking(handler -> handler.complete(new JsonObject((Map<String, Object>) chainr.transform(Json.convert(item)))), true) :
                Single.just(item);
    }

    /**
     * Emits an item to the cache pipeline
     *
     * @param notif the stream notification
     */
    private void cache(Notification<JsonObject> notif) {
        if (notif.isOnNext()) {
            vertx().eventBus().send(cacheAddress, notif.getValue());
        }
    }

    @Override
    public String name() {
        return "books-service";
    }

    @Override
    public Handler<Future<Status>> beat() {
        return status -> {
            BookCriteria criteria = new BookCriteria(defaults)
                    .setTerm(beats.next())
                    .setMaxResults(0);
            searchService.search(new BookCriteriaParams(criteria).toMap())
                    .subscribe((res, err) -> {
                        if (!status.isComplete()) {
                            if (res != null) {
                                status.complete(res.code() == 200 ? Status.OK() : Status.KO());
                            } else {
                                status.complete(Status.KO());
                            }
                        }
                    });
        };
    }
}
