package fr.myprysm.mediasearch.book.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.myprysm.mediasearch.book.dto.Book;
import fr.myprysm.mediasearch.book.dto.BookCriteria;
import fr.myprysm.mediasearch.book.dto.BookCriteria.OrderBy;
import fr.myprysm.mediasearch.book.dto.BookCriteria.PrintType;
import fr.myprysm.mediasearch.book.service.BookSearchService;
import fr.myprysm.mediasearch.book.service.BookSearchServiceException;
import fr.myprysm.mediasearch.book.service.SampleBookSearchService;
import fr.myprysm.vertx.core.reactivex.config.ConfigService;
import fr.myprysm.vertx.core.utils.Utils;
import fr.myprysm.vertx.web.AbstractOpenAPIEndpoint;
import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.api.RequestParameter;
import io.vertx.ext.web.api.RequestParameters;
import io.vertx.reactivex.circuitbreaker.CircuitBreaker;
import io.vertx.reactivex.core.Future;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.servicediscovery.ServiceDiscovery;
import io.vertx.reactivex.servicediscovery.types.EventBusService;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static fr.myprysm.pipeline.util.JsonHelpers.obj;

@Slf4j
public class SearchBooksEndpoint extends AbstractOpenAPIEndpoint {
    private static final JsonObject SERVICE_FILTER = new JsonObject().put("service.interface", BookSearchService.class.getName());
    private static final String CIRCUIT_BREAKER_CONFIG = "search.book.endpoint.circuitBreaker";
    private static final String DEFAULT_CRITERIA_CONFIG = "search.book.service.default";

    private static final String BREAKER_GET_SERVICE = "book-service-endpoint:get-service:" + Utils.instanceId;
    private static final String BREAKER_HANDLE_SEARCH = "book-service-endpoint:handle-search:" + Utils.instanceId;
    private static final String SERVICE_DISCOVERY_NAME = "search-book-endpoint:" + Utils.instanceId;

    private ServiceDiscovery discovery;
    private CircuitBreaker breakerHandleSearch;
    private CircuitBreaker breakerGetService;
    private ConfigService configProxy;
    private BookCriteria defaults;
    private BookSearchService fallbackService;

    @Override
    public String operationId() {
        return "searchBooks";
    }


    @Override
    public void handle(RoutingContext event) {
        RequestParameters params = event.get("parsedParameters");
        setContentJson(event);
        BookCriteria criteria = handleParams(params);
        breakerGetService
                .rxExecuteCommand(this::getService)
                .flatMap(this.search(criteria))
                .onErrorResumeNext(this.fallback(criteria))
                .map(this.processFields(params))
                .subscribe(this.writeResponse(event, criteria), err -> event.response().end("[]"));

    }

    private Single<List<Book>> fallback(BookCriteria criteria) {
        return Single.create(emitter -> {
            log.info("Using book fallback");
            fallbackService.search(criteria, ar -> {
                if (ar.succeeded()) {
                    emitter.onSuccess(ar.result());
                } else {
                    emitter.onError(ar.cause());
                }
            });
        });
    }

    private Consumer<List<JsonObject>> writeResponse(RoutingContext event, BookCriteria criteria) {
        return books -> {
            ObjectMapper mapper = criteria.getPrettyPrint() ? Json.prettyMapper : Json.mapper;
            event.response().end(mapper.writeValueAsString(books));
        };
    }

    /**
     * Process the requested fields for each book.
     * <p>
     * Default is minimal.
     * Whenever the "fields" field is present with at least one value, then the mapper is discovered.
     *
     * @param params the request parameters
     * @return the mapper
     */
    private Function<List<Book>, List<JsonObject>> processFields(RequestParameters params) {
        java.util.function.Function<Book, Book> mapper;
        if (params.queryParameter("fields") != null && params.queryParameter("fields").getArray().size() > 0) {
            Set<String> fields = params.queryParameter("fields").getArray().stream().map(RequestParameter::getString).collect(Collectors.toSet());
            mapper = this.getMapper(fields);
        } else {
            mapper = this::minimalMapper;
        }

        return books -> books.stream()
                .map(mapper)
                .map(Book::toJson)
                .collect(Collectors.toList());
    }

    /**
     * Get the appropriate mapper to fit the requested fields.
     * When "full" is requested, identity is returned.
     * When "minimal" is requested, minimal mapper is returned.
     * When the fields set size is equal to 6, then identity is returned (everything is requested).
     * Otherwise a custom mapper will copy field by field the book to match expected response.
     *
     * @param fields the fields to map
     * @return the mapper
     */
    private java.util.function.Function<Book, Book> getMapper(Set<String> fields) {
        if (fields.contains("full")) {
            return java.util.function.Function.identity();
        } else if (fields.contains("minimal")) {
            return this::minimalMapper;
        } else if (fields.size() == 6) {
            return java.util.function.Function.identity();
        } else {
            return this.customMapper(fields);
        }
    }


    /**
     * Returns a custom mapper that will build a new book with the minimal information + requested fields.
     *
     * @param fields the fields to add to the new book
     * @return the mapper
     */
    private java.util.function.Function<Book, Book> customMapper(Set<String> fields) {
        return book -> {
            Book mapped = this.minimalMapper(book);
            if (fields.contains("id")) {
                mapped.setId(book.getId());
            }

            if (fields.contains("term")) {
                mapped.setTerm(book.getTerm());
            }

            if (fields.contains("description")) {
                mapped.setDescription(book.getDescription());
            }

            if (fields.contains("publisher")) {
                mapped.setPublisher(book.getPublisher());
            }

            if (fields.contains("category")) {
                mapped.setCategory(book.getCategory());
            }

            if (fields.contains("language")) {
                mapped.setLanguage(book.getLanguage());
            }

            return mapped;
        };
    }

    /**
     * Minimal mapper adding only author and title.
     *
     * @param book the book to map
     * @return the mapped book
     */
    private Book minimalMapper(Book book) {
        return new Book().setAuthor(book.getAuthor()).setTitle(book.getTitle());
    }

    private Function<BookSearchService, Single<List<Book>>> search(BookCriteria criteria) {
        return service -> breakerHandleSearch.rxExecuteCommand(books -> {
            //noinspection unchecked
            service.search(criteria, ar -> {
                if (ar.succeeded()) {
                    books.complete(ar.result());
                } else {
                    log.error("Unable to get books", ar.cause());
                    books.fail(ar.cause());
                }
                ServiceDiscovery.releaseServiceObject(discovery, service);
            });
        });
    }

    private BookCriteria handleParams(RequestParameters params) {
        BookCriteria criteria = new BookCriteria(defaults);
        criteria.setTerm(params.queryParameter("term").getString());

        if (params.queryParameter("maxResults") != null) {
            criteria.setMaxResults(params.queryParameter("maxResults").getInteger());
        }

        if (params.queryParameter("startIndex") != null) {
            criteria.setStartIndex(params.queryParameter("startIndex").getInteger());
        }

        if (params.queryParameter("country") != null) {
            criteria.setLangRestrict(params.queryParameter("country").getString());
        }

        if (params.queryParameter("prettyPrint") != null) {
            criteria.setPrettyPrint(params.queryParameter("prettyPrint").getBoolean());
        }

        if (params.queryParameter("sortOrder") != null) {
            criteria.setOrderBy(OrderBy.valueOf(params.queryParameter("sortOrder").getString()));
        }

        if (params.queryParameter("media") != null) {
            criteria.setMedia(PrintType.valueOf(params.queryParameter("media").getString()));
        }
        return criteria;
    }

    @Override
    public Completable configure() {
        configProxy = Utils.configServiceProxy(vertx());
        return Completable.concatArray(
                Completable.create(this::prepareFallbackService),
                configureDiscovery(),
                configureCircuitBreaker(),
                configureDefaultCriteria()
        );
    }

    private void prepareFallbackService(CompletableEmitter emitter) {
        fallbackService = new SampleBookSearchService(vertx().getDelegate(), service -> emitter.onComplete());
    }

    private Completable configureDefaultCriteria() {
        return configProxy
                .rxGetObject(DEFAULT_CRITERIA_CONFIG)
                .onErrorResumeNext(Single.just(obj()))
                .map(BookCriteria::new)
                .doOnSuccess(this::setDefaults)
                .toCompletable();
    }

    private void setDefaults(BookCriteria criteria) {
        defaults = criteria;
    }

    private Completable configureCircuitBreaker() {
        return configProxy
                .rxGetObject(CIRCUIT_BREAKER_CONFIG)
                .onErrorResumeNext(Single.just(obj()))
                .map(CircuitBreakerOptions::new)
                .doOnSuccess(this::prepareBreakers)
                .toCompletable();
    }

    private void prepareBreakers(CircuitBreakerOptions options) {
        breakerHandleSearch = CircuitBreaker.create(BREAKER_HANDLE_SEARCH, vertx(), options);
        breakerGetService = CircuitBreaker.create(BREAKER_GET_SERVICE, vertx(), options);
    }

    private Completable configureDiscovery() {
        return Single.<ServiceDiscovery>create(emitter -> ServiceDiscovery.create(vertx(), new ServiceDiscoveryOptions().setName(SERVICE_DISCOVERY_NAME), emitter::onSuccess))
                .doOnSuccess(this::setDiscovery)
                .toCompletable();
    }

    private void setDiscovery(ServiceDiscovery discovery) {
        this.discovery = discovery;
    }

    private void getService(Future<BookSearchService> getService) {
        EventBusService.getServiceProxyWithJsonFilter(discovery, SERVICE_FILTER, BookSearchService.class, ar -> {
            if (ar.succeeded()) {
                BookSearchService service = ar.result();
                getService.complete(service);
            } else {
                log.error("Unable to discover service [book-search-service]", ar.cause());
                if (ar.cause() instanceof BookSearchServiceException && ((BookSearchServiceException) ar.cause()).getDebugInfo() != null) {
                    log.error("Debug info: ", ((BookSearchServiceException) ar.cause()).getDebugInfo());
                }
                getService.fail(ar.cause());
            }
        });
    }
}
