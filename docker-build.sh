#!/usr/bin/env bash

MODS="all"
DIR=$(dirname "$(readlink -f "$0")")

if [ "x$1" == "x" ]; then
    echo "tag is required"
    exit 1
fi

docker build -t myprysm/search-service:$1 $DIR/search-service
docker build -t myprysm/book-service:$1 $DIR/book-service
docker build -t myprysm/media-service:$1 $DIR/media-service
docker build -t myprysm/barrel:$1 $DIR/barrel