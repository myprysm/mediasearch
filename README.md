# Mediasearch

Search media and book content across Google Book API and Apple iTunes API.

## Design

This search service is designed to be resilient, fault-tolerant and scalable.
It relies on [Vert.X](https://vertx.io) clustering capabilities to deploy a set of independent and highly configurable microservices
that can communicate with each other through either REST API or Vert.X event bus (sharing contracts).

Two dedicated search services (`media-service` and `book-service`) define their respective contracts (`media-contract` and `book-contract`).
They act as proxies to their relying services.
They also provide data transformation and cache capability in an elasticsearch cluster.


The edge `search-service` consumes the contracts of the dedicated services 
to provide an aggregated response to the incoming requests.

Whenever the `search-service` cannot rely on the dedicated services, it will fallback to the `ElasticSearch` cache.
When disaster occurs and no link to `ElasticSearch` is made, then it will fallback to the sample implementation
of the underlying contract in order to respond to a request in an acceptable time.

All the process is designed asynchronously with the help of [RxJava](https://github.com/ReactiveX/RxJava).

In order to achieve the mission it relies on the following tools from Vert.X:

* vertx-codegen
* vertx-web
* vertx-web-api-contract
* vertx-rx-java2
* vertx-service-discovery
* vertx-circuit-breaker
* vertx-config
* vertx-health-check
* vertx-dropwizard-metrics
* vertx-hazelcast
* vertx-service-proxy

Some components from [vertx-pipeline](https://github.com/myprysm/vertx-pipeline) are also used:

* Json validation micro framework
* pipeline for asynchronous bulk caching into `ElasticSearch`

It relies also on a micro framework (vertx-core) bringing:

* configuration service (loads configuration from file + ENV/System properties)
* service lifecycle management (startup & shutdown, service discovery registration, healthcheck registration)
* automatic endpoint instanciation 
    (either through OpenAPI 3.0 specs w/ `OpenAPI3Endpoint`, or basic `Endpoint` w/ route registration)
* monitoring, metrics & healthchecks (default route is `/__`)
* full support for Cross Origin Resource Sharing configuration


The following endpoints are available by default when starting a WebVerticle:

* `/swagger.json` when OpenAPI 3.0 is enabled, exposes the service specs
* `/__/metrics/vertx` all the metrics exposed by vertx
* `/__/metrics/pipeline` all the metrics exposed by vertx-pipeline
* `/__/metrics/requests` [ThroughputTimer](https://vertx.io/docs/vertx-dropwizard-metrics/java/#throughput_timer) per endpoint
* `/__/metrics/hystrix` SSE stream for Hystrix Dashboard
* `/__/hc` service healthcheck

## Build & Run

In order to build this project you will need to install locally `vertx-pipeline` components:

```
git checkout https://github.com/myprysm/vertx-pipeline
cd vertx-pipeline
git checkout develop
mvn clean install
```

You can then `mvn clean install` to build the project. See below to build docker containers through maven.

Docker compose files are provided for a 2 nodes ElasticSearch cluster and the search service.

All endpoints are exposed respectively:

* search-service:   4000
* book-service:     4001
* media-service:    4002
* web-gateway:      80

A demo is available at [https://mediasearch.myprysm.fr](https://mediasearch.myprysm.fr)
as well as [API Specs](https://mediasearch.myprysm.fr/1.0/swagger.json).
Health Checks and Metrics are also exposed in this context 
while it is more reasonable to keep them for internal use in other real life contexts. 



### Configuration

Each property contained in the `configuration` object of the `config.yml` can be overridden through environment.
They must be provided as `configuration_your_configuration_key=your new value`, 
(note that `configuration.your.configuration.key` is also supported but does not work anymore with newer `sh` version).

#### Book Service

Please don't forget to provide the Google API Key (environment: `configuration_search_book_service_apiKey`).

Default parameters are:

```
search.book.service.default.maxResults=5
search.book.service.default.media=books
```

#### Media Service

Default parameters are:

```
search.media.service.default.maxResults=5
search.media.service.default.media=album
```

#### Search Service

Default parameters are:

```
search.config.service.default.maxResultsPerSource=5
search.config.service.default.sort.fields=title
search.config.service.default.sort.order=asc

search.book.service.default.media=books
search.book.service.default.startIndex=0

search.media.service.default.media=album
search.media.service.default.country=US
```

### Build

A maven profile `-Pdocker` is configured to automatically generate the default docker images for each service.
It expects to find some `docker daemon` on unix socket `tcp://localhost:2375`.

Clustering can be disabled for `barrel` by using `-Pdocker,nocluster` profiles.

Docker image build is on a per-module basis by adding the following configuration in maven build plugins:

```
<profiles>
    <profile>
        <id>docker</id>
        <build>
            <plugins>
                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <executions>
                        <execution>
                            <id>docker</id>
                            <phase>package</phase>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </build>
    </profile>
</profiles>
```

### Deploy



#### The microservice way

The following architecture will allow the service to be up, scalable and resilient.

Whenever needed each can be scaled with the appropriate configuration to handle the throughput
for each service.

`http` nodes on `search-service` can work without any service configured, 
but it is recommended to start at least the cache search service locally.

`mediasearch` Docker compose file starts a similar architecture, using just one node per service.

Keep in mind your availability zones and you will never be down.

![deploy](./global-architecture.png)



#### The plain old fashion

The project can be also used as a "barrel" or an "uber-jar" (see module `barrel`), bundling all the services.
By bringing the good configuration each component can still be started independently.

Used in combination with the `ha` verticle option from a Vert.X cluster you can ensure that
your verticles will be automatically redeployed when some of the node leaves the cluster.

See the `verticles` section from the `config.yml`.

`mediasearch-all-in-one` Docker compose file starts the hosted demo 
as this is the more reasonable way to run on a single host.