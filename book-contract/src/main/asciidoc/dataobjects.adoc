= Cheatsheets

[[Book]]
== Book


[cols=">25%,^25%,50%"]
[frame="topbot"]
|===
^|Name | Type ^| Description
|[[author]]`author`|`String`|-
|[[category]]`category`|`String`|-
|[[description]]`description`|`String`|-
|[[id]]`id`|`String`|-
|[[language]]`language`|`String`|-
|[[publisher]]`publisher`|`String`|-
|[[term]]`term`|`String`|-
|[[title]]`title`|`String`|-
|===

[[BookCriteria]]
== BookCriteria


[cols=">25%,^25%,50%"]
[frame="topbot"]
|===
^|Name | Type ^| Description
|[[langRestrict]]`langRestrict`|`String`|-
|[[maxResults]]`maxResults`|`Number (Integer)`|-
|[[media]]`media`|`link:enums.html#PrintType[PrintType]`|-
|[[orderBy]]`orderBy`|`link:enums.html#OrderBy[OrderBy]`|-
|[[prettyPrint]]`prettyPrint`|`Boolean`|-
|[[startIndex]]`startIndex`|`Number (Integer)`|-
|[[term]]`term`|`String`|-
|===

