require 'vertx/util/utils.rb'
# Generated from fr.myprysm.mediasearch.book.service.BookSearchService
module MsBookInterface
  class BookSearchService
    # @private
    # @param j_del [::MsBookInterface::BookSearchService] the java delegate
    def initialize(j_del)
      @j_del = j_del
    end
    # @private
    # @return [::MsBookInterface::BookSearchService] the underlying java delegate
    def j_del
      @j_del
    end
    @@j_api_type = Object.new
    def @@j_api_type.accept?(obj)
      obj.class == BookSearchService
    end
    def @@j_api_type.wrap(obj)
      BookSearchService.new(obj)
    end
    def @@j_api_type.unwrap(obj)
      obj.j_del
    end
    def self.j_api_type
      @@j_api_type
    end
    def self.j_class
      Java::FrMyprysmMediasearchBookService::BookSearchService.java_class
    end
    #  Search books by term (full text search)
    #  <p>
    #  This will use the default parameters configured in the service.
    #  For a more fine grained search please check {::MsBookInterface::BookSearchService#search}.
    # @param [String] term the full text search
    # @yield result handler
    # @return [self]
    def find_by_term(term=nil)
      if term.class == String && block_given?
        @j_del.java_method(:findByTerm, [Java::java.lang.String.java_class,Java::IoVertxCore::Handler.java_class]).call(term,(Proc.new { |ar| yield(ar.failed ? ar.cause : nil, ar.succeeded ? ar.result.to_a.map { |elt| elt != nil ? JSON.parse(elt.toJson.encode) : nil } : nil) }))
        return self
      end
      raise ArgumentError, "Invalid arguments when calling find_by_term(#{term})"
    end
    #  Search books by term
    # @param [Hash] criteria the book search criteria
    # @yield the result handler
    # @return [self]
    def search(criteria=nil)
      if criteria.class == Hash && block_given?
        @j_del.java_method(:search, [Java::FrMyprysmMediasearchBookDto::BookCriteria.java_class,Java::IoVertxCore::Handler.java_class]).call(Java::FrMyprysmMediasearchBookDto::BookCriteria.new(::Vertx::Util::Utils.to_json_object(criteria)),(Proc.new { |ar| yield(ar.failed ? ar.cause : nil, ar.succeeded ? ar.result.to_a.map { |elt| elt != nil ? JSON.parse(elt.toJson.encode) : nil } : nil) }))
        return self
      end
      raise ArgumentError, "Invalid arguments when calling search(#{criteria})"
    end
  end
end
