/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package fr.myprysm.mediasearch.book.dto;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link fr.myprysm.mediasearch.book.dto.Book}.
 *
 * NOTE: This class has been automatically generated from the {@link fr.myprysm.mediasearch.book.dto.Book} original class using Vert.x codegen.
 */
public class BookConverter {

  public static void fromJson(JsonObject json, Book obj) {
    if (json.getValue("author") instanceof String) {
      obj.setAuthor((String)json.getValue("author"));
    }
    if (json.getValue("category") instanceof String) {
      obj.setCategory((String)json.getValue("category"));
    }
    if (json.getValue("description") instanceof String) {
      obj.setDescription((String)json.getValue("description"));
    }
    if (json.getValue("id") instanceof String) {
      obj.setId((String)json.getValue("id"));
    }
    if (json.getValue("language") instanceof String) {
      obj.setLanguage((String)json.getValue("language"));
    }
    if (json.getValue("publisher") instanceof String) {
      obj.setPublisher((String)json.getValue("publisher"));
    }
    if (json.getValue("term") instanceof String) {
      obj.setTerm((String)json.getValue("term"));
    }
    if (json.getValue("title") instanceof String) {
      obj.setTitle((String)json.getValue("title"));
    }
  }

  public static void toJson(Book obj, JsonObject json) {
    if (obj.getAuthor() != null) {
      json.put("author", obj.getAuthor());
    }
    if (obj.getCategory() != null) {
      json.put("category", obj.getCategory());
    }
    if (obj.getDescription() != null) {
      json.put("description", obj.getDescription());
    }
    if (obj.getId() != null) {
      json.put("id", obj.getId());
    }
    if (obj.getLanguage() != null) {
      json.put("language", obj.getLanguage());
    }
    if (obj.getPublisher() != null) {
      json.put("publisher", obj.getPublisher());
    }
    if (obj.getTerm() != null) {
      json.put("term", obj.getTerm());
    }
    if (obj.getTitle() != null) {
      json.put("title", obj.getTitle());
    }
  }
}