package fr.myprysm.mediasearch.book.groovy.service;
public class BookSearchService_GroovyExtension {
  public static fr.myprysm.mediasearch.book.service.BookSearchService findByTerm(fr.myprysm.mediasearch.book.service.BookSearchService j_receiver, java.lang.String term, io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<java.util.Map<String, Object>>>> handler) {
    io.vertx.core.impl.ConversionHelper.fromObject(j_receiver.findByTerm(term,
      handler != null ? new io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.book.dto.Book>>>() {
      public void handle(io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.book.dto.Book>> ar) {
        handler.handle(ar.map(event -> event != null ? event.stream().map(elt -> elt != null ? io.vertx.core.impl.ConversionHelper.fromJsonObject(elt.toJson()) : null).collect(java.util.stream.Collectors.toList()) : null));
      }
    } : null));
    return j_receiver;
  }
  public static fr.myprysm.mediasearch.book.service.BookSearchService search(fr.myprysm.mediasearch.book.service.BookSearchService j_receiver, java.util.Map<String, Object> criteria, io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<java.util.Map<String, Object>>>> handler) {
    io.vertx.core.impl.ConversionHelper.fromObject(j_receiver.search(criteria != null ? new fr.myprysm.mediasearch.book.dto.BookCriteria(io.vertx.core.impl.ConversionHelper.toJsonObject(criteria)) : null,
      handler != null ? new io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.book.dto.Book>>>() {
      public void handle(io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.book.dto.Book>> ar) {
        handler.handle(ar.map(event -> event != null ? event.stream().map(elt -> elt != null ? io.vertx.core.impl.ConversionHelper.fromJsonObject(elt.toJson()) : null).collect(java.util.stream.Collectors.toList()) : null));
      }
    } : null));
    return j_receiver;
  }
}
