/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package fr.myprysm.mediasearch.book.dto;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link fr.myprysm.mediasearch.book.dto.BookCriteria}.
 *
 * NOTE: This class has been automatically generated from the {@link fr.myprysm.mediasearch.book.dto.BookCriteria} original class using Vert.x codegen.
 */
public class BookCriteriaConverter {

  public static void fromJson(JsonObject json, BookCriteria obj) {
    if (json.getValue("langRestrict") instanceof String) {
      obj.setLangRestrict((String)json.getValue("langRestrict"));
    }
    if (json.getValue("maxResults") instanceof Number) {
      obj.setMaxResults(((Number)json.getValue("maxResults")).intValue());
    }
    if (json.getValue("media") instanceof String) {
      obj.setMedia(fr.myprysm.mediasearch.book.dto.BookCriteria.PrintType.valueOf((String)json.getValue("media")));
    }
    if (json.getValue("orderBy") instanceof String) {
      obj.setOrderBy(fr.myprysm.mediasearch.book.dto.BookCriteria.OrderBy.valueOf((String)json.getValue("orderBy")));
    }
    if (json.getValue("prettyPrint") instanceof Boolean) {
      obj.setPrettyPrint((Boolean)json.getValue("prettyPrint"));
    }
    if (json.getValue("startIndex") instanceof Number) {
      obj.setStartIndex(((Number)json.getValue("startIndex")).intValue());
    }
    if (json.getValue("term") instanceof String) {
      obj.setTerm((String)json.getValue("term"));
    }
  }

  public static void toJson(BookCriteria obj, JsonObject json) {
    if (obj.getLangRestrict() != null) {
      json.put("langRestrict", obj.getLangRestrict());
    }
    if (obj.getMaxResults() != null) {
      json.put("maxResults", obj.getMaxResults());
    }
    if (obj.getMedia() != null) {
      json.put("media", obj.getMedia().name());
    }
    if (obj.getOrderBy() != null) {
      json.put("orderBy", obj.getOrderBy().name());
    }
    if (obj.getPrettyPrint() != null) {
      json.put("prettyPrint", obj.getPrettyPrint());
    }
    if (obj.getStartIndex() != null) {
      json.put("startIndex", obj.getStartIndex());
    }
    if (obj.getTerm() != null) {
      json.put("term", obj.getTerm());
    }
  }
}