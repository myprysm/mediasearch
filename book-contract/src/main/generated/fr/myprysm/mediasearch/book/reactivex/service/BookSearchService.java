/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package fr.myprysm.mediasearch.book.reactivex.service;

import java.util.Map;
import io.reactivex.Observable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import fr.myprysm.mediasearch.book.dto.BookCriteria;
import java.util.List;
import fr.myprysm.mediasearch.book.dto.Book;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;


@io.vertx.lang.reactivex.RxGen(fr.myprysm.mediasearch.book.service.BookSearchService.class)
public class BookSearchService {

  @Override
  public String toString() {
    return delegate.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BookSearchService that = (BookSearchService) o;
    return delegate.equals(that.delegate);
  }
  
  @Override
  public int hashCode() {
    return delegate.hashCode();
  }

  public static final io.vertx.lang.reactivex.TypeArg<BookSearchService> __TYPE_ARG = new io.vertx.lang.reactivex.TypeArg<>(
    obj -> new BookSearchService((fr.myprysm.mediasearch.book.service.BookSearchService) obj),
    BookSearchService::getDelegate
  );

  private final fr.myprysm.mediasearch.book.service.BookSearchService delegate;
  
  public BookSearchService(fr.myprysm.mediasearch.book.service.BookSearchService delegate) {
    this.delegate = delegate;
  }

  public fr.myprysm.mediasearch.book.service.BookSearchService getDelegate() {
    return delegate;
  }

  /**
   * Search books by term (full text search)
   * <p>
   * This will use the default parameters configured in the service.
   * For a more fine grained search please check {@link fr.myprysm.mediasearch.book.reactivex.service.BookSearchService#search}.
   * @param term the full text search
   * @param handler result handler
   * @return this
   */
  public BookSearchService findByTerm(String term, Handler<AsyncResult<List<Book>>> handler) { 
    delegate.findByTerm(term, handler);
    return this;
  }

  /**
   * Search books by term (full text search)
   * <p>
   * This will use the default parameters configured in the service.
   * For a more fine grained search please check {@link fr.myprysm.mediasearch.book.reactivex.service.BookSearchService#search}.
   * @param term the full text search
   * @return 
   */
  public Single<List<Book>> rxFindByTerm(String term) { 
    return new io.vertx.reactivex.core.impl.AsyncResultSingle<List<Book>>(handler -> {
      findByTerm(term, handler);
    });
  }

  /**
   * Search books by term
   * @param criteria the book search criteria
   * @param handler the result handler
   * @return this
   */
  public BookSearchService search(BookCriteria criteria, Handler<AsyncResult<List<Book>>> handler) { 
    delegate.search(criteria, handler);
    return this;
  }

  /**
   * Search books by term
   * @param criteria the book search criteria
   * @return 
   */
  public Single<List<Book>> rxSearch(BookCriteria criteria) { 
    return new io.vertx.reactivex.core.impl.AsyncResultSingle<List<Book>>(handler -> {
      search(criteria, handler);
    });
  }


  public static  BookSearchService newInstance(fr.myprysm.mediasearch.book.service.BookSearchService arg) {
    return arg != null ? new BookSearchService(arg) : null;
  }
}
