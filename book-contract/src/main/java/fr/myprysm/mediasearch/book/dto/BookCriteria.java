package fr.myprysm.mediasearch.book.dto;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@DataObject(generateConverter = true)
public class BookCriteria {

    private Integer maxResults = 10;
    private Integer startIndex = 0;
    private PrintType media;
    private OrderBy orderBy;
    private String langRestrict;
    private String term;
    private Boolean prettyPrint = false;

    public BookCriteria(BookCriteria other) {
        setMaxResults(other.maxResults);
        setStartIndex(other.startIndex);
        setMedia(other.media);
        setOrderBy(other.orderBy);
        setLangRestrict(other.langRestrict);
        setTerm(other.term);
        setPrettyPrint(other.prettyPrint);
    }

    public BookCriteria(JsonObject json) {
        BookCriteriaConverter.fromJson(json, this);
    }

    public BookCriteria setMaxResults(Integer maxResults) {
        if (maxResults != null) {
            if (maxResults < 0) {
                maxResults = 0;
            }

            if (maxResults > 40) {
                maxResults = 40;
            }
        }
        this.maxResults = maxResults;
        return this;
    }

    public BookCriteria setStartIndex(Integer startIndex) {
        if (startIndex != null) {
            if (startIndex < 0) {
                startIndex = 0;
            }
        }
        this.startIndex = startIndex;
        return this;
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        BookCriteriaConverter.toJson(this, json);
        return json;
    }


    public enum OrderBy {newest, relevance}

    public enum PrintType {all, books, magazines}

}
