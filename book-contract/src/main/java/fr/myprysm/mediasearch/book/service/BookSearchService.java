package fr.myprysm.mediasearch.book.service;

import fr.myprysm.mediasearch.book.dto.Book;
import fr.myprysm.mediasearch.book.dto.BookCriteria;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.GenIgnore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

import java.util.List;

@VertxGen
@ProxyGen
public interface BookSearchService {

    /**
     * Generates the sample service bundled with the contract
     *
     * @param vertx   the vertx instance
     * @param handler the handler
     * @return the service, not yet initialized (use handler)
     */
    @GenIgnore
    static BookSearchService createSample(Vertx vertx, Handler<AsyncResult<BookSearchService>> handler) {
        return new SampleBookSearchService(vertx, handler);
    }

    /**
     * Search books by term (full text search)
     * <p>
     * This will use the default parameters configured in the service.
     * For a more fine grained search please check {@link #search(BookCriteria, Handler)}.
     *
     * @param term    the full text search
     * @param handler result handler
     * @return this
     */
    @Fluent
    BookSearchService findByTerm(String term, Handler<AsyncResult<List<Book>>> handler);

    /**
     * Search books by term
     *
     * @param criteria the book search criteria
     * @param handler  the result handler
     * @return this
     */
    @Fluent
    BookSearchService search(BookCriteria criteria, Handler<AsyncResult<List<Book>>> handler);
}
