package fr.myprysm.mediasearch.book.dto;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@DataObject(generateConverter = true)
public class Book {

    private String id;
    private String term;
    private String title;
    private String author;
    private String publisher;
    private String description;
    private String category;
    private String language;

    public Book(Book other) {
        id = other.id;
        term = other.term;
        title = other.title;
        author = other.author;
        publisher = other.publisher;
        description = other.description;
        category = other.category;
        language = other.language;
    }

    public Book(JsonObject json) {
        BookConverter.fromJson(json, this);
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        BookConverter.toJson(this, json);
        return json;
    }
}
