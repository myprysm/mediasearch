package fr.myprysm.mediasearch.book.kotlin.dto

import fr.myprysm.mediasearch.book.dto.BookCriteria
import fr.myprysm.mediasearch.book.dto.BookCriteria.OrderBy
import fr.myprysm.mediasearch.book.dto.BookCriteria.PrintType

fun BookCriteria(
        langRestrict: String? = null,
        maxResults: Int? = null,
        media: PrintType? = null,
        orderBy: OrderBy? = null,
        prettyPrint: Boolean? = null,
        startIndex: Int? = null,
        term: String? = null): BookCriteria = fr.myprysm.mediasearch.book.dto.BookCriteria().apply {

    if (langRestrict != null) {
        this.setLangRestrict(langRestrict)
    }
    if (maxResults != null) {
        this.setMaxResults(maxResults)
    }
    if (media != null) {
        this.setMedia(media)
    }
    if (orderBy != null) {
        this.setOrderBy(orderBy)
    }
    if (prettyPrint != null) {
        this.setPrettyPrint(prettyPrint)
    }
    if (startIndex != null) {
        this.setStartIndex(startIndex)
    }
    if (term != null) {
        this.setTerm(term)
    }
}

