package fr.myprysm.mediasearch.book.kotlin.dto

import fr.myprysm.mediasearch.book.dto.Book

fun Book(
        author: String? = null,
        category: String? = null,
        description: String? = null,
        id: String? = null,
        language: String? = null,
        publisher: String? = null,
        term: String? = null,
        title: String? = null): Book = fr.myprysm.mediasearch.book.dto.Book().apply {

    if (author != null) {
        this.setAuthor(author)
    }
    if (category != null) {
        this.setCategory(category)
    }
    if (description != null) {
        this.setDescription(description)
    }
    if (id != null) {
        this.setId(id)
    }
    if (language != null) {
        this.setLanguage(language)
    }
    if (publisher != null) {
        this.setPublisher(publisher)
    }
    if (term != null) {
        this.setTerm(term)
    }
    if (title != null) {
        this.setTitle(title)
    }
}

