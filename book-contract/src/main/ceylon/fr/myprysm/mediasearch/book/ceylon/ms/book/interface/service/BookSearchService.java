package fr.myprysm.mediasearch.book.ceylon.ms.book.interface.service;

import com.redhat.ceylon.compiler.java.metadata.Ceylon;
import com.redhat.ceylon.compiler.java.metadata.TypeInfo;
import com.redhat.ceylon.compiler.java.metadata.TypeParameter;
import com.redhat.ceylon.compiler.java.metadata.TypeParameters;
import com.redhat.ceylon.compiler.java.metadata.Variance;
import com.redhat.ceylon.compiler.java.metadata.Ignore;
import com.redhat.ceylon.compiler.java.metadata.Name;
import com.redhat.ceylon.compiler.java.runtime.model.TypeDescriptor;
import com.redhat.ceylon.compiler.java.runtime.model.ReifiedType;
import ceylon.language.Callable;
import ceylon.language.DocAnnotation$annotation$;
import java.util.List;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

@Ceylon(major = 8)
public class BookSearchService implements ReifiedType {

  @Ignore
  public static final io.vertx.lang.ceylon.ConverterFactory<fr.myprysm.mediasearch.book.service.BookSearchService, BookSearchService> TO_CEYLON = new io.vertx.lang.ceylon.ConverterFactory<fr.myprysm.mediasearch.book.service.BookSearchService, BookSearchService>() {
    public io.vertx.lang.ceylon.Converter<fr.myprysm.mediasearch.book.service.BookSearchService, BookSearchService> converter(final TypeDescriptor... descriptors) {
      return new io.vertx.lang.ceylon.Converter<fr.myprysm.mediasearch.book.service.BookSearchService, BookSearchService>() {
        public BookSearchService convert(fr.myprysm.mediasearch.book.service.BookSearchService src) {
          return new BookSearchService(src);
        }
      };
    }
  };

  @Ignore
  public static final io.vertx.lang.ceylon.Converter<BookSearchService, fr.myprysm.mediasearch.book.service.BookSearchService> TO_JAVA = new io.vertx.lang.ceylon.Converter<BookSearchService, fr.myprysm.mediasearch.book.service.BookSearchService>() {
    public fr.myprysm.mediasearch.book.service.BookSearchService convert(BookSearchService src) {
      return src.delegate;
    }
  };

  @Ignore public static final TypeDescriptor $TypeDescriptor$ = new io.vertx.lang.ceylon.VertxTypeDescriptor(TypeDescriptor.klass(BookSearchService.class), fr.myprysm.mediasearch.book.service.BookSearchService.class, TO_JAVA, TO_CEYLON);
  @Ignore private final fr.myprysm.mediasearch.book.service.BookSearchService delegate;

  public BookSearchService(fr.myprysm.mediasearch.book.service.BookSearchService delegate) {
    this.delegate = delegate;
  }

  @Ignore 
  public TypeDescriptor $getType$() {
    return $TypeDescriptor$;
  }

  @Ignore
  public Object getDelegate() {
    return delegate;
  }

  @DocAnnotation$annotation$(description = " Search books by term (full text search)\n <p>\n This will use the default parameters configured in the service.\n For a more fine grained search please check [search](../service/BookSearchService.type.html#search).\n")
  @TypeInfo("fr.myprysm.mediasearch.book.ceylon.ms.book.interface.service::BookSearchService")
  public BookSearchService findByTerm(
    final @TypeInfo("ceylon.language::String") @Name("term")@DocAnnotation$annotation$(description = "the full text search\n") ceylon.language.String term, 
    final @TypeInfo("ceylon.language::Anything(ceylon.language::Throwable|ceylon.language::List<fr.myprysm.mediasearch.book.ceylon.ms.book.interface.dto::Book>)") @Name("handler")@DocAnnotation$annotation$(description = "result handler\n") Callable<?> handler) {
    java.lang.String arg_0 = io.vertx.lang.ceylon.ToJava.String.safeConvert(term);
    io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.book.dto.Book>>> arg_1 = handler == null ? null : new io.vertx.lang.ceylon.CallableAsyncResultHandler<java.util.List<fr.myprysm.mediasearch.book.dto.Book>>(handler) {
      public Object toCeylon(java.util.List<fr.myprysm.mediasearch.book.dto.Book> event) {
        return io.vertx.lang.ceylon.ToCeylon.convertList(fr.myprysm.mediasearch.book.ceylon.ms.book.interface.dto.Book.$TypeDescriptor$, event, fr.myprysm.mediasearch.book.ceylon.ms.book.interface.dto.book_.get_().getToCeylon());
      }
    };
    BookSearchService ret = fr.myprysm.mediasearch.book.ceylon.ms.book.interface.service.BookSearchService.TO_CEYLON.converter().safeConvert(delegate.findByTerm(arg_0, arg_1));
    return this;
  }

  @DocAnnotation$annotation$(description = " Search books by term\n")
  @TypeInfo("fr.myprysm.mediasearch.book.ceylon.ms.book.interface.service::BookSearchService")
  public BookSearchService search(
    final @TypeInfo("fr.myprysm.mediasearch.book.ceylon.ms.book.interface.dto::BookCriteria") @Name("criteria")@DocAnnotation$annotation$(description = "the book search criteria\n") fr.myprysm.mediasearch.book.ceylon.ms.book.interface.dto.BookCriteria criteria, 
    final @TypeInfo("ceylon.language::Anything(ceylon.language::Throwable|ceylon.language::List<fr.myprysm.mediasearch.book.ceylon.ms.book.interface.dto::Book>)") @Name("handler")@DocAnnotation$annotation$(description = "the result handler\n") Callable<?> handler) {
    fr.myprysm.mediasearch.book.dto.BookCriteria arg_0 = criteria == null ? null : new fr.myprysm.mediasearch.book.dto.BookCriteria(io.vertx.lang.ceylon.ToJava.JsonObject.convert(criteria.toJson()));
    io.vertx.core.Handler<io.vertx.core.AsyncResult<java.util.List<fr.myprysm.mediasearch.book.dto.Book>>> arg_1 = handler == null ? null : new io.vertx.lang.ceylon.CallableAsyncResultHandler<java.util.List<fr.myprysm.mediasearch.book.dto.Book>>(handler) {
      public Object toCeylon(java.util.List<fr.myprysm.mediasearch.book.dto.Book> event) {
        return io.vertx.lang.ceylon.ToCeylon.convertList(fr.myprysm.mediasearch.book.ceylon.ms.book.interface.dto.Book.$TypeDescriptor$, event, fr.myprysm.mediasearch.book.ceylon.ms.book.interface.dto.book_.get_().getToCeylon());
      }
    };
    BookSearchService ret = fr.myprysm.mediasearch.book.ceylon.ms.book.interface.service.BookSearchService.TO_CEYLON.converter().safeConvert(delegate.search(arg_0, arg_1));
    return this;
  }

}
