import fr.myprysm.mediasearch.book.dto {
  BookCriteria_=BookCriteria
}
import ceylon.json {
  JsonObject=Object,
  JsonArray=Array,
  parse
}
import io.vertx.lang.ceylon {
  BaseDataObject,
  Converter,
  ToJava
}
import ceylon.collection {
  HashMap
}
import io.vertx.core.json {
  JsonObject_=JsonObject,
  JsonArray_=JsonArray
}
/* Generated from fr.myprysm.mediasearch.book.dto.BookCriteria */
shared class BookCriteria(
  shared String? langRestrict = null,
  shared Integer? maxResults = null,
  shared String? media = null,
  shared String? orderBy = null,
  shared Boolean? prettyPrint = null,
  shared Integer? startIndex = null,
  shared String? term = null) satisfies BaseDataObject {
  shared actual default JsonObject toJson() {
    value json = JsonObject();
    if (exists langRestrict) {
      json.put("langRestrict", langRestrict);
    }
    if (exists maxResults) {
      json.put("maxResults", maxResults);
    }
    if (exists media) {
      json.put("media", media);
    }
    if (exists orderBy) {
      json.put("orderBy", orderBy);
    }
    if (exists prettyPrint) {
      json.put("prettyPrint", prettyPrint);
    }
    if (exists startIndex) {
      json.put("startIndex", startIndex);
    }
    if (exists term) {
      json.put("term", term);
    }
    return json;
  }
}

shared object bookCriteria {

  shared BookCriteria fromJson(JsonObject json) {
    String? langRestrict = json.getStringOrNull("langRestrict");
    Integer? maxResults = json.getIntegerOrNull("maxResults");
    String? media = json.getStringOrNull("media");
    String? orderBy = json.getStringOrNull("orderBy");
    Boolean? prettyPrint = json.getBooleanOrNull("prettyPrint");
    Integer? startIndex = json.getIntegerOrNull("startIndex");
    String? term = json.getStringOrNull("term");
    return BookCriteria {
      langRestrict = langRestrict;
      maxResults = maxResults;
      media = media;
      orderBy = orderBy;
      prettyPrint = prettyPrint;
      startIndex = startIndex;
      term = term;
    };
  }

  shared object toCeylon extends Converter<BookCriteria_, BookCriteria>() {
    shared actual BookCriteria convert(BookCriteria_ src) {
      value json = parse(src.toJson().string);
      assert(is JsonObject json);
      return fromJson(json);
    }
  }

  shared object toJava extends Converter<BookCriteria, BookCriteria_>() {
    shared actual BookCriteria_ convert(BookCriteria src) {
      // Todo : make optimized version without json
      value json = JsonObject_(src.toJson().string);
      value ret = BookCriteria_(json);
      return ret;
    }
  }
  shared JsonObject toJson(BookCriteria obj) => obj.toJson();
}
