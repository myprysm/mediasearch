import fr.myprysm.mediasearch.book.dto {
  Book_=Book
}
import ceylon.json {
  JsonObject=Object,
  JsonArray=Array,
  parse
}
import io.vertx.lang.ceylon {
  BaseDataObject,
  Converter,
  ToJava
}
import ceylon.collection {
  HashMap
}
import io.vertx.core.json {
  JsonObject_=JsonObject,
  JsonArray_=JsonArray
}
/* Generated from fr.myprysm.mediasearch.book.dto.Book */
shared class Book(
  shared String? author = null,
  shared String? category = null,
  shared String? description = null,
  shared String? id = null,
  shared String? language = null,
  shared String? publisher = null,
  shared String? term = null,
  shared String? title = null) satisfies BaseDataObject {
  shared actual default JsonObject toJson() {
    value json = JsonObject();
    if (exists author) {
      json.put("author", author);
    }
    if (exists category) {
      json.put("category", category);
    }
    if (exists description) {
      json.put("description", description);
    }
    if (exists id) {
      json.put("id", id);
    }
    if (exists language) {
      json.put("language", language);
    }
    if (exists publisher) {
      json.put("publisher", publisher);
    }
    if (exists term) {
      json.put("term", term);
    }
    if (exists title) {
      json.put("title", title);
    }
    return json;
  }
}

shared object book {

  shared Book fromJson(JsonObject json) {
    String? author = json.getStringOrNull("author");
    String? category = json.getStringOrNull("category");
    String? description = json.getStringOrNull("description");
    String? id = json.getStringOrNull("id");
    String? language = json.getStringOrNull("language");
    String? publisher = json.getStringOrNull("publisher");
    String? term = json.getStringOrNull("term");
    String? title = json.getStringOrNull("title");
    return Book {
      author = author;
      category = category;
      description = description;
      id = id;
      language = language;
      publisher = publisher;
      term = term;
      title = title;
    };
  }

  shared object toCeylon extends Converter<Book_, Book>() {
    shared actual Book convert(Book_ src) {
      value json = parse(src.toJson().string);
      assert(is JsonObject json);
      return fromJson(json);
    }
  }

  shared object toJava extends Converter<Book, Book_>() {
    shared actual Book_ convert(Book src) {
      // Todo : make optimized version without json
      value json = JsonObject_(src.toJson().string);
      value ret = Book_(json);
      return ret;
    }
  }
  shared JsonObject toJson(Book obj) => obj.toJson();
}
