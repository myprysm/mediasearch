package fr.myprysm.mediasearch.book.service;

import fr.myprysm.mediasearch.book.dto.BookCriteria;
import fr.myprysm.vertx.test.VertxTest;
import io.vertx.core.Vertx;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SampleBookSearchServiceTest implements VertxTest {

    @Test
    @DisplayName("SampleBookSearchService should serve results")
    void itShouldServeResults(Vertx vertx, VertxTestContext ctx) {
        Checkpoint cp = ctx.checkpoint(2);
        new SampleBookSearchService(vertx, ctx.succeeding(service -> {
            service.search(new BookCriteria().setTerm("Some term").setMaxResults(10), ctx.succeeding(books -> {
                ctx.verify(() -> {
                    assertThat(books.size()).isEqualTo(10);
                    assertThat(books.get(0).getTerm()).isEqualTo("Some term");
                });
                cp.flag();
            }));

            service.findByTerm("Another term", ctx.succeeding(books -> {
                ctx.verify(() -> {
                    assertThat(books.size()).isEqualTo(5);
                    assertThat(books.get(0).getTerm()).isEqualTo("Another term");
                });
                cp.flag();
            }));
        }));

    }
}